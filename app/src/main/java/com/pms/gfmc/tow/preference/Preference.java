package com.pms.gfmc.tow.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.pms.gfmc.tow.utils.StringUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Preference {
    Context mContext;
    private static Preference sInstance;
    protected SharedPreferences mSharedPreferences;






    // 2018-07-31 jhlee 암호화 복호화를 위한 작업
    private AES256Cipher aesInstance;

    protected Preference(Context appContext) {
        mContext = appContext;

        // 2018-07-31 jhlee 암호화 복호화를 위한 작업
        aesInstance = AES256Cipher.getInstance();
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new Preference(context);
        }
    }

    public static synchronized Preference getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(Preference.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }


    public int getInt(String key) {
        // 복호화 이전 기존로직
//        return mSharedPreferences.getInt(key, 0);

        // 2018-07-31 jhlee 복호화 작업
        int value = 0;
        try {
            value = Integer.parseInt(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (Exception e) {

        }
        return value;
    }

    public int getInt(String key, int value) {
        // 복호화 이전 기존로직
//        return mSharedPreferences.getInt(key, value);
        // 2018-07-31 jhlee 복호화 작업
        int returnValue = 0;
        try {
            returnValue = Integer.parseInt(aesInstance.AES_Decode(mSharedPreferences.getString(key, String.valueOf(value))));
        } catch (Exception e) {

        }
        return returnValue;
    }

    public long getLong(String key) {
        // 복호화 이전 기존로직
//        return mSharedPreferences.getLong(key, 0l);

        // 2018-07-31 jhlee 복호화 작업
        long value = 0;
        try {
            value = Long.parseLong(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public String getString(String key) {
        String value = mSharedPreferences.getString(key, "");
        String decodeValue = "";
        // 2018-07-31 jhlee 복호화 작업
        
        if(StringUtil.isEmpty(value)) return "";

        try {
            decodeValue = aesInstance.AES_Decode(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (decodeValue.equalsIgnoreCase("null")) {
            return "";
        } else {
            return decodeValue;
        }
    }


    public String getString(String key, String defaultMsg) {
        // 암호화 이전 기존로직
//        return mSharedPreferences.getString(key, "");

        String value = mSharedPreferences.getString(key, defaultMsg);
        String decodeValue = "";
        // 2018-07-31 jhlee 복호화 작업
        try {
            decodeValue = aesInstance.AES_Decode(value);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (decodeValue.equalsIgnoreCase("null") || decodeValue == null) {
                return "";
            } else if ("".equals(decodeValue) && !"".equals(defaultMsg)) {
                return defaultMsg;
            } else {
                return decodeValue;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public double getDouble(String key) {
        // 복호화 이전 기존로직
//        String number = getString(key);
//        try {
//            double value = Double.parseDouble(number);
//            return value;
//        } catch (NumberFormatException e) {
//            return 0;
//        }

        double value = 0;
        try {
            value = Double.parseDouble(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (Exception e) {

        }
        return value;
    }

    @SuppressLint("NewApi")
    public void putInt(String key, int value) {
        // 암호화 이전 기존로직
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//        editor.putInt(key, value);
//        editor.apply();

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        String strValue = "";
        // 2018-07-31 jhlee 암호화 작업
        try {
            strValue = aesInstance.AES_Encode(String.valueOf(value));
        } catch (Exception e) {

        }

        editor.putString(key, strValue);
        editor.apply();
    }

    public void putLong(String key, long value) {
        // 암호화 이전 기존로직
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//        editor.putLong(key, value);
//        editor.apply();

        // 2018-07-31 jhlee 암호화 작업
        try {
            putString(key, String.valueOf(value));
        } catch (Exception e) {

        }
    }

    public void putDouble(String key, double value) {
        putString(key, String.valueOf(value));
    }

    public void putString(String key, String value) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        // 2018-07-31 jhlee 암호화 작업
        try {
            if (!StringUtil.isEmpty(value)) {
                value = aesInstance.AES_Encode(value);
            } else {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        editor.putString(key, value);
        editor.apply();

    }

    public void putList(String key, ArrayList<String> marray) {

        // 복호화 이전 기존로직
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//        String[] mystringlist = marray.toArray(new String[marray.size()]);
//        // the comma like character used below is not a comma it is the SINGLE
//        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
//        // seprating the items in the list
//        editor.putString(key, TextUtils.join("‚‗‚", mystringlist));
//        editor.apply();

        // 2018-07-31 jhlee 암호화 작업
        String[] mystringlist = marray.toArray(new String[marray.size()]);
        putString(key, TextUtils.join("‚‗‚", mystringlist));
    }

    public void addList(String key, String marray) {
        //해당 리스트 가져오기.
        // 암호화 복호화 이전 로직
//        String[] mylist = TextUtils.split(mSharedPreferences.getString(key, ""), "‚‗‚");
//        ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
//
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//        gottenlist.add(marray);
//
//        editor.putString(key, TextUtils.join("‚‗‚", gottenlist));
//        editor.apply();

        try {
            // 2018-07-31 jhlee 복호화 작업
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));

            SharedPreferences.Editor editor = mSharedPreferences.edit();
            gottenlist.add(marray);

            // 2018-07-31 jhlee 암호화 작업
            editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
            editor.apply();
        } catch (Exception e) {

        }
    }

    public void changeList(String key, String marray) {
        //해당 리스트 가져오기.
        // 암호화 이전 로직
//        String[] mylist = TextUtils.split(mSharedPreferences.getString(key, ""), "‚‗‚");
//        ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
//
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//
////        if(gottenlist.contains(marray)){
////
////        }
//
//        String userId=marray.split("\\|\\|")[0];
//        for(int i=0 ; i < gottenlist.size() ; i++){
//            if(gottenlist.get(i).split("\\|\\|")[0].equals(userId)){
//                gottenlist.set(i,marray);
//            }
//        }
//
//
//        editor.putString(key, TextUtils.join("‚‗‚", gottenlist));
//        editor.apply();

        try {
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));

            SharedPreferences.Editor editor = mSharedPreferences.edit();

//        if(gottenlist.contains(marray)){
//
//        }

            String userId = marray.split("\\|\\|")[0];
            for (int i = 0; i < gottenlist.size(); i++) {
                if (gottenlist.get(i).split("\\|\\|")[0].equals(userId)) {
                    gottenlist.set(i, marray);
                }
            }


            editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
            editor.apply();
        } catch (Exception e) {

        }
    }

    public void replaceOrAddList(String key, String marray) {
        // 암호화 이전 기존로직
//        boolean checking = false;
//
//        String[] mylist = TextUtils.split(mSharedPreferences.getString(key, ""), "‚‗‚");
//        ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//
//        String userId=marray.split("\\|\\|")[0];
//        if(StringUtil.isValidString(userId)){
//            for (int i = 0; i < gottenlist.size(); i++) {
//                if (gottenlist.get(i).split("\\|\\|")[0].equals(userId)) {
//                    checking = true;
//                    gottenlist.set(i, marray);
//                }
//            }
//
//            if (!checking) {
//                gottenlist.add(marray);
//            }
//
//            editor.putString(key, TextUtils.join("‚‗‚", gottenlist));
//            editor.apply();
//        }

        try {
            boolean checking = false;

            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
            SharedPreferences.Editor editor = mSharedPreferences.edit();

            String userId = marray.split("\\|\\|")[0];
            if (StringUtil.isValidString(userId)) {
                for (int i = 0; i < gottenlist.size(); i++) {
                    if (gottenlist.get(i).split("\\|\\|")[0].equals(userId)) {
                        checking = true;
                        gottenlist.set(i, marray);
                    }
                }

                if (!checking) {
                    gottenlist.add(marray);
                }

                editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
                editor.apply();
            }
        } catch (Exception e) {

        }

    }

    public void cleanList(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, "");
        editor.apply();
    }

    public ArrayList<String> getList(String key) {
        // 복호화 이전 기존로직
//        // the comma like character used below is not a comma it is the SINGLE
//        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
//        // seprating the items in the list
//        String[] mylist = TextUtils.split(mSharedPreferences.getString(key, ""), "‚‗‚");
//        ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
//        return gottenlist;
        ArrayList<String> gottenlist = new ArrayList<>();
        try {
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            gottenlist = new ArrayList<String>(Arrays.asList(mylist));
        } catch (Exception e) {

        }
        return gottenlist;
    }


    /*
     * jhlee
     * 하단 부분은 미 사용부분으로 암호화/복호화 작업 안되어있음 필요시 암호화/복호화 하여 사용해야 함
     */
    public void putListInt(String key, ArrayList<Integer> marray, Context context) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Integer[] mystringlist = marray.toArray(new Integer[marray.size()]);
        // the comma like character used below is not a comma it is the SINGLE
        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
        // seprating the items in the list
        editor.putString(key, TextUtils.join("‚‗‚", mystringlist));
        editor.apply();
    }

    public ArrayList<Integer> getListInt(String key,
                                         Context context) {
        // the comma like character used below is not a comma it is the SINGLE
        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
        // seprating the items in the list
        String[] mylist = TextUtils
                .split(mSharedPreferences.getString(key, ""), "‚‗‚");
        ArrayList<String> gottenlist = new ArrayList<String>(
                Arrays.asList(mylist));
        ArrayList<Integer> gottenlist2 = new ArrayList<Integer>();
        for (int i = 0; i < gottenlist.size(); i++) {
            gottenlist2.add(Integer.parseInt(gottenlist.get(i)));
        }

        return gottenlist2;
    }

    public void putListBoolean(String key, ArrayList<Boolean> marray) {
        ArrayList<String> origList = new ArrayList<String>();
        for (Boolean b : marray) {
            if (b == true) {
                origList.add("true");
            } else {
                origList.add("false");
            }
        }
        putList(key, origList);
    }

    public ArrayList<Boolean> getListBoolean(String key) {
        ArrayList<String> origList = getList(key);
        ArrayList<Boolean> mBools = new ArrayList<Boolean>();
        for (String b : origList) {
            if (b.equals("true")) {
                mBools.add(true);
            } else {
                mBools.add(false);
            }
        }
        return mBools;
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    public boolean getBoolean(String key,boolean defalutValue) {
        return mSharedPreferences.getBoolean(key, defalutValue);
    }

    // Default True
    public boolean getBooleanReverse(String key) {
        return mSharedPreferences.getBoolean(key, true);
    }

    public void putFloat(String key, float value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public float getFloat(String key) {
        return mSharedPreferences.getFloat(key, 0f);
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public Boolean deleteImage(String path) {
        File tobedeletedImage = new File(path);
        Boolean isDeleted = tobedeletedImage.delete();
        return isDeleted;
    }

    public void clear() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public Map<String, ?> getAll() {
        return mSharedPreferences.getAll();
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }


}
