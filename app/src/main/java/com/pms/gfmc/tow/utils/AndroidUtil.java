package com.pms.gfmc.tow.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.pms.gfmc.tow.GlobalApplication;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by KimJehyun on 2017. 1. 11..
 */

public class AndroidUtil {

    private static String mDeviceID;

    public static boolean isInKeyguardRestrictedInputMode() {
        KeyguardManager km = (KeyguardManager) GlobalApplication.getContext().getSystemService(Context.KEYGUARD_SERVICE);

        return km.inKeyguardRestrictedInputMode();
    }

    public static String getAndroidOSVersion() {
        return String.valueOf(Build.VERSION.RELEASE);
    }

    public static String getModel() {
        return Build.MODEL;
    }
    public static String getAppVersion(Context context)
    {
        String version = "";
        String name = "";
        name = context.getPackageName();
        try
        {
            PackageInfo i = context.getPackageManager().getPackageInfo(name, 0);
            version = i.versionName != null ? i.versionName : "";
        }
        catch(android.content.pm.PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
            version = "";
        }
        return version;
    }

    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public static String getVersionName() {
        String returnVal = "";
        try {
            returnVal = GlobalApplication.getContext().getPackageManager()
                    .getPackageInfo(GlobalApplication.getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return returnVal;
    }

    public static String getVersionName(Context context) {
        String returnVal = "";
        try {
            returnVal = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // Auto-generated catch block
            e.printStackTrace();
        }
        return returnVal;
    }

    public static String getLanguage() {
        // Auto-generated method stub
        return Locale.getDefault().getLanguage();
    }

    @SuppressLint("MissingPermission")
    public static String getDeviceLine1Number(Context context) {
        try {
            return ((TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE))
                    .getLine1Number();
        } catch (Exception e) {
            Log.e(context.getClass().getSimpleName(), e.toString());
        }
        return null;
    }

    public static String getIMEI(Context context, boolean isEncrypt) {
        String deviceID = "";
        try {
            deviceID = getDeviceId();
            if (deviceID != null && !"".equals(deviceID)) {
                if (isEncrypt) {
                    return StringUtil.md5(deviceID);
                } else {
                    return deviceID;
                }
            }
        } catch (Exception e) {
            Log.e(context.getClass().getSimpleName(), e.toString());
        }
        return deviceID;
    }

//    @SuppressLint("MissingPermission")
//    public static String getDeviceId() {
//        try {
//            return ((TelephonyManager) GlobalApplication.getContext().getSystemService(
//                    Context.TELEPHONY_SERVICE)).getDeviceId();
//        } catch (Exception e) {
////			Log.e("getDeviceId", "getDeviceId..." + e.getMessage());
//        }
//        return null;
//    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId() {
        if (TextUtils.isEmpty(mDeviceID)) {
            mDeviceID = Settings.Secure.getString(GlobalApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return mDeviceID;
    }


    public static String getAppVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
//				Log.e("getAppVersionName", "APPLICATION NAME : " + packageInfo.versionName);
                return packageInfo.versionName;
            }
        } catch (Exception e) {
//			Log.e("getAppVersionName", "error to load app version information..." + e.getMessage());
        }
        return "";
    }

    /*
     * public static String getAppVersionCode(Context context) { try {
     * PackageInfo packageInfo = context.getPackageManager().
     * getPackageInfo(context.getPackageName(), 0); if (packageInfo != null) {
     * if(NateOnLib.LOG_ENABLE) LogUtil.i("APPLICATION CODE : " +
     * packageInfo.versionCode); return String.valueOf(packageInfo.versionCode);
     * } } catch (Exception e) { if(NateOnLib.LOG_ENABLE)
     * LogUtil.e("error to load app version information..." + e.getMessage()); }
     * return ""; }
     */

    public static String getAndroidId(Context context) {
        String androidId = "";
        try {
            androidId = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
        }
//		Log.d("getAndroidId", "androidId : " + androidId);
        return androidId;
    }

    // ?좎럩伊볩옙?먯삕占썼?占? ?좎룞?셲erial --> ?좎럩裕녶뜝?몄뿉?좎룞?숋옙瑜곴텕嶺뚯쉻?쇿뜝?덈늉占쏙옙
    public static String getSerial() {
        String serial = "";
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
        } catch (Exception e) {
//			Log.e("getSerial", "getSerial..." + e.getMessage());
        }
//		Log.d("getSerial", "serial : " + serial);
        return serial;
    }

    // ?좎럩伊볩옙?먯삕占썼?占? 嶺뚮엪?셙ddress
//    public static String getMacAddress(Context context) {
//        String mac = "";
//        try {
//            WifiManager wm = (WifiManager) context
//                    .getSystemService(Context.WIFI_SERVICE);
//            mac = wm.getConnectionInfo().getMacAddress();
//        } catch (Exception ignored) {
//        }
//        return mac;
//    }

    public static int dipToPixel(Context context, int dip) {
        return (int) (dip * context.getResources().getDisplayMetrics().density + 0.5f);
    }

    public static boolean isMobileNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo wifiNetwork = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                return wifiNetwork != null && wifiNetwork.isConnected();
            }
        }

        return false;
    }

    public static boolean is3GMobile(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo threeGNetwork = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                return threeGNetwork != null && threeGNetwork.isConnected();
            }
        }

        return false;
    }

    public static boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo wifi = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                return wifi != null && wifi.isAvailable() && wifi.isConnected();
            }
        }

        return false;
    }

    public static void hideKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public static void showKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(view, 0);
                // imm.showSoftInputFromInputMethod(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public static void showKeyboard2(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInputFromWindow(view.getApplicationWindowToken(),
                        InputMethodManager.SHOW_FORCED, 0);
            }
        } catch (Exception e) {
        }
    }

    public static void sendSms(Context context, boolean isShow, String phoneNo) {
        if (isShow && phoneNo != null && !"".equals(phoneNo)) {
            Uri msgUri = Uri.parse("smsto:" + phoneNo);
            try {
                Intent intent = new Intent(Intent.ACTION_SENDTO, msgUri);
                context.startActivity(intent);
            } catch (Exception e) {

                return;
            }
        } else {

            return;
        }
    }

    public static void sendEmail(Context context, boolean isShow, String email) {
        if (isShow && email != null && !"".equals(email)) {
            Uri mailUri = Uri.parse("mailto:" + email);
            try {
                Intent intent = new Intent(Intent.ACTION_SENDTO, mailUri);
                context.startActivity(intent);
            } catch (Exception e) {

                return;
            }
        } else {

            return;
        }
    }

    public static void callDialar(Context context, boolean isShow,
                                  String phoneNo) {
        if (isShow && phoneNo != null && !"".equals(phoneNo)) {
            Uri callUri = Uri.parse("tel:" + phoneNo);
            try {
                Intent intent = new Intent(Intent.ACTION_DIAL, callUri);
                context.startActivity(intent);
            } catch (Exception e) {
                return;
            }
        } else {

            return;
        }
    }

    /**
     * Bold嶺뚳퐦?쇿뜝?꾨궚占쎌뮋?쇿뜝?숈삕占쎈쪋???좎럥遊억옙占썲뜝?뚮츇?좑옙?ル??숋옙占쎌삕?좑옙?띠룊?숁묾?숈삕占쎄퀣夷?
     * Bold嶺뚳퐢占썲뜝占썲뜝?щ걠占쎌닂?쇿뜝占? *
     *
     * @param textView
     */
    public static void setBold(TextView textView) {
        if (textView != null) {
            textView.setPaintFlags(textView.getPaintFlags()
                    | Paint.FAKE_BOLD_TEXT_FLAG);
        }
    }

    public static boolean isPhoneBusy(Context context) {
        TelephonyManager tv = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        if (tv == null)
            return false;

//		Log.d("isPhoneBusy", "Service is PhoneBusy info(call:" + (tv.getCallState() != TelephonyManager.CALL_STATE_IDLE));

        return tv.getCallState() != TelephonyManager.CALL_STATE_IDLE;
    }

    public static boolean isForeToBackground(Context context) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> info = manager.getRunningTasks(1);
        ComponentName topActivity = info.get(0).topActivity;

        return !topActivity.getPackageName().equals(context.getPackageName());
    }

    public static String getTopActivity(Context context) {
        String topAct = "";
        try {
            ActivityManager manager = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> info = manager.getRunningTasks(1);
            ComponentName topActivity = info.get(0).topActivity;
            topAct = topActivity.getShortClassName();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return topAct;
    }


    public static boolean isRunning(Context context) {
        int maxNum = 256; // maximum task num
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfos = am.getRunningTasks(maxNum);
        for (ActivityManager.RunningTaskInfo rti : taskInfos) {
            if (rti == null || rti.topActivity == null)
                continue;
            String strPackage = rti.topActivity.getPackageName();
            if (strPackage.equals(context.getPackageName()))
                return true;
        }
        return false;
    }

    public static String isTablet(Context context) {
        // This hacky stuff goes away when we allow users to target devices
        int xlargeBit = 4; // Configuration.SCREENLAYOUT_SIZE_XLARGE;  // upgrade to HC SDK to get this
        Configuration config = context.getResources().getConfiguration();
        if ((config.screenLayout & xlargeBit) == xlargeBit) {
            return "04";
        }
        return "03";
    }

    public static boolean isPhone(Context context) {
        // This hacky stuff goes away when we allow users to target devices
        int xlargeBit = 4; // Configuration.SCREENLAYOUT_SIZE_XLARGE;  // upgrade to HC SDK to get this
        Configuration config = context.getResources().getConfiguration();
        return (config.screenLayout & xlargeBit) != xlargeBit;
    }


    @SuppressLint("MissingPermission")
    public static String createUUID(Context context) {
        UUID deviceUuid = null;

        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            final String tmDevice = "" + tm.getDeviceId();
            final String tmSerial = "" + tm.getSimSerialNumber();
            final String androidId = "" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (deviceUuid == null) {
            return "";
        }

        return deviceUuid.toString();
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connMgr.getActiveNetworkInfo();
        if (info == null) {
            return false;
        }
        return info.isConnected();
    }

    /*
     * http://diyall.tistory.com/846
     */
	/*public static String getMarketVersionName(Context context)
	{
		MarketSession session = new MarketSession();
		session.login(email, password)

	}*/


    public static boolean isVersionUpdate(String version) {

        String[] s_versions = version.split("\\.");
        String[] a_verions = getVersionName().split("\\.");

        int s_first = Integer.parseInt(s_versions[0]);
        int s_second = Integer.parseInt(s_versions[1]);
        int s_third = Integer.parseInt(s_versions[2]);

        int a_first = Integer.parseInt(a_verions[0]);
        int a_second = Integer.parseInt(a_verions[1]);
        int a_third = Integer.parseInt(a_verions[2]);

        if (s_first > a_first) {
            return true;
        } else {
            if (s_second > a_second) {
                return true;
            } else {
                return s_third > a_third;
            }
        }

    }


//	public static void showAlert(Context context, String msg, String positiveBtn, DialogInterface.OnClickListener positiveBtnListener, String negativeBtn,
//			DialogInterface.OnClickListener negativeBtnListener) {
//		AlertDialog.Builder ad = null;
//		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
//			ad = new AlertDialog.Builder(context, R.style.DialogThemeHoloLight).setCancelable(false).setTitle("BeautyPoint");
//		}else{
//			ad = new AlertDialog.Builder(context).setCancelable(false).setTitle("BeautyPoint");
//		}
//
//		ad.setPositiveButton(positiveBtn, positiveBtnListener);
//		if (negativeBtn != null) {
//			ad.setNegativeButton(negativeBtn, negativeBtnListener);
//		}
//		ad.setMessage(msg);
//
//		AlertDialog dialog = ad.show();
//		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
//		messageText.setGravity(Gravity.CENTER);
//		dialog.show();
//	}


    public static String checkAppHash(Context context) {
        //key hash 비교.
        String returnStr = "";
        try {
            Signature[] sigs = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (Signature sig : sigs) {
                Log.d("myapp", "Signature hashcode : " + sig.hashCode());
                returnStr = Integer.toString(sig.hashCode());
            }
        } catch (Exception e) {
            Log.d("myapp", e.getMessage());
        }
        /*
        String returnStr="";
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
//            Log.d("MY KEY HASH 1: " + info);
//            Log.d("MY KEY HASH 2: " + info.signatures);
//            Log.d("MY KEY HASH 3: " + info.packageName);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                Log.d("MY KEY HASH 4: " + md);
//                md.update(signature.toByteArray());

//                byte[] digest = md.digest();
//
//                for(int i = 0; i < digest.length; i++) {
//                    Log.d("MY KEY HASH 55: " + digest[i]);
//                    returnStr += String.format("%02X", digest[i]);
//                }
//                Base64.encodeToString(md.digest(), Base64.DEFAULT);

            try{
                for (int i = 0; i < info.signatures.length; i++) {
                    Signature signature = info.signatures[i];
//                    BPLog.d("MY KEY HASH 4:" + signature.toCharsString());

//                    MessageDigest md = MessageDigest.getInstance("SHA1");
//                    md.update(signature.toByteArray());
//                    returnStr = Base64.encodeToString(md.digest(),Base64.DEFAULT);
//                    returnStr = StringUtil.isValidString(returnStr)?returnStr.trim():"";


                    MessageDigest sh = MessageDigest.getInstance("SHA-256");
                    sh.update(signature.toByteArray());
                    byte byteData[] = sh.digest();
                    StringBuffer sb = new StringBuffer();

                    for (int j = 0; j < byteData.length; j++) {
                        sb.append(Integer.toString((byteData[j] & 0xff) + 0x100, 16).substring(1));
                    }
//                    rtnSHA = sb.toString();


                    Log.d("test", "MY KEY HASH 44: " + sb.toString());
                    returnStr=sb.toString();
                }
            } catch (NoSuchAlgorithmException nsae){
                nsae.printStackTrace();
            }

            Log.d("System","c05"+returnStr+"0a1");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
*/
        return returnStr;
    }

    private static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                key = new SecretKeySpec((keyString).getBytes(StandardCharsets.UTF_8), algo);
            } else {
                key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            }
            Mac mac = Mac.getInstance(algo);
            mac.init(key);
            byte[] bytes = new byte[0];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                bytes = mac.doFinal(msg.getBytes(StandardCharsets.US_ASCII));
            } else {
                bytes = mac.doFinal(msg.getBytes("US-ASCII"));
            }


            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return digest;
    }

    //170612 jhkim. 평문화 노출만 안 되면 되기에 그냥 간단히 처리.
    public static String encrypt(String str) {
        // ******DEFAULT -> 76자가 넘어가면  \n을 자동으로 시켜버림. NO_WRAP은 이 \n시키는 것을 제외시키는 것이어서 NO_WRAP으로 해야함. 아니면 통신하는 도중에 에러남....******
        String base64 = "";
        try {
            byte[] data = new byte[0];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                data = str.getBytes(StandardCharsets.UTF_8);
            } else {
                data = str.getBytes("UTF-8");
            }
            base64 = Base64.encodeToString(data, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64;
    }

    //180424 jmlee. 모의해킹에서 고객 데이터 평문으로 저장시키는 이슈 처리 (base64 인코딩 로직 추가)
    public static String getBase64encode(String content) {
        if (!StringUtil.isEmpty(content)) {
            return "ANGEL:::" + Base64.encodeToString(content.getBytes(), 0);
        } else {
            return "";
        }

    }

    // 180424 jmlee. 모의해킹에서 고객 데이터 평문으로 저장시키는 이슈 처리 (base64 디코딩 로직 추가)
    public static String getBase64decode(String content) {
        if (isValidBase64(content)) {
            return new String(Base64.decode(content.replaceFirst("ANGEL:::", ""), 0));
        } else {
            return content;
        }
    }

    public static boolean isValidBase64(final String s) {
        return s.contains("ANGEL:::");
    }


    //170710 jhkim. 스테이스바 높이 가져오기
    public static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return (resourceId > 0) ? resources.getDimensionPixelSize(resourceId) : 0;
    }

    public static float dpToPx(Context context, float dp) {
        if (context == null) {
            return -1;
        }
        return dp * context.getResources().getDisplayMetrics().density;
    }


    public static float pxToDp(Context context, float px) {
        if (context == null) {
            return -1;
        }
        return px / context.getResources().getDisplayMetrics().density;
    }
}
