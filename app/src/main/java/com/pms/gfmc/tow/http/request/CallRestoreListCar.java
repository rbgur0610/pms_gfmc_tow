package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.RestoreListModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallRestoreListCar extends APICall {

    public ActionResponseListener<RestoreListModel> actionResponseListener;
    private HashMap<String, String> map;

    public CallRestoreListCar(Context context, HashMap<String, String> map, ActionResponseListener<RestoreListModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
        this.map = map;
    }

    @Override
    public void run() {

        call = APIClient.getInstance().getRestoreList(map);
        call.enqueue(new Callback<RestoreListModel>() {
            @Override
            public void onResponse(Call<RestoreListModel> call, Response<RestoreListModel> response) {
                callDone(response, CallRestoreListCar.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<RestoreListModel> call, Throwable t) {
                callDone(t, CallRestoreListCar.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
