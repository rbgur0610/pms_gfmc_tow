package com.pms.gfmc.tow.screen.retore;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.databinding.ActivityRestoreBinding;
import com.pms.gfmc.tow.databinding.ActivityRestoreListBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.RestoreListModel;
import com.pms.gfmc.tow.http.model.Tocim01;
import com.pms.gfmc.tow.http.model.WaitListModel;
import com.pms.gfmc.tow.http.request.CallRestoreListCar;
import com.pms.gfmc.tow.http.request.CallToInListCar;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.adapter.RestoreListAdapter;
import com.pms.gfmc.tow.screen.adapter.WaitListAdapter;
import com.pms.gfmc.tow.screen.payment.PaymentActivity;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;
import com.pms.gfmc.tow.view.DateRangePicker;

import java.util.ArrayList;
import java.util.HashMap;

public class RestoreListActivity extends BaseActivity {

    private ActivityRestoreBinding binding;
    private RestoreListAdapter mAdapter;
    private String cartype = "";
    private ArrayList<String> carTypeList = new ArrayList<>();
    private ArrayList<String> carTypeCdList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityRestoreBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnBack.setOnClickListener(view -> finish());


        mAdapter = new RestoreListAdapter(mContext);
        binding.rvList.setAdapter(mAdapter);

        carTypeList = new ArrayList<>();
        carTypeCdList = new ArrayList<>();

        carTypeCdList.add("");
        carTypeList.add("선택안함");

        ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
        for (String item : basicCodes) {
            if (item.contains("T018")) {
                String[] carType = item.split("-");
                carTypeCdList.add(carType[1]);
                carTypeList.add(carType[2]);
            }
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, carTypeList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spCarType.setAdapter(adapter);
        binding.spCarType.setOnItemSelectedListener(new carTypeSelectedListener());


        binding.btnSearch.setOnClickListener(v -> {
            AndroidUtil.hideKeyboard(mContext, binding.edCarnum);
            reqtList();
        });

        binding.edCarnum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                AndroidUtil.hideKeyboard(mContext, binding.edCarnum);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    reqtList();
                    return true;
                }
                return false;
            }
        });


        binding.btnConfirm.setOnClickListener(v -> {
            if (mAdapter.getSelectItems().size() == 0) {
                Toast.makeText(mContext, "선택된 항목이 없습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            String[] carNo = new String[mAdapter.getSelectItems().size()];
            String[] in_no =  new String[mAdapter.getSelectItems().size()];

            int index = 0;
            for (Tocim01 item : mAdapter.getSelectItems()) {
                carNo[index] = item.CARNO;
                in_no[index] = item.INNO;
                index++;
            }

            Intent i = new Intent(mContext, PaymentActivity.class);
            i.putExtra("CAR_NO", carNo);
            i.putExtra("INNO", in_no);
            i.putExtra("SELECT_TYPE","MULTI");
            startActivity(i);
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        reqtList();
    }


    private void reqtList() {

        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("carno", binding.edCarnum.getText().toString());
        map.put("cartype", cartype);

        APICallRuller.getInstance().addCall(new CallToInListCar(mContext, map, new ActionResponseListener<WaitListModel>() {
            @Override
            public void onResponse(WaitListModel mData) {
                try {

                    if (mData != null && mData.tocim01 != null && mData.tocim01.size() > 0) {
                        mAdapter.setListData(mData.tocim01, !StringUtil.isEmpty(cartype));

                        if (!StringUtil.isEmpty(cartype)) {
                            binding.btnConfirm.setVisibility(View.VISIBLE);
                        } else {
                            binding.btnConfirm.setVisibility(View.GONE);
                        }

                    } else {
                        mAdapter.setListData(null, false);
                        Toast.makeText(mContext, "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    public class carTypeSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            cartype = carTypeCdList.get(position);
            reqtList();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // Do nothing
        }
    }
}
