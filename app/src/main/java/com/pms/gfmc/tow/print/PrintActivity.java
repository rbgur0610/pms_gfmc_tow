/**
 *
 */
package com.pms.gfmc.tow.print;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.utils.Logger;
import com.woosim.printer.WoosimBarcode;

import org.apache.http.util.ByteArrayBuffer;

import java.io.UnsupportedEncodingException;
import java.util.Set;

/**
 * @author Baek
 *
 */
@SuppressLint("MissingPermission")
public class PrintActivity extends BaseActivity {

    private static final String TAG = "PrintActivity";
    private static final boolean D = true;
    private static final byte EOT = 0x04;
    private static final byte LF = 0x0a;
    private static final byte ESC = 0x1b;
    private static final byte GS = 0x1d;
    private static final byte[] CMD_INIT_PRT = {ESC, 0x40};        // Initialize printer (ESC @)

    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private byte mCharsize = 0x00;
    private byte mJustification = 0x00;
    private String endedMessage = "";
    private int mFlag;
    private PrintData mData;
    private boolean stopFlag = false;


    @Override
    public void Print() {
        super.Print();
        showProgressDialog();
        printText(setText(mData));
        completePrint();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.print_activity);
        showProgressDialog();
        // Get local Bluetooth adapter
        init();

        if (!bleSetting()) closedActivity();


        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mPrintService == null || mWoosim == null) {
                setupPrint();
            }
            printSetting();
        }
    }

    private void printSetting() {
        if (mPrintService != null && mPrintService.getState() == BluetoothPrintService.STATE_CONNECTED) {
            Print();
        } else {
            pServiceSetting();
//            onResume();
            Print();
        }
    }

    private void init() {

        View touch_finish = (View) findViewById(R.id.touch_finish);
        touch_finish.setOnClickListener(v -> finish());

        mFlag = getIntent().getIntExtra("flag", 1);
        mData = (PrintData) getIntent().getSerializableExtra("data");
        endedMessage = mData.type;
//        switch (mData.type) {
//            case "FE":
//                endedMessage = "단속등록";
//                break;
//            case "RE":
//                endedMessage = "재출력";
//                break;
//        }
    }

    private String setText(PrintData data) { //PrintActivity
        StringBuffer mStrBuffer = new StringBuffer();
        mStrBuffer.append(data.carNum + "\n\n");
        if (data.curbDate != null)
            mStrBuffer.append(data.curbDate.replaceAll("\n", " ") + "\n\n");
        mStrBuffer.append(data.location + "\n\n");
        if (data.group != null)
            mStrBuffer.append(data.group + "\n\n");

        if (data.PayAmt != null)
            mStrBuffer.append(data.PayAmt + "\n\n");
//		mStrBuffer.append(data.ipgm_date+ "\n\n");
//        mStrBuffer.append("발행일로부터 15일 이내 납부\n\n");
//        if (!StringUtil.isEmpty(mData.bank_vcnt) && !StringUtil.isEmpty(mData.bank_name)) {
//            mStrBuffer.append(mData.bank_vcnt + "(" + mData.bank_name + ")" + "\n\n");
//        } else {
//            mStrBuffer.append("가상계좌 미존재" + "\n\n");
//        }
        mStrBuffer.append("인천광역시 남동구" + "\n\n");
        Log.e("", mStrBuffer.toString());
        return mStrBuffer.toString();
    }


    @Override
    public synchronized void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mPrintService != null && mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            // Get a set of currently paired devices
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                Log.e("pairedDevices", pairedDevices.size() + "");
                for (BluetoothDevice device : pairedDevices) {
                    String saveDevice = PrefData.getInstance(mContext).getString(PrefData.PRINT_NAME, "");
                    Log.e(">>>>", " device.getName() >>> " + device.getName() + "      saveDevice  : " + saveDevice);
                    if (saveDevice.equals(device.getName())) {
                        BluetoothDevice printDevice = mBluetoothAdapter.getRemoteDevice(device.getAddress());
                        // Attempt to connect to the device
                        mPrintService.connect(printDevice, false);
                        stopFlag = true;
                    }
                }
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            } else {
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            }
        }


        System.out.println("mFlag = " + mFlag);
    }

    public void closedActivity() {
        super.closedActivity();

        closeProgressDialog();
//        Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
        finish();
//        new CommonDialog(this).getDialog(endedMessage + " 완료.\n\r프린트 연결 취소.", new DialogButton("확인") {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                // TODO Auto-generated method stub
//                Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
////                Intent aIntent = new Intent(PrintActivity.this, MainActivity.class);
////                aIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                startActivity(aIntent);
//                finish();
//            }
//        }).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        closeProgressDialog();
        Logger.e("Print onDestroy");
    }


    /**
     * Print data.
     * @param data  A byte array to print.
     */
    private void sendData(byte[] data) {
        // Check that we're actually connected before trying printing
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (data.length > 0)
            mPrintService.write(data);
    }

    /**
     * On click function for sample print button. 
     */
    /*public void onCheckboxClicked(View v) {
    	boolean checked = ((CheckBox) v).isChecked();
    	
    	switch(v.getId()) {
    	case R.id.cbx_emphasis:
    		if (checked) mEmphasis = true;
    		else mEmphasis = false;
    		break;
    	case R.id.cbx_underline:
    		if (checked) mUnderline = true;
    		else mUnderline = false;
    		break;
    	}
    }*/
    
    /*public void onRadioButtonClicked(View v) {
    	boolean checked = ((RadioButton) v).isChecked();
    	
    	switch(v.getId()) {
    	case R.id.rbtn_left: 
    		if (checked) mJustification = (byte)0x00;
    		break;
    	case R.id.rbtn_center:
    		if (checked) mJustification = (byte)0x01;
    		break;
    	case R.id.rbtn_right:
    		if (checked) mJustification = (byte)0x02;
    		break;
    	}
    }*/
    private void printHeader() {
        String content;
        if (mFlag == 2) {
            content = "[1차단속 경고장]\n";
        } else if (mFlag == 3) {
            content = "[2차단속 경고장]\n";
        } else if (mFlag == 4) {
            content = "[3차단속 경고장]\n";
        } else if (mFlag == 5) {
            content = "[출입금지 경고장]\n";
        } else {
            content = "[부정주차 경고장]\n";
        }
        byte[] text;
        try {
            text = content.getBytes("EUC-KR");
            if (text.length == 0) return;

            mJustification = (byte) 0x01;
            ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

            byte[] cmd = {ESC, 0x45, (byte) (mEmphasis ? 1 : 0),        // ESC E
                    ESC, 0x2D, (byte) (mUnderline ? 1 : 0),    // ESC -
                    GS, 0x21, mCharsize,                        // GS !
                    ESC, 0x61, mJustification                    // ESC a
            };

            buffer.append(cmd, 0, cmd.length);
            buffer.append(text, 0, text.length);
            buffer.append(LF);

            sendData(CMD_INIT_PRT);
            sendData(buffer.toByteArray());
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void printText(String content) {
        //EditText editText = (EditText) findViewById(R.id.edit_text);

        byte[] text;
        try {
            text = content.getBytes("EUC-KR");
            if (text.length == 0) return;
            mJustification = 0x00;
            ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

            byte[] cmd = {ESC, 0x45, (byte) (mEmphasis ? 1 : 0),        // ESC E
                    ESC, 0x2D, (byte) (mUnderline ? 1 : 0),    // ESC -
                    GS, 0x21, mCharsize,                        // GS !
                    ESC, 0x61, mJustification,
            };                // ESC a

            buffer.append(cmd, 0, cmd.length);
            buffer.append(text, 0, text.length);
            buffer.append(LF);

            sendData(CMD_INIT_PRT);
            sendData(buffer.toByteArray());
            movementPaper();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void movementPaper() {
        ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
        byte[] cmd = {ESC, 0x7A, ESC, 0x79};
        buffer.append(cmd, 0, cmd.length);
        sendData(buffer.toByteArray());
    }

    private void completePrint() {
    	/*
    	new CommonDialog(this).getDialog("출력이 완료되었습니다.", new DialogButton("확인") {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				showSmsDialog();
			}
		}).show();
		*/
        showSmsDialog();
    }

    private void showSmsDialog() {
    	/*
    	new CommonDialog(this, CommonDialog.DLG_TYPE_2).getDialog("문자 발송하시겠습니까?", new DialogButton("발송") {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				new RegSmsAction(PrintActivity.this, mSmsListener).execute(mData.control_num);
			}
		}, new DialogButton("완료") {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				completeCurb(false);
			}
		}).show();
		*/
        completeCurb(false);
    }

    private void completeCurb(boolean smsFlag) {
        closeProgressDialog();
        if (smsFlag) {
//            Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
            finish();

//            new CommonDialog(this).getDialog("문자발송 및 단속처리 완료되었습니다.", new DialogButton("확인") {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    // TODO Auto-generated method stub
//                    Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
//                    Intent aIntent = new Intent(PrintActivity.this, MainActivity.class);
//                    aIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(aIntent);
//                }
//            }).show();
        } else {
//            Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
            finish();

//            new CommonDialog(this).getDialog(endedMessage + " 완료되었습니다.", new DialogButton("확인") {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    // TODO Auto-generated method stub
//                    Toast.makeText(PrintActivity.this, endedMessage + "완료", Toast.LENGTH_SHORT).show();
//                    Intent aIntent;
//                    switch (mData.type) {
//                        case "FE":
////                            aIntent = new Intent(PrintActivity.this, MainActivity.class);
////                            aIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                            startActivity(aIntent);
//                            finish();
//                            break;
//                        case "RE":
//                            //this.onBackPressed();
//                            finish();
//                            break;
//                    }
//                }
//            }).show();
        }
    }

    /**
     * On click function for barcode print button. 
     */
    public void print1DBarcode(View v) {
        final byte[] barcode = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30};
        final byte[] barcode8 = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37};
        ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

        final String title1 = "UPC-A Barcode\r\n";
        byte[] UPCA = WoosimBarcode.createBarcode(WoosimBarcode.UPC_A, 2, 60, true, barcode);
        buffer.append(title1.getBytes(), 0, title1.getBytes().length);
        buffer.append(UPCA, 0, UPCA.length);
        buffer.append(LF);

        final String title2 = "UPC-E Barcode\r\n";
        byte[] UPCE = WoosimBarcode.createBarcode(WoosimBarcode.UPC_E, 2, 60, true, barcode);
        buffer.append(title2.getBytes(), 0, title2.getBytes().length);
        buffer.append(UPCE, 0, UPCE.length);
        buffer.append(LF);

        final String title3 = "EAN13 Barcode\r\n";
        byte[] EAN13 = WoosimBarcode.createBarcode(WoosimBarcode.EAN13, 2, 60, true, barcode);
        buffer.append(title3.getBytes(), 0, title3.getBytes().length);
        buffer.append(EAN13, 0, EAN13.length);
        buffer.append(LF);

        final String title4 = "EAN8 Barcode\r\n";
        byte[] EAN8 = WoosimBarcode.createBarcode(WoosimBarcode.EAN8, 2, 60, true, barcode8);
        buffer.append(title4.getBytes(), 0, title4.getBytes().length);
        buffer.append(EAN8, 0, EAN8.length);
        buffer.append(LF);

        final String title5 = "CODE39 Barcode\r\n";
        byte[] CODE39 = WoosimBarcode.createBarcode(WoosimBarcode.CODE39, 2, 60, true, barcode);
        buffer.append(title5.getBytes(), 0, title5.getBytes().length);
        buffer.append(CODE39, 0, CODE39.length);
        buffer.append(LF);

        final String title6 = "ITF Barcode\r\n";
        byte[] ITF = WoosimBarcode.createBarcode(WoosimBarcode.ITF, 2, 60, true, barcode);
        buffer.append(title6.getBytes(), 0, title6.getBytes().length);
        buffer.append(ITF, 0, ITF.length);
        buffer.append(LF);

        final String title7 = "CODEBAR Barcode\r\n";
        byte[] CODEBAR = WoosimBarcode.createBarcode(WoosimBarcode.CODEBAR, 2, 60, true, barcode);
        buffer.append(title7.getBytes(), 0, title7.getBytes().length);
        buffer.append(CODEBAR, 0, CODEBAR.length);
        buffer.append(LF);

        sendData(CMD_INIT_PRT);
        sendData(buffer.toByteArray());
    }

    public void print2DBarcode(View v) {
        final byte[] barcode = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30};
        ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

        final String title1 = "PDF417 2D Barcode\r\n";
        byte[] PDF417 = WoosimBarcode.create2DBarcodePDF417(2, 3, 4, 2, false, barcode);
        buffer.append(title1.getBytes(), 0, title1.getBytes().length);
        buffer.append(PDF417, 0, PDF417.length);
        buffer.append(LF);

        final String title2 = "DATAMATRIX 2D Barcode\r\n";
        byte[] dataMatrix = WoosimBarcode.create2DBarcodeDataMatrix(0, 0, 4, barcode);
        buffer.append(title2.getBytes(), 0, title2.getBytes().length);
        buffer.append(dataMatrix, 0, dataMatrix.length);
        buffer.append(LF);

        final String title3 = "QR-CODE 2D Barcode\r\n";
        byte[] QRCode = WoosimBarcode.create2DBarcodeQRCode(0, (byte) 0x4d, 5, barcode);
        buffer.append(title3.getBytes(), 0, title3.getBytes().length);
        buffer.append(QRCode, 0, QRCode.length);
        buffer.append(LF);

        final String title4 = "Micro PDF417 2D Barcode\r\n";
        byte[] microPDF417 = WoosimBarcode.create2DBarcodeMicroPDF417(2, 2, 0, 2, barcode);
        buffer.append(title4.getBytes(), 0, title4.getBytes().length);
        buffer.append(microPDF417, 0, microPDF417.length);
        buffer.append(LF);

        final String title5 = "Truncated PDF417 2D Barcode\r\n";
        byte[] truncPDF417 = WoosimBarcode.create2DBarcodeTruncPDF417(2, 3, 4, 2, false, barcode);
        buffer.append(title5.getBytes(), 0, title5.getBytes().length);
        buffer.append(truncPDF417, 0, truncPDF417.length);
        buffer.append(LF);

        // Maxicode can be printed only with RX version
        final String title6 = "Maxicode 2D Barcode\r\n";
        final byte[] mxcode = {0x41, 0x42, 0x43, 0x44, 0x45, 0x31, 0x32, 0x33, 0x34, 0x35, 0x61, 0x62, 0x63, 0x64, 0x65};
        byte[] maxCode = WoosimBarcode.create2DBarcodeMaxicode(4, mxcode);
        buffer.append(title6.getBytes(), 0, title6.getBytes().length);
        buffer.append(maxCode, 0, maxCode.length);
        buffer.append(LF);

        sendData(CMD_INIT_PRT);
        sendData(buffer.toByteArray());
    }

    public void printGS1Databar(View v) {
        final byte[] data = {0x30, 0x30, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30};
        ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

        final String title0 = "GS1 Databar type0\r\n";
        byte[] gs0 = WoosimBarcode.createGS1Databar(0, 2, data);
        buffer.append(title0.getBytes(), 0, title0.getBytes().length);
        buffer.append(gs0, 0, gs0.length);
        buffer.append(LF);

        final String title1 = "GS1 Databar type1\r\n";
        byte[] gs1 = WoosimBarcode.createGS1Databar(1, 2, data);
        buffer.append(title1.getBytes(), 0, title1.getBytes().length);
        buffer.append(gs1, 0, gs1.length);
        buffer.append(LF);

        final String title2 = "GS1 Databar type2\r\n";
        byte[] gs2 = WoosimBarcode.createGS1Databar(2, 2, data);
        buffer.append(title2.getBytes(), 0, title2.getBytes().length);
        buffer.append(gs2, 0, gs2.length);
        buffer.append(LF);

        final String title3 = "GS1 Databar type3\r\n";
        byte[] gs3 = WoosimBarcode.createGS1Databar(3, 2, data);
        buffer.append(title3.getBytes(), 0, title3.getBytes().length);
        buffer.append(gs3, 0, gs3.length);
        buffer.append(LF);

        final String title4 = "GS1 Databar type4\r\n";
        byte[] gs4 = WoosimBarcode.createGS1Databar(4, 2, data);
        buffer.append(title4.getBytes(), 0, title4.getBytes().length);
        buffer.append(gs4, 0, gs4.length);
        buffer.append(LF);

        final String title5 = "GS1 Databar type5\r\n";
        final byte[] data5 = {0x5b, 0x30, 0x31, 0x5d, 0x39, 0x30, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x38,
                0x5b, 0x33, 0x31, 0x30, 0x33, 0x5d, 0x30, 0x31, 0x32, 0x32, 0x33, 0x33};
        byte[] gs5 = WoosimBarcode.createGS1Databar(5, 2, data5);
        buffer.append(title5.getBytes(), 0, title5.getBytes().length);
        buffer.append(gs5, 0, gs5.length);
        buffer.append(LF);

        final String title6 = "GS1 Databar type6\r\n";
        final byte[] data6 = {0x5b, 0x30, 0x31, 0x5d, 0x39, 0x30, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x38,
                0x5b, 0x33, 0x31, 0x30, 0x33, 0x5d, 0x30, 0x31, 0x32, 0x32, 0x33, 0x33,
                0x5b, 0x31, 0x35, 0x5d, 0x39, 0x39, 0x31, 0x32, 0x33, 0x31};
        byte[] gs6 = WoosimBarcode.createGS1Databar(6, 4, data6);
        buffer.append(title6.getBytes(), 0, title6.getBytes().length);
        buffer.append(gs6, 0, gs6.length);
        buffer.append(LF);

        sendData(CMD_INIT_PRT);
        sendData(buffer.toByteArray());
    }

    public void setMSRDoubleTrackMode(View v) {
        byte[] cmd_msr_dbltrack = {ESC, 0x4d, 0x45};    // Set MSR double track reading mode (ESC M E)
        sendData(cmd_msr_dbltrack);
    }

    public void setMSRTripleTrackMode(View v) {
        byte[] cmd_msr_tpltrack = {ESC, 0x4d, 0x46};    // Set MSR triple track reading mode (ESC M F)
        sendData(cmd_msr_tpltrack);
    }

    public void cancelMSRMode(View v) {
        byte[] cmd_msr_cancel = {EOT};    // Cancel MSR reading mode
        sendData(cmd_msr_cancel);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                closeProgressDialog();
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a print
                    setupPrint();
                    printSetting();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d("", "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }

    }


    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BLuetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mPrintService.connect(device, secure);
    }
}
