package com.pms.gfmc.tow.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by mwkim
 * on 18/01/2019.
 */
public class NullStringToEmptyAdapterFactory implements TypeAdapterFactory {

    @SuppressWarnings(value = "unchecked")
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

        if (type.getRawType() != String.class) {
            return null;
        }

        return (TypeAdapter<T>) new StringAdapter();
    }

    class StringAdapter extends TypeAdapter<String> {

        @Override
        public void write(JsonWriter out, String value) throws IOException {

            if (TextUtils.isEmpty(value)) {
                out.nullValue();
                return;
            }

            out.value(value);
        }

        @Override
        public String read(JsonReader in) throws IOException {

            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return "";
            }

            return in.nextString();
        }
    }
}
