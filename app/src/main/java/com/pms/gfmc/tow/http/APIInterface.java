package com.pms.gfmc.tow.http;


import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.BasicCodeModel;
import com.pms.gfmc.tow.http.model.BustListModel;
import com.pms.gfmc.tow.http.model.LoginModel;
import com.pms.gfmc.tow.http.model.ParkCodeModel;
import com.pms.gfmc.tow.http.model.PayInfoModel;
import com.pms.gfmc.tow.http.model.RestoreListModel;
import com.pms.gfmc.tow.http.model.Tocim01;
import com.pms.gfmc.tow.http.model.WaitListModel;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIInterface {


    @FormUrlEncoded
    @POST(Constants.URL_LOGIN)
    Call<LoginModel> getLoginData(@FieldMap Map<String, String> filters);


    @GET(Constants.URL_BASIC_PUBLIC_CODE)
    Call<BasicCodeModel> getBasicCode();

    @FormUrlEncoded
    @POST(Constants.URL_CAR_SIZE)
    Call<BaseDataModel> getCarSize(@FieldMap Map<String, String> filters);

    @FormUrlEncoded
    @POST(Constants.URL_WAIT_CAR)
    Call<BaseDataModel> getWaitCar(@FieldMap Map<String, String> filters);


    @FormUrlEncoded
    @POST(Constants.URL_TOIN_LIST)
    Call<WaitListModel> getToInList(@FieldMap Map<String, String> filters);



    @FormUrlEncoded
    @POST(Constants.URL_WAIT_LIST)
    Call<WaitListModel> getWaitList(@FieldMap Map<String, String> filters);



    @FormUrlEncoded
    @POST(Constants.URL_TOIN_DETAIL)
    Call<WaitListModel> getToInDetail(@FieldMap Map<String, String> filters);

    @Headers("Content-Type: application/json")
    @POST(Constants.URL_TOOUT_CAR)
    Call<BaseDataModel> getToOutCar(@Body RequestBody filters);

    @Headers("Content-Type: application/json")
    @POST(Constants.URL_PAY_INFO)
    Call<PayInfoModel> getPayInfo(@Body RequestBody filters);

    @FormUrlEncoded
    @POST(Constants.URL_RESTORE_LIST)
    Call<RestoreListModel> getRestoreList(@FieldMap Map<String, String> filters);

    @Headers("Content-Type: application/json")
    @POST(Constants.URL_WAIT_PM)
    Call<BaseDataModel> getWaitPm(@Body RequestBody filters);

    @Headers("Content-Type: application/json")
    @POST(Constants.URL_TOIN_CAR)
    Call<BaseDataModel> getToinCar(@Body RequestBody filters);

}
