package com.pms.gfmc.tow.screen.enter;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.databinding.ActivityEntranceMenuBinding;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.carnum.AutoCarNumberActivity;
import com.pms.gfmc.tow.screen.payment.PayData;
import com.pms.gfmc.tow.screen.payment.ReceiptReturnVo;
import com.pms.gfmc.tow.screen.payment.ReceiptVo;

public class EntranceMenuActivity extends BaseActivity {
    private ActivityEntranceMenuBinding binding;
    private String TAG = "MAP";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityEntranceMenuBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnBack.setOnClickListener(v -> finish());
        binding.btnM00.setOnClickListener(v -> {
            Intent mIntent = new Intent(mContext, AutoCarNumberActivity.class);
            mIntent.putExtra(Constants.NEXT_ACTIVITY, "ENTRANCE");
            startActivity(mIntent);
        });

        binding.btnM01.setOnClickListener(v -> {
            Intent mIntent = new Intent(mContext, EntrancePMActivity.class);
            startActivity(mIntent);

        });

    }


}

