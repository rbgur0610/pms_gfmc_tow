package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.ParkCodeModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallCarSize extends APICall {

    public ActionResponseListener<BaseDataModel> actionResponseListener;
    private HashMap<String, String> map;

    public CallCarSize(Context context, HashMap<String, String> map, ActionResponseListener<BaseDataModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
        this.map = map;
    }

    @Override
    public void run() {

        call = APIClient.getInstance(true).getCarSize(map);
        call.enqueue(new Callback<BaseDataModel>() {
            @Override
            public void onResponse(Call<BaseDataModel> call, Response<BaseDataModel> response) {
                callDone(response, CallCarSize.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<BaseDataModel> call, Throwable t) {
                callDone(t, CallCarSize.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
