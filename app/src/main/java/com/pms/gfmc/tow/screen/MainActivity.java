package com.pms.gfmc.tow.screen;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.databinding.ActivityMainBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BasicCodeModel;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.http.model.DpComndlt;
import com.pms.gfmc.tow.http.request.CallBasicCode;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.print.BluetoothPrintService;
import com.pms.gfmc.tow.print.DeviceListActivity;
import com.pms.gfmc.tow.screen.enter.EntranceMenuActivity;
import com.pms.gfmc.tow.screen.enter.EntranceWaitActivity;
import com.pms.gfmc.tow.screen.retore.RestoreCompleteListActivity;
import com.pms.gfmc.tow.screen.retore.RestoreListActivity;
import com.pms.gfmc.tow.view.dialog.TreeDialog;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#ffffff"));
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getBasicCode();

        binding.btnMain00.setOnClickListener(v -> {
            startActivity(new Intent(mContext, EntranceMenuActivity.class));
        });

        binding.btnMain01.setOnClickListener(v -> {
            startActivity(new Intent(mContext, EntranceWaitActivity.class));
        });

        binding.btnMain02.setOnClickListener(v -> {
            startActivity(new Intent(mContext, RestoreListActivity.class));
        });

        binding.btnMain03.setOnClickListener(v -> {
            startActivity(new Intent(mContext, RestoreCompleteListActivity.class));
        });

        bleSetting();
        pServiceSetting();
    }




    private void getBasicCode() {
        showProgressDialog();

        APICallRuller.getInstance().addCall(new CallBasicCode(mContext, new ActionResponseListener<BasicCodeModel>() {
            @Override
            public void onResponse(BasicCodeModel mData) {
                try {
                    ArrayList<String> codes = new ArrayList<>();
                    ArrayList<String> dp_codes = new ArrayList<>();
                    for (BasicOPM01Model item : mData.sysopm01) {
                        if (item.commonCd.equals("$")) continue;
                        codes.add(item.classCd + "-" + item.commonCd + "-" + item.codeNm + "-" + item.RELCD1);
                    }

                    PrefData.getInstance(mContext).putListString(PrefData.SHARED_PREF_BASIC_CODE, codes);

                    if (mData.dp_comn_code_dtl != null && mData.dp_comn_code_dtl.size() > 0) {
                        for (DpComndlt item : mData.dp_comn_code_dtl) {
                            dp_codes.add(item.CODE_GRUP + "-" + item.CODE_ID + "-" + item.LANG0);
                        }
                    }
                    PrefData.getInstance(mContext).putListString(PrefData.SHARED_PREF_DP_CODE, dp_codes);


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        printStop();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        twoButtonDialog(Constants.MAINACTIVITY_DESTROY_ACTIVITY, getString(R.string.title_exit), getString(R.string.msg_exit_app)).show();
    }

    private boolean stopFlag = false;


    @Override
    protected void onRestart() {
        super.onRestart();
        stopFlag = false;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();

        if (mPrintService != null && mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            // Get a set of currently paired devices
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    Log.d(">>>>", " device.getName() >>> " + device.getName());
                    if (PrefData.getInstance(mContext).getString(PrefData.PRINT_NAME, "").equals(device.getName())) {
                        BluetoothDevice printDevice = mBluetoothAdapter.getRemoteDevice(device.getAddress());
                        // Attempt to connect to the device
                        mPrintService.connect(printDevice, false);
                        stopFlag = true;
                    }
                }
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            } else {
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            }
        }
    }
}
