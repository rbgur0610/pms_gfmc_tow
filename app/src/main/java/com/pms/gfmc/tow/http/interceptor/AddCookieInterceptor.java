package com.pms.gfmc.tow.http.interceptor;

import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import androidx.annotation.NonNull;

import com.pms.gfmc.tow.GlobalApplication;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddCookieInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(GlobalApplication.getContext());
        }

        Request.Builder builder = chain.request().newBuilder();

        String cookie = CookieManager.getInstance().getCookie(chain.request().url().toString());

        if (cookie != null) {
            builder.addHeader("Cookie", cookie);
        }

        return chain.proceed(builder.build());
    }
}
