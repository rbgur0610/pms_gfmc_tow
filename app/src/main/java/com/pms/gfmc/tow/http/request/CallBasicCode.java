package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BasicCodeModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallBasicCode extends APICall {

    public ActionResponseListener<BasicCodeModel> actionResponseListener;

    public CallBasicCode(Context context, ActionResponseListener<BasicCodeModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
    }

    @Override
    public void run() {

        call = APIClient.getInstance().getBasicCode();
        call.enqueue(new Callback<BasicCodeModel>() {
            @Override
            public void onResponse(Call<BasicCodeModel> call, Response<BasicCodeModel> response) {
                callDone(response, CallBasicCode.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<BasicCodeModel> call, Throwable t) {
                callDone(t, CallBasicCode.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
