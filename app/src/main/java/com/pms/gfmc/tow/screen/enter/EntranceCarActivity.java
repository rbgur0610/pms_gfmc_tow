package com.pms.gfmc.tow.screen.enter;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.databinding.ActivityEnteranceCarBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.BasicCodeModel;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.http.model.DpComndlt;
import com.pms.gfmc.tow.http.request.CallBasicCode;
import com.pms.gfmc.tow.http.request.CallCarSize;
import com.pms.gfmc.tow.http.request.CallWaitCar;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.carnum.AutoCarNumberActivity;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;
import com.pms.gfmc.tow.view.dialog.TreeDialog;
import com.pms.gfmc.tow.view.dialog.TreeHolderHolder;
import com.pms.gfmc.tow.view.treeview.model.TreeNode;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

public class EntranceCarActivity extends BaseActivity {

    private ActivityEnteranceCarBinding binding;
    private String carArea = "";

    private ArrayList<DpComndlt> bust_dong;
    private ArrayList<BasicOPM01Model> bust_type;
    private ArrayList<BasicOPM01Model> bust_agency = new ArrayList<>();
    private ArrayList<BasicOPM01Model> bust_team = new ArrayList<>();
    private ArrayList<BasicOPM01Model> bust_user = new ArrayList<>();

    private ArrayList<BasicOPM01Model> carref01_list;
    private ArrayList<BasicOPM01Model> carref02_list;


    private int baseTime;
    private String thisTime;
    private String carref01 = "";
    private String carref02 = "";
    private int currHour;
    private int currMin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityEnteranceCarBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String carName = getIntent().getStringExtra(Constants.CAR_INPUT_NO);

        binding.tvCarName.setText(carName);
        binding.tvCarName.setOnClickListener(v -> {
            Intent mIntent = new Intent(mContext, AutoCarNumberActivity.class);
            mIntent.putExtra(Constants.NEXT_ACTIVITY, "ENTRANCE");
            startActivity(mIntent);
            finish();
        });

        binding.btnBack.setOnClickListener(view -> finish());

        getBasicCode();
        getCarSize();

        thisTime = DateUtil.getYmdhms("yyyy-MM-dd HH:mm");
        binding.tvEnterTime.setText(thisTime);

        binding.tvBustDate.setText(DateUtil.getMin(-30));
        currHour = Integer.parseInt(DateUtil.getSimpleHour(0));
        currMin = Integer.parseInt(DateUtil.getSimpleMin(-30));
        binding.tvBustDate.setOnClickListener(v -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    currHour = hourOfDay;
                    currMin = minute;
                    binding.tvBustDate.setText(DateUtil.getDay() + " " + StringUtil.addZero(currHour, 2) + ":" + StringUtil.addZero(currMin, 2));

                }
            }, currHour, currMin, true);

            timePickerDialog.setMessage("시간 변경");
            timePickerDialog.show();
        });


        binding.btnEntranceConfirm.setOnClickListener(view -> {
            reqEntranceCar();
        });


    }

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    private void getCarSize() {
        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("carno", binding.tvCarName.getText().toString());

        APICallRuller.getInstance().addCall(new CallCarSize(mContext, map, new ActionResponseListener<BaseDataModel>() {
            @Override
            public void onResponse(BaseDataModel mData) {
                try {
                    /**
                     <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
                     <soap:Header><commonHeader xmlns=\"http://ccais.mopas.go.kr/dh/nrw/infoservice/ts/ReductionTsCarInfo/types\">
                     <serviceName>ReductionTsCarInfoService</serviceName>
                     <useSystemCode>D366028CMC</useSystemCode>
                     <certServerId>SVRB552067004</certServerId>
                     <transactionUniqueId>2022040823333713287531604</transactionUniqueId>
                     <userDeptCode />
                     <userName />
                     </commonHeader></soap:Header>
                     <soap:Body>
                     <getReductionTsCarInfoResponse xmlns=\"http://ccais.mopas.go.kr/dh/nrw/infoservice/ts/ReductionTsCarInfo/types\">
                     <cntcResultCode>MSG50000</cntcResultCode>\n
                     <cntcResultDtls>정상</cntcResultDtls>\n
                     <vhctyAsortNm>승용</vhctyAsortNm>\n
                     <dsplvl>1998</dsplvl>\n
                     <cbdLt>4865</cbdLt>\n
                     <vhctySeNm>중형</vhctySeNm>\n
                     <tkcarPscapCo>5</tkcarPscapCo>\n
                     <processImprtyResnCode>00</processImprtyResnCode>\n
                     <processImprtyResnDtls>정상</processImprtyResnDtls>
                     </getReductionTsCarInfoResponse>
                     </soap:Body>
                     </soap:Envelope>
                     **/


                    if (!StringUtil.isEmpty(mData.result)) {
                        // xml 파싱하기
                        InputSource is = new InputSource(new StringReader(mData.result));
                        DocumentBuilder builder = factory.newDocumentBuilder();
                        Document doc = builder.parse(is);
                        XPathFactory xpathFactory = XPathFactory.newInstance();
                        XPath xpath = xpathFactory.newXPath();


                        XPathExpression expr = xpath.compile("//getReductionTsCarInfoResponse");
                        NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
                        for (int i = 0; i < nodeList.getLength(); i++) {
                            NodeList child = nodeList.item(i).getChildNodes();
                            for (int j = 0; j < child.getLength(); j++) {
                                Node node = child.item(j);
                                System.out.println("현재 노드 이름 : " + node.getNodeName());
                                System.out.println("현재 노드 값 : " + node.getTextContent());
                                if ("vhctyAsortNm".equals(node.getNodeName())) {

                                    int index = 0;
                                    for (BasicOPM01Model data : carref01_list) {
                                        if (data.codeNm.equals(node.getTextContent())) {
                                            binding.spCarGbn.setSelection(index);
                                            break;
                                        }
                                        index++;
                                    }
                                }



                                if ("vhctySeNm".equals(node.getNodeName())) {
                                    int index = 0;
                                    for (BasicOPM01Model data : carref02_list) {
                                        if (data.codeNm.equals(node.getTextContent())) {
                                            binding.spCarSize.setSelection(index);
                                            break;
                                        }
                                        index++;
                                    }
                                }
                            }
                        }
                    } else {
                        Toast.makeText(mContext, "차량 조회에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(mContext, "코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
//                Toast.makeText(mContext, "코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    private void getBasicCode() {

        APICallRuller.getInstance().addCall(new CallBasicCode(mContext, new ActionResponseListener<BasicCodeModel>() {
            @Override
            public void onResponse(BasicCodeModel mData) {
                try {

                    bust_dong = new ArrayList<>();
                    bust_agency = new ArrayList<>();
                    bust_team = new ArrayList<>();
                    bust_user = new ArrayList<>();
                    bust_type = new ArrayList<>();

                    carref01_list = new ArrayList<>();
                    carref02_list = new ArrayList<>();

                    ArrayList<String> bustDongSpList = new ArrayList<>();
                    ArrayList<String> bustAgencySpList = new ArrayList<>();
                    ArrayList<String> bustTeamSpList = new ArrayList<>();
                    ArrayList<String> bustUserSpList = new ArrayList<>();
                    ArrayList<String> bustTypeSpList = new ArrayList<>();

                    ArrayList<String> carref01List = new ArrayList<>();
                    ArrayList<String> carref02List = new ArrayList<>();

                    if (mData != null) {
                        if (mData.dp_comn_code_dtl != null && mData.dp_comn_code_dtl.size() > 0) {
                            for (DpComndlt data : mData.dp_comn_code_dtl) {
                                bust_dong.add(data);
                                bustDongSpList.add(data.LANG0);
                            }
                        }

                        if (mData.sysopm01 != null && mData.sysopm01.size() > 0) {
                            for (BasicOPM01Model data : mData.sysopm01) {
                                if ("T004".equals(data.classCd)) {
                                    bust_agency.add(data);
                                    bustAgencySpList.add(data.codeNm);
                                }
                                if ("T012".equals(data.classCd)) {
                                    bust_team.add(data);
                                    bustTeamSpList.add(data.codeNm);
                                }

                                if ("T013".equals(data.classCd)) {
                                    bust_user.add(data);
                                    bustUserSpList.add(data.codeNm);
                                }

                                if ("T001".equals(data.classCd)) {
                                    bust_type.add(data);
                                    bustTypeSpList.add(data.codeNm);
                                }

                                if ("T015".equals(data.classCd)) {
                                    carref01_list.add(data);
                                    carref01List.add(data.codeNm);
                                }

                                if ("T016".equals(data.classCd)) {
                                    carref02_list.add(data);
                                    carref02List.add(data.codeNm);
                                }
                            }
                        }



                        if (carref01List.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, carref01List);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spCarGbn.setAdapter(adapter);
                            binding.spCarGbn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spCarGbn.setTag(carref01_list.get(i).commonCd);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }



                        if (carref02List.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, carref02List);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spCarSize.setAdapter(adapter);
                            binding.spCarSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spCarSize.setTag(carref02_list.get(i).commonCd);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }




                        if (bustDongSpList.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustDongSpList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spAddressDong.setAdapter(adapter);
                            binding.spAddressDong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spAddressDong.setTag(bust_dong.get(i).CODE_ID);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }




                        if (bustTeamSpList.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustTeamSpList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spBustTeam.setAdapter(adapter);
                            binding.spBustTeam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spBustTeam.setTag(bust_team.get(i).commonCd);

                                    if (bust_team.get(i).codeNm.startsWith("단속")) {
                                        binding.tvBustAgency.setTag("GG");
                                        binding.tvBustAgency.setText("금천구청");
                                        binding.tvBustUser.setTag("010");
                                        binding.tvBustUser.setText("구청단속");
                                    } else if (bust_team.get(i).codeNm.startsWith("공단")) {
                                        binding.tvBustAgency.setTag("KG");
                                        binding.tvBustAgency.setText("금천구시설관리공단");
                                        binding.tvBustUser.setTag("020");
                                        binding.tvBustUser.setText("공단단속");
                                    }else if (bust_team.get(i).codeNm.startsWith("외주")) {
                                        binding.tvBustAgency.setTag("B1");
                                        binding.tvBustAgency.setText("외주업체");
                                        binding.tvBustUser.setTag("030");
                                        binding.tvBustUser.setText("외주업체");
                                    }else if (bust_team.get(i).codeNm.startsWith("시청")) {
                                        binding.tvBustAgency.setTag("SG");
                                        binding.tvBustAgency.setText("서울시청");
                                        binding.tvBustUser.setTag("040");
                                        binding.tvBustUser.setText("시청단속");
                                    }else if (bust_team.get(i).codeNm.startsWith("경찰")) {
                                        binding.tvBustAgency.setTag("PG");
                                        binding.tvBustAgency.setText("경찰");
                                        binding.tvBustUser.setTag("050");
                                        binding.tvBustUser.setText("경찰단속");
                                    }


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }




                        if (bustTypeSpList.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustTypeSpList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spBustType.setAdapter(adapter);
                            binding.spBustType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spBustType.setTag(bust_type.get(i).commonCd);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    private void reqEntranceCar() {
        showProgressDialog();
        String userName = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_NAME, "");
        String userID = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_ID, "");

        HashMap<String, String> map = new HashMap<>();
        map.put("carno", binding.tvCarName.getText().toString());
        map.put("cartype", binding.edCarType.getText().toString());
        map.put("carref01", binding.spCarGbn.getTag().toString());
        map.put("carref02", binding.spCarSize.getTag().toString());
        map.put("intype", binding.spBustType.getTag().toString());
        map.put("spotcd", binding.spAddressDong.getTag().toString());
        map.put("spotnm", binding.edAddress.getText().toString());
        map.put("spottime", binding.tvBustDate.getText().toString().replace("-", "").replace(" ", "").replace(":", "") + "00");
        map.put("towtime", binding.tvEnterTime.getText().toString().replace("-", "").replace(" ", "").replace(":", "") + "00");
        map.put("takingperson", userName);
        map.put("pnucode", binding.tvBustAgency.getTag().toString());
        map.put("spotgroup", binding.spBustTeam.getTag().toString());
        map.put("spotperson", binding.tvBustUser.getTag().toString());
        map.put("incomment", binding.edMemo.getText().toString());
        map.put("id", userID);


        APICallRuller.getInstance().addCall(new CallWaitCar(mContext, map, new ActionResponseListener<BaseDataModel>() {
            @Override
            public void onResponse(BaseDataModel mData) {
                try {
                    Toast.makeText(mContext, "정상 처리 하였습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

}
