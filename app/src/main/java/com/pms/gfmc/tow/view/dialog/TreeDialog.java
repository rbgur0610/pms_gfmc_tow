package com.pms.gfmc.tow.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.view.treeview.model.TreeNode;
import com.pms.gfmc.tow.view.treeview.view.AndroidTreeView;

import java.util.ArrayList;


public class TreeDialog extends Dialog {
    private Context mContext;
    private final ViewGroup containerView;
    private AndroidTreeView tView;
    private TreeNode.TreeNodeClickListener nodeClickListener;
    private ArrayList<BasicOPM01Model> bust_agency;
    private ArrayList<BasicOPM01Model> bust_team;
    private ArrayList<BasicOPM01Model> bust_user;

    public TreeDialog(Context context, TreeNode.TreeNodeClickListener nodeClickListener, ArrayList<BasicOPM01Model> bust_agency, ArrayList<BasicOPM01Model> bust_team, ArrayList<BasicOPM01Model> bust_user) {
        super(context);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.layout_tree_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        this.nodeClickListener = nodeClickListener;
        this.bust_agency = bust_agency;
        this.bust_team = bust_team;
        this.bust_user = bust_user;

        containerView = (ViewGroup) findViewById(R.id.container);
        setTreeData();

    }


    private void setTreeData() {
        TreeNode root = TreeNode.root();
        TreeNode _1depth = null;
        for(BasicOPM01Model top : bust_agency){
            _1depth = new TreeNode(new TreeHolderHolder.Item(top));
            for(BasicOPM01Model center : bust_team){
                if("GG".equals(top.commonCd)){

                }


                for(BasicOPM01Model bottom : bust_user){

                }
            }

        }
        root.addChildren(_1depth);

//        TreeNode computerRoot = new TreeNode(new TreeHolderHolder.Item("My Computer"));
//        computerRoot.setExpanded(true);
//        TreeNode myDocuments = new TreeNode(new TreeHolderHolder.Item("My Documents"));
//        TreeNode downloads = new TreeNode(new TreeHolderHolder.Item("Downloads"));
//        TreeNode file1 = new TreeNode(new TreeHolderHolder.Item("Folder 1"));
//        TreeNode file2 = new TreeNode(new TreeHolderHolder.Item("Folder 2"));
//        downloads.addChildren(file1, file2);
//
//        TreeNode myMedia = new TreeNode(new TreeHolderHolder.Item("Photos"));
//        TreeNode photo1 = new TreeNode(new TreeHolderHolder.Item("Folder 1"));
//        TreeNode photo2 = new TreeNode(new TreeHolderHolder.Item("Folder 2"));
//        myMedia.addChildren(photo1, photo2);
//
//        myDocuments.addChild(downloads);
//        computerRoot.addChildren(myDocuments, myMedia);
//
//        root.addChildren(computerRoot);

        tView = new AndroidTreeView(mContext, root);
        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        tView.setDefaultViewHolder(TreeHolderHolder.class);
        tView.setDefaultNodeClickListener(nodeClickListener);
        tView.setDefaultNodeLongClickListener(nodeLongClickListener);

        containerView.addView(tView.getView());

    }



    private TreeNode.TreeNodeLongClickListener nodeLongClickListener = new TreeNode.TreeNodeLongClickListener() {
        @Override
        public boolean onLongClick(TreeNode node, Object value) {
            TreeHolderHolder.Item item = (TreeHolderHolder.Item) value;
            Toast.makeText(mContext, "Long click: " + item.data, Toast.LENGTH_SHORT).show();
            return true;
        }
    };
}
