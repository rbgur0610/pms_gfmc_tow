package com.pms.gfmc.tow.screen.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.http.model.Tocim02;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class RestoreCompleteListAdapter extends RecyclerView.Adapter<RestoreCompleteListAdapter.eventBaseViewHolder> {

    Context mContext;
    List<Tocim02> list;
    boolean isFirst = true;

    public RestoreCompleteListAdapter(Context context) {
        this.mContext = context;
    }


    public void setListData(List<Tocim02> listData) {
        if (listData == null) {
            this.list = new ArrayList<>();
        }
        this.list = listData;
        try {
            new Handler().post(this::notifyDataSetChanged);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @NonNull
    @Override
    public eventBaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_restore_complete, parent, false);
        return new eventBaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull eventBaseViewHolder holder, final int position) {
        final Tocim02 item = list.get(position);

        holder.tv_date.setText(DateUtil.convertDateType("yyyy-MM-dd", item.OUTDAY+"000000"));
        holder.tv_carno.setText(item.CARNO);
        holder.tv_car_type.setText(item.CARTYPE);
        holder.tv_amt.setText(StringUtil.addComma(item.TOTALAMT)+"원");
    }


    @Override
    public int getItemCount() {
        if (list == null) return 0;
        return list.size();
    }

    class eventBaseViewHolder extends RecyclerView.ViewHolder {

        eventBaseViewHolder(View itemView) {
            super(itemView);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_carno = itemView.findViewById(R.id.tv_carno);
            tv_car_type = itemView.findViewById(R.id.tv_car_type);
            ly_contents = itemView.findViewById(R.id.ly_contents);
            tv_amt = itemView.findViewById(R.id.tv_amt);
        }

        public TextView tv_amt;
        public View ly_contents;
        public TextView tv_date;
        public TextView tv_carno;
        public TextView tv_car_type;
    }

}
