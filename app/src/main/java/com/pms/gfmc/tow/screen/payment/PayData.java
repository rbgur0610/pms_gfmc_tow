package com.pms.gfmc.tow.screen.payment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.print.BluetoothPrintService;
import com.pms.gfmc.tow.utils.MsgUtil;
import com.woosim.printer.WoosimCmd;
import com.woosim.printer.WoosimImage;

import org.apache.http.util.ByteArrayBuffer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

public class PayData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1969139487822750251L;

	private final String TAG = this.getClass().getSimpleName();
	
	public static final int PAY_TYPE = 0x000111;
	
	// 입/출차 
	public static final int PAY_DIALOG_TYPE_EXIT_CASH  	      = PAY_TYPE + 10;
	public static final int PAY_DIALOG_TYPE_EXIT_CARD  	      = PAY_TYPE + 11;
	public static final int PAY_DIALOG_TYPE_EXIT_ONLY 	  	  = PAY_TYPE + 12;
	public static final int PAY_DIALOG_TYPE_PREPAY_CASH 	  = PAY_TYPE + 13;
	public static final int PAY_DIALOG_TYPE_PREPAY_CARD 	  = PAY_TYPE + 14;
	public static final int PAY_DIALOG_TYPE_ENTRANCE_TIME 	  = PAY_TYPE + 15;
	public static final int PAY_DIALOG_TYPE_COUPON 			  = PAY_TYPE + 16;
	public static final int PAY_DIALOG_TYPE_PARK_CHANGE_ADV   = PAY_TYPE + 17;
	public static final int PAY_DIALOG_TYPE_PARK_CHANGE_LATER = PAY_TYPE + 18;
	
	// 미수
	public static final int PAY_DIALOG_TYPE_UNPAY_CASH  	= PAY_TYPE + 20;
	public static final int PAY_DIALOG_TYPE_UNPAY_CARD  	= PAY_TYPE + 21;
	public static final int PAY_DIALOG_TYPE_UNPAY_BILL 		= PAY_TYPE + 22;
	public static final int PAY_DIALOG_TYPE_UNPAY_SUCCESS 	= PAY_TYPE + 23;
	
	// 정기권 
	public static final int PAY_DIALOG_TYPE_TICKET_CASH 	= PAY_TYPE + 30;
	public static final int PAY_DIALOG_TYPE_TICKET_CARD 	= PAY_TYPE + 31;
	public static final int PAY_DIALOG_TYPE_TICKET_SUCCESS  = PAY_TYPE + 32;
	public static final int PAY_DIALOG_TYPE_TICKET_UNPAY 	= PAY_TYPE + 33;
	
	// 입/출차 취소 	
	public static final int PAY_DIALOG_CANCEL 				   = PAY_TYPE + 40;
	public static final int PAY_DIALOG_CANCEL_APPROVAL_EXIT    = PAY_TYPE + 41;
	public static final int PAY_DIALOG_CANCEL_APPROVAL_CHANGE  = PAY_TYPE + 42;
	public static final int PAY_DIALOG_CANCEL_ENTRANCE_TIME    = PAY_TYPE + 43;
	public static final int PAY_DIALOG_CANCEL_APPROVAL_CANCEL  = PAY_TYPE + 44;
	
	// 현금영수증
	public static final int PAY_DIALOG_TYPE_CASH_RECEIPT 	 = PAY_TYPE + 50;
	public static final int PAY_DIALOG_CANCEL_CASH_RECEIPT 	 = PAY_TYPE + 51;
	
	// 리스트 조회 
	public static final int PAY_TYPE_TICKET_MANAGER 	     = PAY_TYPE + 80;
	public static final int PAY_TYPE_UNPAY_MANAGER 	         = PAY_TYPE + 81;
	public static final int PAY_TYPE_EXIT_MANAGER            = PAY_TYPE + 82;	
	
	// 마감
	public static final int PAY_TYPE_PARK_CLOSE_STATUS       = PAY_TYPE + 85;
	
	/**
	 * print
	 */
	public static final byte EOT = 0x04;
	public static final byte LF  = 0x0a;
	public static final byte ESC = 0x1b;
	public static final byte GS  = 0x1d;
	public static final byte[] CMD_INIT_PRT = { ESC, 0x40 }; // Initialize
	  														  // printer (ESC, @)
	private BluetoothPrintService mPrintService;
	
	private Context mContext;
	

	private Bitmap m_bmpPrint;
	
	public PayData(Context context){
		this.mContext 	  = context;
	}
	
	public void printPayment(int type, ReceiptVo vo, BluetoothPrintService printService, Bundle data, Bitmap signBitmap) {
		 
		mPrintService = printService;
		
		Log.d(TAG, " type >>> " + type);

		try {
			printText(vo.print());
//		    switch (type) {
//				case PAY_DIALOG_TYPE_EXIT_CARD:
//					ReceiptType4Vo type4Vo = (ReceiptType4Vo) vo;
//					type4Vo.setPrint(OUT_PRINT);
//					type4Vo.setTitle("출차승인영수증");
//					type4Vo.setPayAmt(data.getString("cardAmt"));
//					type4Vo.setCardNo(data.getString("cardNo"));
//					type4Vo.setCardCompany(data.getString("cardCompany"));
//					type4Vo.setConfirmCardNum(data.getString("approvalNo"));
//					printText(type4Vo.print());
//					break;
//			   case PAY_DIALOG_TYPE_UNPAY_CARD:
//				    ReceiptType10Vo type10Vo = (ReceiptType10Vo) vo;
//					// TODO 미납금 영수증 필요값 셋팅
//					type10Vo.setCardNo(data.getString("cardNo"));
//					type10Vo.setCardCompany(data.getString("cardCompany"));
//					type10Vo.setConfirmCardNum(data.getString("approvalNo"));
//					printText(type10Vo.print());
//					break;
//			   case PAY_DIALOG_TYPE_PREPAY_CARD:
//				    ReceiptType13Vo type13Vo = (ReceiptType13Vo) vo;
//				    type13Vo.setPrintText(IN_PRINT);
//					type13Vo.setCardNo(data.getString("cardNo"));
//					type13Vo.setCardCompany(data.getString("cardCompany"));
//					type13Vo.setApprovalNo(data.getString("approvalNo"));
//					printText(type13Vo.print());
//				   break;
//			   case PAY_DIALOG_TYPE_TICKET_CARD:
//				    ReceiptType11Vo type11Vo = (ReceiptType11Vo) vo;
//				    type11Vo.setPrint(TICKET_PRINT);
//					type11Vo.setCardNo(data.getString("cardNo"));
//					type11Vo.setCardCompany(data.getString("cardCompany"));
//					type11Vo.setConfirmCardNum(data.getString("approvalNo"));
//					printText(type11Vo.print());
//				   break;
//
//			}
			 
			Log.d(TAG, " signBitmap >>> " + signBitmap);
			if(signBitmap != null){
				Log.d(TAG, " signBitmap.getWidth() >>> " + signBitmap.getWidth() );
				Log.d(TAG, " signBitmap.getHeight() >>> " + signBitmap.getHeight() );
				pirntBMPImage(makePrintBitmap(signBitmap));
				printText("\n\n\n\n");	     
			}
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
	}
	
	
	public void printPaymentCancel(int type, ReceiptVo vo, BluetoothPrintService printService, Bundle data, Bitmap signBitmap) {
		
		mPrintService = printService;
		
		Log.d(TAG, " type >>> " + type);
		
		try {
			printText(vo.print());
//			switch (type) {
//			  case PAY_DIALOG_CANCEL_ENTRANCE_TIME:
//				 ReceiptType13Vo type13Vo = (ReceiptType13Vo) vo;
//				 printText(type13Vo.print());
//				break;
//			  case PAY_DIALOG_CANCEL_APPROVAL_EXIT :
//			  case PAY_DIALOG_CANCEL_APPROVAL_CHANGE :
//				 ReceiptType4Vo type4Vo = (ReceiptType4Vo) vo;
//				 type4Vo.setPrint(OUT_PRINT);
//				 type4Vo.setTitle("결제취소영수증");
//				 type4Vo.setPayAmt(data.getString("cardAmt"));
//				 printText(type4Vo.print());
//				break;
//			}
			
			if(signBitmap != null){
				pirntBMPImage(makePrintBitmap(signBitmap));
				printText("\n\n\n\n");	     
			}
			
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	   	
		
	}
	
	// 영수증 출력
	public void printDefault(ReceiptVo vo, BluetoothPrintService printService){
		
		mPrintService = printService;

		try {
			printText(vo.print());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			MsgUtil.ToastMessage(mContext, "인쇄되지 않았습니다. 다시 시도해 주세요", Toast.LENGTH_LONG);
		}
		
	}


	public void printText(ByteArrayBuffer buffer) throws UnsupportedEncodingException {

		byte[] text;
		sendData(CMD_INIT_PRT);
		String str = "\n\n";
		text = str.getBytes("EUC-KR");
		buffer.append(text, 0, text.length);
		sendData(buffer.toByteArray());

		sendData(WoosimCmd.cutPaper(0x01));

	}

	public void printText(String str) throws UnsupportedEncodingException {
		Log.e("str----print---==", str);

		byte[] text = str.getBytes("EUC-KR");
		if (text.length == 0)
			return;

		ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
		buffer.append(text, 0, text.length);
		buffer.append(LF);

		sendData(CMD_INIT_PRT);
		sendData(buffer.toByteArray());
		sendData(WoosimCmd.cutPaper(0x00));
	}

	/**
	 * Print data.
	 *
	 * @param data A byte array to print.
	 */
	private void sendData(byte[] data) {
		// Check that we're actually connected before trying printing

		if (mPrintService == null || mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
			MsgUtil.ToastMessage(mContext, R.string.not_connected);
			return;
		}

		// Check that there's actually something to send
		Log.e("data.length", String.valueOf(data.length));
		if (data.length > 0) {
			mPrintService.write(data);
		}
	}

	public Bitmap makePrintBitmap(Bitmap signBitmap) {
		if (m_bmpPrint != null) {
			m_bmpPrint.recycle();
			m_bmpPrint = null;
		}
		m_bmpPrint = Bitmap.createScaledBitmap(signBitmap, 384, 200, false);
		return m_bmpPrint;
	}

	public void pirntBMPImage(Bitmap bitmapImage) {
		byte[] cmd_init_prt = {ESC, 0x40};    // Initialize printer (ESC @)
		byte[] cmd_pagemode = {ESC, 0x4c};    // Select page mode (ESC L)
		byte[] data = WoosimImage.printARGBbitmap(0, 0, 384, 200, bitmapImage);
		byte[] cmd_stdmode = {ESC, 0x53};    // Select standard mode (ESC S)

		ByteArrayBuffer buffer = new ByteArrayBuffer(1024);

		buffer.append(cmd_init_prt, 0, cmd_init_prt.length);
		buffer.append(cmd_pagemode, 0, cmd_pagemode.length);
		buffer.append(data, 0, data.length);
		buffer.append(cmd_stdmode, 0, cmd_stdmode.length);

		mPrintService.write(buffer.toByteArray());
	}
	
}