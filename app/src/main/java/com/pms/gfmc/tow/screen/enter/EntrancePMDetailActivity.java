package com.pms.gfmc.tow.screen.enter;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.databinding.ActivityEnterancePmDetailBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.BasicCodeModel;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.http.model.DpComndlt;
import com.pms.gfmc.tow.http.request.CallBasicCode;
import com.pms.gfmc.tow.http.request.CallWaitCar;
import com.pms.gfmc.tow.http.request.CallWaitPm;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;
import com.pms.gfmc.tow.view.tag.TagView;

import java.util.ArrayList;
import java.util.HashMap;

public class EntrancePMDetailActivity extends BaseActivity {
    private int currHour;
    private int currMin;
    private ActivityEnterancePmDetailBinding binding;
    private ArrayList<String> mQuickList;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityEnterancePmDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnBack.setOnClickListener(view -> finish());

        mQuickList = getIntent().getStringArrayListExtra("QuickList");

        for (String tag : mQuickList) {
            binding.rvList.addTag(tag);
        }

        binding.lyScroll.measure(0, 0);
        if (binding.lyScroll.getMeasuredHeight() > AndroidUtil.dpToPx(mContext, 140)) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, (int) AndroidUtil.dpToPx(mContext, 140));
            binding.lyScroll.setLayoutParams(lp);
        }


        binding.rvList.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                if(binding.rvList.getChildCount()==1) {
                    Toast.makeText(mContext, "마지막 항목은 삭제하실수 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                AlertDialog dialog = new AlertDialog.Builder(mContext)
                        .setTitle("삭제하기")
                        .setMessage("선택한 아이디를 삭제하시겠습니까?")
                        .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (position < binding.rvList.getChildCount()) {
                                    binding.rvList.removeTag(position);

                                    binding.lyScroll.measure(0, 0);
                                    if (binding.lyScroll.getMeasuredHeight() < AndroidUtil.dpToPx(mContext, 140)) {
                                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        binding.lyScroll.setLayoutParams(lp);
                                    }

                                }
                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onSelectedTagDrag(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {

            }
        });
        getBasicCode();

        String thisTime = DateUtil.getYmdhms("yyyy-MM-dd HH:mm");
        binding.tvEnterTime.setText(thisTime);

        binding.tvBustDate.setText(DateUtil.getMin(-30));
        currHour = Integer.parseInt(DateUtil.getSimpleHour(0));
        currMin = Integer.parseInt(DateUtil.getSimpleMin(-30));


        binding.tvBustDate.setOnClickListener(v -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    currHour = hourOfDay;
                    currMin = minute;
                    binding.tvBustDate.setText(DateUtil.getDay() + " " + StringUtil.addZero(currHour, 2) + ":" + StringUtil.addZero(currMin, 2));

                }
            }, currHour, currMin, true);

            timePickerDialog.setMessage("시간 변경");
            timePickerDialog.show();
        });


        binding.btnEntranceConfirm.setOnClickListener(v->{
            reqEntrancePmCar();
        });
    }

    private ArrayList<DpComndlt> bust_dong;
    private ArrayList<BasicOPM01Model> bust_agency;
    private ArrayList<BasicOPM01Model> bust_team;
    private ArrayList<BasicOPM01Model> bust_user;
    private ArrayList<BasicOPM01Model> bust_pm;

    private void getBasicCode() {

        APICallRuller.getInstance().addCall(new CallBasicCode(mContext, new ActionResponseListener<BasicCodeModel>() {
            @Override
            public void onResponse(BasicCodeModel mData) {
                try {

                    bust_dong = new ArrayList<>();
                    bust_agency = new ArrayList<>();
                    bust_team = new ArrayList<>();
                    bust_user = new ArrayList<>();
                    bust_pm = new ArrayList<>();

                    ArrayList<String> bustDongSpList = new ArrayList<>();
//                    ArrayList<String> bustAgencySpList = new ArrayList<>();
                    ArrayList<String> bustTeamSpList = new ArrayList<>();
//                    ArrayList<String> bustUserSpList = new ArrayList<>();


                    if (mData != null) {
                        if (mData.dp_comn_code_dtl != null && mData.dp_comn_code_dtl.size() > 0) {
                            for (DpComndlt data : mData.dp_comn_code_dtl) {
                                bust_dong.add(data);
                                bustDongSpList.add(data.LANG0);
                            }
                        }

                        if (mData.sysopm01 != null && mData.sysopm01.size() > 0) {
                            for (BasicOPM01Model data : mData.sysopm01) {
                                if ("T004".equals(data.classCd)) {
                                    bust_agency.add(data);
//                                    bustAgencySpList.add(data.codeNm);
                                }
                                if ("T012".equals(data.classCd)) {
                                    bust_team.add(data);
                                    bustTeamSpList.add(data.codeNm);
                                }

                                if ("T013".equals(data.classCd)) {
                                    bust_user.add(data);
//                                    bustUserSpList.add(data.codeNm);
                                }

                                if ("T018".equals(data.classCd)) {
                                    bust_pm.add(data);
                                }


                            }
                        }


                        if (bustDongSpList.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustDongSpList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spAddressDong.setAdapter(adapter);
                            binding.spAddressDong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spAddressDong.setTag(bust_dong.get(i).CODE_ID);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }


//                        if (bustAgencySpList.size() > 1) {
//                            ArrayAdapter<String> adapter =
//                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustAgencySpList);
//                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            binding.spBustAgency.setAdapter(adapter);
//                            binding.spBustAgency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                @Override
//                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                                    binding.spBustAgency.setTag(bust_agency.get(i).commonCd);
//                                }
//
//                                @Override
//                                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                                }
//                            });
//                        }


                        if (bustTeamSpList.size() > 1) {
                            ArrayAdapter<String> adapter =
                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustTeamSpList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.spBustTeam.setAdapter(adapter);
                            binding.spBustTeam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    binding.spBustTeam.setTag(bust_team.get(i).commonCd);
                                    if (bust_team.get(i).codeNm.startsWith("단속")) {
                                        binding.tvBustAgency.setTag("GG");
                                        binding.tvBustAgency.setText("금천구청");
                                        binding.tvBustUser.setTag("010");
                                        binding.tvBustUser.setText("구청단속");
                                    } else if (bust_team.get(i).codeNm.startsWith("공단")) {
                                        binding.tvBustAgency.setTag("KG");
                                        binding.tvBustAgency.setText("금천구시설관리공단");
                                        binding.tvBustUser.setTag("020");
                                        binding.tvBustUser.setText("공단단속");
                                    }else if (bust_team.get(i).codeNm.startsWith("외주")) {
                                        binding.tvBustAgency.setTag("B1");
                                        binding.tvBustAgency.setText("외주업체");
                                        binding.tvBustUser.setTag("030");
                                        binding.tvBustUser.setText("외주업체");
                                    }else if (bust_team.get(i).codeNm.startsWith("시청")) {
                                        binding.tvBustAgency.setTag("SG");
                                        binding.tvBustAgency.setText("서울시청");
                                        binding.tvBustUser.setTag("040");
                                        binding.tvBustUser.setText("시청단속");
                                    }else if (bust_team.get(i).codeNm.startsWith("경찰")) {
                                        binding.tvBustAgency.setTag("PG");
                                        binding.tvBustAgency.setText("경찰");
                                        binding.tvBustUser.setTag("050");
                                        binding.tvBustUser.setText("경찰단속");
                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }


//                        if (bustUserSpList.size() > 1) {
//                            ArrayAdapter<String> adapter =
//                                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, bustUserSpList);
//                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            binding.spBustUser.setAdapter(adapter);
//                            binding.spBustUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                @Override
//                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                                    binding.spBustUser.setTag(bust_user.get(i).commonCd);
//                                }
//
//                                @Override
//                                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                                }
//                            });
//                        }


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    private void reqEntrancePmCar() {
        String[] cartype = new String[binding.rvList.getTags().size()];
        String[] carId = new String[binding.rvList.getTags().size()];
        int i = 0;
        for (String pm : binding.rvList.getTags()) {
            String[] pms = pm.split(" ");
            for (BasicOPM01Model item : bust_pm) {
                if (pms[0].equals(item.codeNm)) {
                    cartype[i] = item.commonCd;
                    carId[i] = pms[1];
                    i++;
                }
            }
        }


        showProgressDialog();
        String userName = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_NAME, "");
        String userID = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_ID, "");

        HashMap<String, Object> map = new HashMap<>();
        map.put("carno", carId);
        map.put("cartype", cartype);


        map.put("spotcd", binding.spAddressDong.getTag().toString());
        map.put("spotnm", binding.edAddress.getText().toString());
        map.put("spottime", binding.tvBustDate.getText().toString().replace("-", "").replace(" ", "").replace(":", "")+"00");
        map.put("towtime", binding.tvEnterTime.getText().toString().replace("-", "").replace(" ", "").replace(":", "")+"00");
        map.put("takingperson", userName);
        map.put("pnucode", binding.tvBustAgency.getTag().toString());
        map.put("spotgroup", binding.spBustTeam.getTag().toString());
        map.put("spotperson", binding.tvBustUser.getTag().toString());
        map.put("id", userID);


        APICallRuller.getInstance().addCall(new CallWaitPm(mContext, toRequestBody(map), new ActionResponseListener<BaseDataModel>() {
            @Override
            public void onResponse(BaseDataModel mData) {
                try {
                    Toast.makeText(mContext, "정상 처리 하였습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }
}
