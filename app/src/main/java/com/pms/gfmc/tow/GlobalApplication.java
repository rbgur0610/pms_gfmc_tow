package com.pms.gfmc.tow;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.multidex.MultiDex;

import com.bumptech.glide.Glide;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.utils.Logger;


/**
 * 이미지를 캐시를 앱 수준에서 관리하기 위한 애플리케이션 객체이다.
 * 로그인 기반 샘플앱에서 사용한다.
 *
 * @author MJ
 */
public class GlobalApplication extends Application {
    private static volatile GlobalApplication instance = null;
    //    private static volatile Activity currentActivity = null;
    private static Context mContext;


    public static Context getContext() {
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = this;
        Logger.init();
        PrefData.initializeInstance(this);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE) {
            //메모리가 부족해지는 경우 다른 프로세스에서 메모리를 확보시킴.
            super.onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_MODERATE);
        } else {
            super.onTrimMemory(level);
        }
        Glide.get(this).trimMemory(level);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }



    public static void setStatusBarColor(Activity activity, int color) {

        activity.runOnUiThread(() -> {
            Window window = activity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (color != Color.parseColor("#ffffff") ) {
                    window.getDecorView().setSystemUiVisibility(0);
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                }
            }

            window.setStatusBarColor(color);
        });

    }
}