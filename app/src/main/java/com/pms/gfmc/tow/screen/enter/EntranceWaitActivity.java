package com.pms.gfmc.tow.screen.enter;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.databinding.ActivityRestoreListBinding;
import com.pms.gfmc.tow.databinding.ActivityWaitListBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.http.model.BustListModel;
import com.pms.gfmc.tow.http.model.RestoreListModel;
import com.pms.gfmc.tow.http.model.Tocim01;
import com.pms.gfmc.tow.http.model.WaitListModel;
import com.pms.gfmc.tow.http.request.CallRestoreListCar;
import com.pms.gfmc.tow.http.request.CallToInCar;
import com.pms.gfmc.tow.http.request.CallWaitList;
import com.pms.gfmc.tow.http.request.CallWaitPm;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.adapter.RestoreListAdapter;
import com.pms.gfmc.tow.screen.adapter.WaitListAdapter;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.view.DateRangePicker;

import java.util.HashMap;
import java.util.List;

public class EntranceWaitActivity extends BaseActivity {

    private ActivityWaitListBinding binding;
    private String date_from;
    private String date_to;
    private WaitListAdapter mAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityWaitListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnBack.setOnClickListener(view -> finish());

        date_from = DateUtil.addDay("yyyyMMdd", -3);
        date_to = DateUtil.getYmdhms("yyyyMMdd");
        String rangeTime = date_from + " ~ " + date_to;
        binding.tvSearchDate.setText(rangeTime);
        binding.tvSearchDate.setOnClickListener(view -> {
            initDateRangePicker();
        });

        mAdapter = new WaitListAdapter(mContext);
        binding.rvList.setAdapter(mAdapter);


        binding.btnConfirm.setOnClickListener(v->{
            reqEntranceCar();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        reqList();
    }




    private void reqList() {

        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("date_from", date_from);
        map.put("date_to", date_to);
        map.put("instate", "");
        map.put("carref01", "");


        APICallRuller.getInstance().addCall(new CallWaitList(mContext, map, new ActionResponseListener<WaitListModel>() {
            @Override
            public void onResponse(WaitListModel mData) {
                try {

                    if (mData != null && mData.tocim01 != null && mData.tocim01.size() > 0) {
                        mAdapter.setListData(mData.tocim01);
                    } else {
                        mAdapter.setListData(null);
                        Toast.makeText(mContext, "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "입차에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

    private void initDateRangePicker() {
        final DateRangePicker dateRangePicker = new DateRangePicker(this, new DateRangePicker.OnCalenderClickListener() {
            @Override
            public void onDateSelected(String selectedStartDate, String selectedEndDate) {
                date_from = selectedStartDate;
                date_to = selectedEndDate;
                binding.tvSearchDate.setText(selectedStartDate + " ~ " + selectedEndDate);
            }
        });
        dateRangePicker.show();
        dateRangePicker.setBtnPositiveText("확인");
        dateRangePicker.setBtnNegativeText("취소");
    }


    private void reqEntranceCar() {

        List<Tocim01> selectList = mAdapter.getSelectItems();

        if(selectList.size()==0){
            Toast.makeText(mContext, "선택된 값이 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] carno = new String[selectList.size()];
        String[] inno = new String[selectList.size()];
        int i = 0;
        for(Tocim01 item : selectList){
            carno[i] = item.CARNO;
            inno[i] = item.INNO;
            i++;
        }


        showProgressDialog();
        String userID = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_ID, "");

        HashMap<String, Object> map = new HashMap<>();
        map.put("carno", carno);
        map.put("inno", inno);
        map.put("id", userID);


        APICallRuller.getInstance().addCall(new CallToInCar(mContext, toRequestBody(map), new ActionResponseListener<BaseDataModel>() {
            @Override
            public void onResponse(BaseDataModel mData) {
                try {
                    Toast.makeText(mContext, "정상 처리 하였습니다.", Toast.LENGTH_SHORT).show();
                    reqList();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }
}
