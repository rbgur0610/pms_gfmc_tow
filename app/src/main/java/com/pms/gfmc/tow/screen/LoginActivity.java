package com.pms.gfmc.tow.screen;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.normal.TedPermission;
import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.databinding.ActivityLoginBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BasicCodeModel;
import com.pms.gfmc.tow.http.model.LoginModel;
import com.pms.gfmc.tow.http.request.CallBasicCode;
import com.pms.gfmc.tow.http.request.CallLoginData;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.update.config.DownloadKey;
import com.pms.gfmc.tow.update.view.UpdateDialog;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.Logger;
import com.pms.gfmc.tow.utils.StringUtil;

import java.util.HashMap;
import java.util.List;

public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#0e63ab"));

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            TedPermission.create()
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("권한 설정을 해주세요. 권한설정을 거절한 경우 어플리케이션 설정->권한에서 직접 설정하실수 있습니다.")
                    .setPermissions(Manifest.permission.GET_ACCOUNTS, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.BLUETOOTH_ADVERTISE, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        } else {
            TedPermission.create()
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("권한 설정을 해주세요. 권한설정을 거절한 경우 어플리케이션 설정->권한에서 직접 설정하실수 있습니다.")
                    .setPermissions(Manifest.permission.GET_ACCOUNTS, Manifest.permission.BLUETOOTH, Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        }


        binding.btnLoginConfirm.setOnClickListener(v -> {
            reqData();
        });


      String saveUserId =   PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_ID);
      if(!StringUtil.isEmpty(saveUserId)){
          binding.edId.setText(saveUserId);
      }


//        //test
//        binding.edPwd.setText("12345");


        binding.edPwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    reqData();
                    return true;
                }
                return false;
            }
        });
    }


    private void reqData() {
        if (binding.edId.getText().toString().equals("")) {
            Toast.makeText(mContext, "아이디를 입력해 주세요.", Toast.LENGTH_SHORT).show();
            return;
        } else if (binding.edPwd.getText().toString().equals("")) {
            Toast.makeText(mContext, "비밀번호를 입력해 주세요.", Toast.LENGTH_SHORT).show();
            return;
        }


        showProgressDialog();

        HashMap<String, String> map = new HashMap<>();
        map.put("id", binding.edId.getText().toString());
        map.put("pwd", binding.edPwd.getText().toString());


        APICallRuller.getInstance().addCall(new CallLoginData(mContext, map, new ActionResponseListener<LoginModel>() {
            @Override
            public void onResponse(LoginModel mData) {

                if (mData.result.equals("success")) {


                    int deviceVersion = StringUtil.sToi(AndroidUtil.getAppVersion(mContext).replace(".", ""));
                    Log.d(TAG, " myAppVersion >>> " + deviceVersion);
                    int serverVersion = StringUtil.sToi(mData.login_info.get(0).APP_VERSION.replace(".",""));
                    Log.d(TAG, " serverAppVersion >>> " + serverVersion);

                    if (serverVersion > deviceVersion) {

                        DownloadKey.apkUrl = mData.login_info.get(0).APP_URL;
                        DownloadKey.version = mData.login_info.get(0).APP_VERSION;
                        DownloadKey.changeLog ="최신 업데이트 파일이 있습니다. 다운로드 해주세요.";

                        startActivityForResult(new Intent(mContext, UpdateDialog.class),UpdateDialog.REQUEST_CODE);
                        return;

                    } else {
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_ID, binding.edId.getText().toString());
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_NAME, mData.login_info.get(0).NAME);

                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_VENDERNM, mData.login_info.get(1).vendernm);
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_VENDERNO, mData.login_info.get(1).venderno);
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_COMMENT, mData.login_info.get(1).Comments);
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_CEO, mData.login_info.get(1).ceo);
                        PrefData.getInstance(mContext).putString(PrefData.SHARED_PREF_TELNO, mData.login_info.get(1).telno);


                        startActivity(new Intent(mContext, MainActivity.class));
                        finish();
                    }

                } else {
                    Toast.makeText(mContext, "로그인에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }


                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "로그인에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    private void getBasicCode() {
        showProgressDialog();

        APICallRuller.getInstance().addCall(new CallBasicCode(mContext, new ActionResponseListener<BasicCodeModel>() {
            @Override
            public void onResponse(BasicCodeModel mData) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "기본코드 갱신에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {

//            getBasicCode();

        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            Toast.makeText(mContext, "권한을 승인해주세요.\n", Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        twoButtonDialog(Constants.MAINACTIVITY_DESTROY_ACTIVITY, getString(R.string.title_exit), getString(R.string.msg_exit_app)).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == UpdateDialog.REQUEST_CODE) {
            finish();
        }

    }
}
