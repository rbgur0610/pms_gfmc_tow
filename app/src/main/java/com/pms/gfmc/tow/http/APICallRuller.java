package com.pms.gfmc.tow.http;


import android.os.Handler;
import android.os.Looper;

import java.util.LinkedList;
import java.util.List;

public class APICallRuller {
    // member variables
    private static LinkedList<Runnable> mRunnableList = new LinkedList<>();
    private static final APICallRuller ruler = new APICallRuller();
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    // constructor
    private APICallRuller() {
    }

    public static APICallRuller getInstance() {
        return ruler;
    }

    public static boolean isListEmpty(List list) {
        return (list == null) || (list.size() == 0);
    }

    public synchronized void runNext() {
        try {
            if (!isListEmpty(mRunnableList)) {
                Runnable runnable = mRunnableList.poll();
                if (runnable != null) {
                    mHandler.post(runnable);
                } else {
                    runFinishAction();
                }
            } else {
                runFinishAction();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void finish() {
        clear();
        runFinishAction();
    }

    public void addCall(APICall call) {
        mRunnableList.add(call);
    }

    public void addCallAtFirst(APICall call) {
        mRunnableList.addFirst(call);
    }

    public int getCount() {
        if (isListEmpty(mRunnableList)) {
            return 0;
        }
        return mRunnableList.size();
    }

    private void clear() {
        mRunnableList.clear();
    }

    private APICall mFinishAction = null;

    private void runFinishAction() {
        if (mFinishAction != null) {
            APICall temp = mFinishAction;
            mFinishAction = null;
            temp.run();
        }
    }

}

