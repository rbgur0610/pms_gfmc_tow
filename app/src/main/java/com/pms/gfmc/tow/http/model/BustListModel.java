package com.pms.gfmc.tow.http.model;

import java.util.List;

public class BustListModel extends BaseDataModel {
    public List<Stopm08Model> sysopm08;
    public List<BustCountModel> count;
}
