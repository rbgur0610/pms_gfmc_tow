package com.pms.gfmc.tow.http.model;

public class BasicOPM04Model {
    public int BASETIME;
    public String OFFDAY;
    public int DAYAMT;

    public int MONTHAMT;
    public String FREETIME;
    public String DISCOUNTYN;

    public int BILLAMT;
    public int BASEAMT;
    public int BILLTIME;

    public String PARKCODE;
    public String PARKNAME;
}
