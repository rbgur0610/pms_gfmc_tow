package com.pms.gfmc.tow.http.model;

import java.util.List;

public class BaseDataModel {
    public String alert;
    public String result;

    public String id;
    public String qrVersion;
    public List<String> value;
}
