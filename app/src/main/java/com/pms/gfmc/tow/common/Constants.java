package com.pms.gfmc.tow.common;

import android.os.Environment;


public class Constants  {

    //true -> 운영, false -> 개발
    public static Boolean APP_DISTRIBUTION = false;
    //true -> 로그보기, false -> 로그 안 보기.
    public static Boolean SHOW_LOG_MODE = true;

    public static final String PARK_NO = "0101";

    public static final String BASE_PUBLIC_IP = "http://totalgfmc.easyonpark.co.kr:5003/";
    public static final String BASE_CAR_INFO_IP = "http://park.e-junggu.or.kr/";
    public static final String URL_LOGIN = "easyonapi/topark/toParkApi.do?method=toLogin";
    public static final String URL_BASIC_PUBLIC_CODE= "easyonapi/topark/toParkApi.do?method=toCodeList";
    public static final String URL_RESTORE_LIST= "easyonapi/topark/toParkApi.do?method=toOutList";
    public static final String URL_CAR_SIZE= "get_car_info.do";
    public static final String URL_WAIT_LIST= "easyonapi/topark/toParkApi.do?method=toWaitList";
    public static final String URL_WAIT_CAR= "easyonapi/topark/toParkApi.do?method=toWaitCar";
    public static final String URL_TOIN_LIST= "easyonapi/topark/toParkApi.do?method=toInList";
    public static final String URL_WAIT_PM= "easyonapi/topark/toParkApi.do?method=toWaitPM";
    public static final String URL_TOIN_CAR= "easyonapi/topark/toParkApi.do?method=toInCar";
    public static final String URL_TOIN_DETAIL= "easyonapi/topark/toParkApi.do?method=toInDetail";
    public static final String URL_TOOUT_CAR= "easyonapi/topark/toParkApi.do?method=toOutCar";
    public static final String URL_PAY_INFO= "easyonapi/topark/toParkApi.do?method=toPayInfo";

    public static final String IMG_SAVE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PMS/IMAGE";



    public static final int MAINACTIVITY_DESTROY_ACTIVITY = 0x2200 + 7;


    // 입차 차량번호 입력 모드 (NORMAL : 일반, MANUAL : 수동, AUTO : 번호인식, UPDATE : 차량번호수정, TICKET : 정기권)
    public static final String CAR_NORMAL = "NORMAL";
    public static final String CAR_MANUAL = "MANUAL";
    public static final String CAR_AUTO = "AUTO";
    public static final String CAR_UPDATE = "UPDATE";
    public static final String CAR_TICKET = "TICKET";
    public static final String CAR_INCAR = "INCAR";

    // 입차 차량번호 입력 모드 키
    public static final String CAR_INPUT_MODE = "car_input_mode";
    public static final String CAR_INPUT_NO = "CAR_INPUT_NO";
    public static final String CAR_AREA = "CAR_AREA";
    public static final String NEXT_ACTIVITY = "NEXT_ACTIVITY";
    public static final String CAR_CINO = "CAR_CINO";
    public static final String UNPARK_NO = "CAR_CINO";
    // 번호 인식 시에 저장 된 주차번호
    public static String AUTO_PIO_NUMBER = "";
    // 번호 인식 시에 저장 된 차량번호
    public static String AUTO_CAR_NUMBER = "";
    // 번호 인식 시에 저장 된 이미지 패스
    public static String AUTO_CAR_IMG_PATH = "";
    // 번호 인식 시에 저장 된 이미지 파일
    public static String AUTO_CAR_IMG_NAME = "";

    // 키보드 입력 차량번호
    public static String KEY_CARNO_1 = "";
    public static String KEY_CARNO_2 = "";
    public static String KEY_CARNO_3 = "";
    public static String KEY_CARNO_4 = "";
    public static String KEY_CARNO_MANUAL = "";


    public static final String PICTURE_TYPE_P1 = "P1"; // 입차사진
    public static final String PICTURE_TYPE_O1 = "O1"; // 출차 정보 사진
    public static final String PICTURE_TYPE_R1 = "R1"; // 정기권 사진
    public static final String PICTURE_TYPE_T1 = "T1"; // 출근정보 사진
    public static final String PICTURE_TYPE_T2 = "T2"; // 퇴근정보 사진
    public static final String PICTURE_TYPE_PP = "PP"; // 전체 사진

    public static final String PREFERENCE_ADJUST_AMT = "ADJUST_AMT";
    // 이미지 확장자 체크
    public static final String IMG_EXT = "jpg|jpeg|png|bmp|gif|tiff";

    public static final int COMMON_TYPE = 0x111123;
    public static final int COMMON_TYPE_TIME_DC = COMMON_TYPE + 1;
    public static final int COMMON_TYPE_CARD_PAYMENT_FAIL	   = COMMON_TYPE+20;


    public static final String PREFERENCE_MCPAY_TRANSACTION_NO = "TRANSACTION_NO";
    public static final String PREFERENCE_MCPAY_APPROVAL_NO = "APPROVAL_NO";
    public static final String PREFERENCE_MCPAY_APPROVAL_DATE = "APPROVAL_DATE";
    public static final String PREFERENCE_MCPAY_CARD_NO = "CARD_NO";
    public static final String PREFERENCE_MCPAY_CARD_COMPANY = "CARD_COMPANY";
    public static final String PREFERENCE_MCPAY_TOTAL_AMT = "MCPAY_TOTAL_AMT";
    public static final String PREFERENCE_MCPAY_PARAMETER = "PAYDATA_PARAMETER";

}
