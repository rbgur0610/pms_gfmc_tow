package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.PayInfoModel;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallPayInfo extends APICall {

    public ActionResponseListener<PayInfoModel> actionResponseListener;
    private RequestBody map;

    public CallPayInfo(Context context, RequestBody map, ActionResponseListener<PayInfoModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
        this.map = map;
    }

    @Override
    public void run() {

        call = APIClient.getInstance().getPayInfo(map);
        call.enqueue(new Callback<PayInfoModel>() {
            @Override
            public void onResponse(Call<PayInfoModel> call, Response<PayInfoModel> response) {
                callDone(response, CallPayInfo.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<PayInfoModel> call, Throwable t) {
                callDone(t, CallPayInfo.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
