package com.pms.gfmc.tow.print;

import java.io.Serializable;

public class PrintData implements Serializable{
	private static final long serialVersionUID = 1L;
	public String carNum;
	public String curbDate;
	public String location;
	public String PayAmt;
	public String control_num;
	public String cd_class;
	public String type;
	public String group;
	public String proc;
	public String bank_vcnt;
	public String bank_name;
	public String ipgm_date;
}
