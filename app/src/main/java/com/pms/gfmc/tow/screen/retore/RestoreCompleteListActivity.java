package com.pms.gfmc.tow.screen.retore;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.databinding.ActivityRestoreListBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.RestoreListModel;
import com.pms.gfmc.tow.http.request.CallRestoreListCar;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.adapter.RestoreCompleteListAdapter;
import com.pms.gfmc.tow.screen.adapter.RestoreListAdapter;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;
import com.pms.gfmc.tow.view.DateRangePicker;

import java.util.ArrayList;
import java.util.HashMap;

public class RestoreCompleteListActivity extends BaseActivity {

    private ActivityRestoreListBinding binding;
    private String date_from;
    private String date_to;
    private RestoreCompleteListAdapter mAdapter;
    private String carref01 = "";
    private ArrayList<String> carTypeList = new ArrayList<>();
    private ArrayList<String> carTypeCdList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityRestoreListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnBack.setOnClickListener(view -> finish());

        String userName = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_NAME, "");
        date_from = DateUtil.addDay("yyyyMMdd", -3);
        date_to = DateUtil.getYmdhms("yyyyMMdd");
        String rangeTime = date_from + " ~ " + date_to;
        binding.tvSearchDate.setText(rangeTime);
        binding.tvSearchDate.setOnClickListener(view -> {
            initDateRangePicker();
        });


        mAdapter = new RestoreCompleteListAdapter(mContext);
        binding.rvList.setAdapter(mAdapter);

        binding.btnSearch.setOnClickListener(v -> {
            AndroidUtil.hideKeyboard(mContext, binding.edCarnum);
            reqtList();
        });

        binding.edCarnum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                AndroidUtil.hideKeyboard(mContext, binding.edCarnum);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    reqtList();
                    return true;
                }
                return false;
            }
        });



//        carTypeList = new ArrayList<>();
//        carTypeCdList = new ArrayList<>();
//
//        carTypeCdList.add("");
//        carTypeList.add("차량구분");
//
//        ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
//        for (String item : basicCodes) {
//            if (item.contains("T015")) {
//                String[] carType = item.split("-");
//                carTypeCdList.add(carType[1]);
//                carTypeList.add(carType[2]);
//            }
//        }
//
//        ArrayAdapter<String> adapter =
//                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, carTypeList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        binding.spCarType.setAdapter(adapter);
//        binding.spCarType.setOnItemSelectedListener(new carTypeSelectedListener());

    }

    @Override
    protected void onResume() {
        super.onResume();
        reqtList();
    }


    private void reqtList() {

        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("date_from", date_from);
        map.put("date_to", date_to);
        map.put("carno", binding.edCarnum.getText().toString());
//        map.put("carref01", carref01);

        APICallRuller.getInstance().addCall(new CallRestoreListCar(mContext, map, new ActionResponseListener<RestoreListModel>() {
            @Override
            public void onResponse(RestoreListModel mData) {
                try {

                    if (mData != null && mData.tocim02 != null && mData.tocim02.size() > 0) {
                        mAdapter.setListData(mData.tocim02);
                    } else {
                        mAdapter.setListData(null);
                        Toast.makeText(mContext, "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

    private void initDateRangePicker() {
        final DateRangePicker dateRangePicker = new DateRangePicker(this, new DateRangePicker.OnCalenderClickListener() {
            @Override
            public void onDateSelected(String selectedStartDate, String selectedEndDate) {
                date_from = selectedStartDate;
                date_to = selectedEndDate;
                binding.tvSearchDate.setText(selectedStartDate + " ~ " + selectedEndDate);
            }
        });
        dateRangePicker.show();
        dateRangePicker.setBtnPositiveText("확인");
        dateRangePicker.setBtnNegativeText("취소");
    }


    public class carTypeSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            carref01 = carTypeCdList.get(position);
            reqtList();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // Do nothing
        }
    }
}
