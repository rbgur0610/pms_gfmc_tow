package com.pms.gfmc.tow.http;

public interface ActionResponseListener<T> {
    void onResponse(T response);
    void onFailure();
}
