package com.pms.gfmc.tow.http;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.pms.gfmc.tow.GlobalApplication;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class APICall implements Runnable {
    private Context context;
    protected Call call;

    public APICall(Context context) {
        this.context = GlobalApplication.getContext();
    }

    public enum ResultType {
        ACTION_RESULT_RUNNEXT,
        ACTION_RESULT_ERROR_DISABLE_NETWORK,
        ACTION_RESULT_ERROR_NOT_RESPONSE,
        ACTION_RESULT_ERROR_INVALID_TOKEN
    }

    @Override
    public void run() {
    }

    protected Context getContext() {
        return context;
    }

    protected void callDone(@NonNull Throwable th, APICall call) {
        call.callCancel();
        callDone(ResultType.ACTION_RESULT_ERROR_NOT_RESPONSE);
    }

    protected void callDone(Response response, APICall call) {
        if (response.code() == 200) {
            callDone(ResultType.ACTION_RESULT_RUNNEXT);
        } else {
            try {
                call.callCancel();
                if (response.errorBody() != null) {
                    callDone(ResultType.ACTION_RESULT_ERROR_NOT_RESPONSE);
                }
            } catch (Exception e) {
                e.printStackTrace();
                callDone(ResultType.ACTION_RESULT_ERROR_NOT_RESPONSE);
            }
        }
    }

    protected void callDone(ResultType type) {

    }


    protected String listToString(List list) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            result.append("").append(list.get(i));
            if (i < list.size() - 1) {
                result.append(",");
            }
        }
        return result.toString();
    }

    protected void callCancel() {
        if (call != null) {
            call.cancel();
        }
    }

    public MultipartBody.Part getMultipartBody(String content_type,String paramName, File file) {//"multipart/form-data"
        CountingFileRequestBody requestBody = new CountingFileRequestBody(content_type, file, getListener(file.length()));
        MultipartBody.Part body = MultipartBody.Part.createFormData(paramName, file.getName(), requestBody);
        return body;
    }

    private CountingFileRequestBody.ProgressListener getListener(final long fileSize) {
        return new CountingFileRequestBody.ProgressListener() {
            @Override
            public void transferred(long num) {
                Log.d("PRETTY", "fileSize:" + FileFormat.convert(fileSize) + ", num:" + FileFormat.convert(num) + ", progress:" + ((float) num / (float) fileSize * 100f));
            }
        };
    }

    public static RequestBody toRequestBody (String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body ;
    }

}
