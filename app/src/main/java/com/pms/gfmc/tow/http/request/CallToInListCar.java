package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.WaitListModel;
import com.pms.gfmc.tow.http.model.WaitListModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallToInListCar extends APICall {

    public ActionResponseListener<WaitListModel> actionResponseListener;
    private HashMap<String, String> map;

    public CallToInListCar(Context context, HashMap<String, String> map, ActionResponseListener<WaitListModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
        this.map = map;
    }

    @Override
    public void run() {

        call = APIClient.getInstance().getToInList(map);
        call.enqueue(new Callback<WaitListModel>() {
            @Override
            public void onResponse(Call<WaitListModel> call, Response<WaitListModel> response) {
                callDone(response, CallToInListCar.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<WaitListModel> call, Throwable t) {
                callDone(t, CallToInListCar.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
