package com.pms.gfmc.tow.preference;

import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.reflect.TypeToken;
import com.pms.gfmc.tow.utils.GsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by songsanghyeon on 2016. 9. 1..
 */
public class PrefData extends Preference {
    private static PrefData mInstance;
    private final String PREF_MAIN = "pref.setting";

    // 로그인 관련

    public static final String SHARED_PREF_ID = "SHARED_PREF_ID";
    public static final String SHARED_PREF_NAME = "SHARED_PREF_NAME";
    public static final String SHARED_PREF_BASIC_CODE = "SHARED_PREF_BASIC_CODE";
    public static final String SHARED_PREF_PARKCODE = "SHARED_PREF_PARKCODE";
    public static final String SHARED_PREF_PARKNAME = "SHARED_PREF_PARKNAME";
    public static final String SHARED_PREF_DP_CODE = "SHARED_PREF_DP_CODE";

    public static final String SHARED_PREF_VENDERNM = "SHARED_PREF_VENDERNM";
    public static final String SHARED_PREF_VENDERNO = "SHARED_PREF_VENDERNO";
    public static final String SHARED_PREF_COMMENT = "SHARED_PREF_COMMENT";
    public static final String SHARED_PREF_CEO = "SHARED_PREF_CEO";
    public static final String SHARED_PREF_TELNO = "SHARED_PREF_TELNO";


    public static String PRINT_NAME = "print_name";

    private final String PREF_NAME = "pref.name";

    protected PrefData(Context appContext) {
        super(appContext);
        mContext = appContext;
        mSharedPreferences = mContext.getSharedPreferences(PREF_MAIN, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PrefData(context);
        }
    }

    public static synchronized PrefData getInstance(Context context) {
        if (mInstance == null)
            synchronized (PrefData.class) {
                mInstance = new PrefData(context);
            }

        return mInstance;
    }


    /**
     * String
     */
    public void putListString(String key, List<String> list) {

        if (list == null) {
            return;
        }

        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, GsonUtils.create().toJson(list));
        editor.apply();
    }

    public List<String> getListString(String key) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String value = pref.getString(key, "");
        List<String> list = null;

        try {
            list = GsonUtils.create().fromJson(value, new TypeToken<List<String>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (list == null) {
            list = new ArrayList<>();
        }

        return list;
    }

    public void remove(String key) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Service.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.remove(key);
        edit.apply();
    }

}
