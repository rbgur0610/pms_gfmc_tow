package com.pms.gfmc.tow.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.utils.StringUtil;
import com.squareup.picasso.Picasso;


public class CarPictureViewDetailActivity extends Activity {

	
	private ImageView iv_image;
	private String imageURL = "";
	
	private String TAG = this.getClass().getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_car_picture_view_detail);
		
		Intent intent = getIntent();
		if (intent != null) {
			imageURL = StringUtil.isNVL(intent.getStringExtra("imageURL"));
			Log.d(TAG, " viewDetail ::: imageURL >>> " + imageURL);
		}
	
		iv_image = (ImageView) findViewById(R.id.iv_image);
		iv_image.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		finish();
        	}
        });

		/***
		File imgFile = new File(imagePath);
		
		if(imgFile.exists()){			
			ImageLoader.setImageView(imagePath,iv_image);
		} else{
			MsgUtil.ToastMessage(this, "파일이 존재하지 않습니다.");
		}
		***/
		
		if(!StringUtil.isEmpty(imageURL)){
			Picasso.get().load(Constants.BASE_PUBLIC_IP + imageURL) .into(iv_image);
		}

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
}
