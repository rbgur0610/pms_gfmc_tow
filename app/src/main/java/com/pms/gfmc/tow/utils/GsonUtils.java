package com.pms.gfmc.tow.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by mwkim
 * on 18/01/2019.
 */
public class GsonUtils {

    public static Gson create() {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory());
        gsonBuilder.serializeNulls();

        return gsonBuilder.create();
    }
}
