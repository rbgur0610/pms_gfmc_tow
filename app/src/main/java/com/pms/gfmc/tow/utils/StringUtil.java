package com.pms.gfmc.tow.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by KimJehyun on 2017. 1. 11..
 */

public class StringUtil {


    public static String isNVL(String src) {
        return src != null && src.trim().length() != 0 ? src : "";
    }

    public static String isNVL(String src, String val) {
        return src != null && src.trim().length() != 0 ? src : val;
    }

    public static String isNVL(String src, int val) {
        return src != null && src.trim().length() != 0 ? src : String.valueOf(val).toString();
    }


    public static String paddingLeftCharZero(String s, int len) {
        return paddingLeftChar(s, '0', len);
    }
    public static String paddingLeftChar(String s, char c, int len) {
        while (s != null && s.length() < len) {
            s = c + s;
        }

        /*
        Formatter fmt = new Formatter();
        fmt.format("%-" + len + "s", s);
        s = fmt.toString().replaceAll(" ", c+"");
        */

        return s;
    }
    public static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() == 0) {
            return null;
        }

        byte[] ba = new byte[hex.length() / 2];
        for (int i = 0; i < ba.length; i++) {
            ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return ba;
    }

    // byte[] to hex
    public static String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (int x = 0; x < ba.length; x++) {
            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }

    public static byte[] zip(byte[] dataByte) {

        // compress the byte
        Deflater def = new Deflater();
        def.setLevel(Deflater.BEST_COMPRESSION);
        def.setInput(dataByte);
        def.finish();

        ByteArrayOutputStream byteArray = new ByteArrayOutputStream(dataByte.length);

        byte[] buf = new byte[1024];

        while (!def.finished()) {
            int compByte = def.deflate(buf);
            byteArray.write(buf, 0, compByte);
        }

        try {
            byteArray.close();
        } catch (IOException ioe) {

        }

        byte[] comData = byteArray.toByteArray();

        return comData;
    }

    public static String getExtension(String path) {
        try {
            final String[] extensions = path.split("\\.");
            return extensions[extensions.length - 1].toLowerCase();
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] unzip(byte[] comData) throws DataFormatException {

        // Decompress the bytes
        Inflater decompresser = new Inflater();
        decompresser.setInput(comData, 0, comData.length);

        byte[] result = new byte[1024];

        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();

        while (!decompresser.finished()) {
            int resultLength = decompresser.inflate(result);
            byteArray.write(result, 0, resultLength);
        }

        decompresser.end();

        try {
            byteArray.close();
        } catch (IOException ioe) {

        }

        return byteArray.toByteArray();
    }

    public static String addZero(int i, int len) {
        return addZero(String.valueOf(i), len);
    }

    public static String addZero(String s, int len) {
        if (s == null) s = "";

        String result = "";
        String prefix = "";
        int sLen = 0;

        if (s.startsWith("-")) {
            prefix = "-";
            result = s.substring(1);
        } else {
            result = s;
        }

        sLen = result.length();

        for (int i = 0; i < len - sLen; i++) {
            result = "0" + result;
        }

        return prefix + result;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String removeZero(String s) {
        if (s == null) return "";
        else s = s.trim();

        boolean remove = true;

        StringBuffer buff = new StringBuffer();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (remove) {
                if (ch == '0') continue;
                else remove = false;
            }

            buff.append(ch);
        }

        return buff.toString();
    }

    public static String transWhTokWh(int iValue) {
        iValue = iValue / 1000;

        return String.valueOf(iValue);
    }

    public static String transWhTokWh(String s) {
        int iValue = 0;

        if (s == null) return "";

        try {
            iValue = Integer.parseInt(s.trim());
            iValue = iValue / 1000;
        } catch (Exception e) {
        }

        return String.valueOf(iValue);
    }

    public static String transkWhToWh(String s) {
        int iValue = 0;

        if (s == null) return "";

        try {
            iValue = Integer.parseInt(s.trim());
            iValue = iValue * 1000;
        } catch (Exception e) {
        }

        return String.valueOf(iValue);
    }

    public static String addComma(int v) {
        return addComma(String.valueOf(v));
    }

    public static String addComma(String s) {
        if (s == null || s.equals("") || s.equalsIgnoreCase("null")) return "0";

        String prefix = "";
        StringBuffer buffer = new StringBuffer();

        if (s.startsWith("-")) {
            prefix = "-";
            buffer.append(s.substring(1));
        } else {
            buffer.append(s);
        }

        String tmp = buffer.reverse().toString();
        buffer.setLength(0);

        for (int i = 0; i < tmp.length(); i++) {
            buffer.append(tmp.charAt(i));

            if ((i + 1) % 3 == 0) buffer.append(",");
        }

        tmp = buffer.reverse().toString();

        if (tmp.startsWith(",")) tmp = tmp.substring(1);

        return prefix + tmp;
    }

    public static int toInt(String str) {
        int i = 0;

        try {
            i = Integer.parseInt(str.trim());
        } catch (Exception e) {

        }

        return i;
    }

    public static double toDouble(String str) {
        double i = 0D;

        try {
            i = Double.parseDouble(str.trim());
        } catch (Exception e) {

        }

        return i;
    }

    public static boolean equalsArrayElement(String[] array, String compare) {
        if (array == null) {
            return false;
        }

        for (String subject : array) {
            if (!TextUtils.isEmpty(compare)) {
                if (subject.equalsIgnoreCase(compare.trim())) {
                    return true;
                }
            }
        }

        return false;
    }


    public static String replaceNullIntent(Bundle eB, String key){
        if(eB == null || eB.getString(key) == null)
            return "";

        return eB.getString(key);
    }
    public static boolean isValidString(String str) {
        try {
            return str != null && str.length() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static String isValidStringReturn(String str) {
        try {
            if (str != null && str.length() > 0) {
                return str;
            }

            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isValidNumberString(String str) {
        try {
            if (str != null && str.length() > 0) {
                return isNumeric(str);
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static String chkValidStrTrim(String str) {
        try {
            if (str != null && str.length() > 0) {
                return str.trim().replace("%20", "");
            }

            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static String chkValidNumberStrTrim(String str) {
        try {
            if (str != null && str.length() > 0 && isNumeric(str)) {
                return str.trim().replace("%20", "");
            }

            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isNumeric(String str) {
//        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }

    public static String md5(String s) {
        StringBuffer md5 = new StringBuffer();
        try {
            byte[] digest = java.security.MessageDigest.getInstance("MD5").digest(s.getBytes());
            for (int i = 0; i < digest.length; i++) {
                md5.append(Integer.toString((digest[i] & 0xf0) >> 4, 16));
                md5.append(Integer.toString(digest[i] & 0x0f, 16));
            }

        } catch (java.security.NoSuchAlgorithmException e) {
            Log.e(s, "md5() error..." + e.getMessage());
        }
        return md5.toString();
    }

    public static void setAritaTypeface(Context context, TextView tv) {
        // Auto-generated method stub
        final String TYPEFACE_NAME = "TTF_Medium.ttf";
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), TYPEFACE_NAME);
        tv.setTypeface(typeface);
    }

    public static void setAritaTypeface(Context context, Button btn) {
        // Auto-generated method stub
        final String TYPEFACE_NAME = "TTF_Medium.ttf";
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), TYPEFACE_NAME);
        btn.setTypeface(typeface);
    }

    public static String getRootUrl(String url) {
        int slash = url.lastIndexOf("/");
        String rootUrl = url.substring(0, slash - 1);
        return rootUrl;
    }


    public static String fromHtml(String str) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(str).toString();
        }
        return Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY).toString();
    }


    public static String escapeHtml(CharSequence text) {
        StringBuilder out = new StringBuilder();
        withinStyle(out, text, 0, text.length());
        return out.toString();
    }

    private static void withinStyle(StringBuilder out, CharSequence text,
                                    int start, int end) {
        for (int i = start; i < end; i++) {
            char c = text.charAt(i);

            if (c == '<') {
                out.append("&lt;");
            } else if (c == '>') {
                out.append("&gt;");
            } else if (c == '&') {
                out.append("&amp;");
            } else if (c >= 0xD800 && c <= 0xDFFF) {
                if (c < 0xDC00 && i + 1 < end) {
                    char d = text.charAt(i + 1);
                    if (d >= 0xDC00 && d <= 0xDFFF) {
                        i++;
                        int codepoint = 0x010000 | (int) c - 0xD800 << 10 | (int) d - 0xDC00;
                        out.append("&#").append(codepoint).append(";");
                    }
                }
            } else if (c > 0x7E || c < ' ') {
                out.append("&#").append((int) c).append(";");
            } else if (c == ' ') {
                while (i + 1 < end && text.charAt(i + 1) == ' ') {
                    out.append("&nbsp;");
                    i++;
                }

                out.append(' ');
            } else {
                out.append(c);
            }
        }
    }

    public static String getPoint(String point) {
        if (TextUtils.isEmpty(point))
            return "0";

        int inValues = Integer.parseInt(StringUtil.chkValidNumberStrTrim(point));
        DecimalFormat Commas = new DecimalFormat("#,###");
        String result_int = Commas.format(inValues);
        return result_int;
    }

    /**
     * 이름 마스킹 처리(이름 중간 마스킹 처리)
     *
     * @param name
     * @return string
     */
    public static String maskingName(String name) {
        String maskedName = "";    // 마스킹 이름

        if (!TextUtils.isEmpty(name)) {
            String firstName = "";    // 성
            String lastName = "";    // 이름
            int lastNameStartPoint;    // 이름 시작 포인터

            firstName = name.substring(0, 1);
            lastNameStartPoint = name.indexOf(firstName);
            lastName = name.substring(lastNameStartPoint + 1);

            String makers = "";

            for (int i = 0; i < lastName.length(); i++) {
                if (lastName.length() != 1 && i == lastName.length() - 1) {
                    makers += lastName.substring(i, i + 1);
                } else {
                    makers += "*";
                }
            }

            lastName = lastName.replace(lastName, makers);
            maskedName = firstName + lastName;
        }

        return maskedName;
    }

    /**
     * 로그인 아이디 마스킹 처리(이름 중간 마스킹 처리)
     *
     * @param loginId
     * @return string
     */
    public static String maskingLoginId(String loginId) {
        String makersLoginId = "";    // 마스킹 이름

        if (!TextUtils.isEmpty(loginId)) {

            for (int i = 0; i < loginId.length(); i++) {
                if (i >= 5) {
                    makersLoginId += "*";
                } else {
                    makersLoginId += loginId.substring(i, i + 1);
                }
            }
        }

        return makersLoginId;
    }

    public static String getChangeName(String name) {
        try {
            StringBuffer sbName = new StringBuffer(name);
            String changeName = "";
            if (sbName.length() < 3) {
                changeName = sbName.replace(1, sbName.length(), "*").toString();
            } else {
                int sIdex = sbName.length() / 2 + 1;

                for (int i = sIdex; i < sbName.length(); i++) {
                    sbName.replace(i, i + 1, "*");
                }
                changeName = sbName.toString();
            }

            return changeName;
        } catch (Exception e) {
            Log.e("", e.getMessage());
            StringBuffer sbName = new StringBuffer(name);
            return sbName.replace(1, sbName.length() - 1, "*").toString();
        }
    }

    // 2019-12-09 kkbeom 텍스트 강조
    public static String getMaskingText(String text, String keyword) {
        if (text != null && !"".equals(text.trim()) && keyword != null && !"".equals(keyword.trim())) {
            String maskingText = text.replace(keyword, "<font color='#6e93f7'><b>" + keyword + "</b></font>");
            return maskingText;
        } else {
            return text;
        }
    }

    // 2019-12-23 단어별 줄바꿈 제거
    public static String getBreakSpaceText(String text) {
        if (text != null && !"".equals(text)) {
            return text.replace(" ", "\u00A0");
        } else {
            return text;
        }
    }


    // 2019-12-23 자동개행 제거
    public static String getBreakAutoNewLine(String text) {
        if (text != null && !"".equals(text)) {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < text.length(); i++) {
                sb.append(text.charAt(i));
                sb.append("\u200B");
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }

            return sb.toString();
        } else {
            return text;
        }
    }

    /**
     * 객체가 null인지 확인하고 null인 경우 "" 로 바꾸는 메서드
     *
     * @param object 원본 객체
     * @return resultVal 문자열
     */
    public static String isNullToString(Object object) {
        String string = "";

        if (object != null) {
            if (object instanceof String) {
                string = (String) object;
            } else {
                string = String.valueOf(object);
            }
        }

        return string.trim();
    }


    /**
     * <p>문자열 내부의 마이너스 character(-)를 모두 제거한다.</p>
     * <p/>
     * <pre>
     * StringUtil.removeMinusChar(null)       = null
     * StringUtil.removeMinusChar("")         = ""
     * StringUtil.removeMinusChar("a-sdfg-qweqe") = "asdfgqweqe"
     * </pre>
     *
     * @param str 입력받는 기준 문자열
     * @return " - "가 제거된 입력문자열
     * 입력문자열이 null인 경우 출력문자열은 null
     */
    public static String removeMinusChar(String str) {
        return remove(str, '-');
    }

    public static String removeDotChar(String str) {
        return remove(str, '.');
    }


    /**
     * <p>기준 문자열에 포함된 모든 대상 문자(char)를 제거한다.</p>
     * <p/>
     * <pre>
     * StringUtil.remove(null, *)       = null
     * StringUtil.remove("", *)         = ""
     * StringUtil.remove("queued", 'u') = "qeed"
     * StringUtil.remove("queued", 'z') = "queued"
     * </pre>
     *
     * @param str    입력받는 기준 문자열
     * @param remove 입력받는 문자열에서 제거할 대상 문자열
     * @return 제거대상 문자열이 제거된 입력문자열. 입력문자열이 null인 경우 출력문자열은 null
     */
    public static String remove(String str, char remove) {
        if (isEmpty(str) || str.indexOf(remove) == -1) {
            return str;
        }
        char[] chars = str.toCharArray();
        int pos = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != remove) {
                chars[pos++] = chars[i];
            }
        }
        return new String(chars, 0, pos);
    }

    public static int sToi(String s) {
        return sToi(s, 0);
    }

    public static int sToi(String s, int r) {
        try {
            int i = Integer.parseInt(s);
            return i;
        } catch (Exception var3) {
            var3.printStackTrace();
            return r;
        }
    }

}

