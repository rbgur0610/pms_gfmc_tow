package com.pms.gfmc.tow.http.model;

public class Stopm08Model {
    public String UNPARKDAY;
    public String PARKNO;
    public String CARNO;
    public String MEASURES;
    public String UNPARKNO;

    public String UNPARKTIME;
    public String WORKID;
    public String PAYAMT;
    public String LEVYST;
    public String REFCD2;

}
