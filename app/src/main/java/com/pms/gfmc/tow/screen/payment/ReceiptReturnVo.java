package com.pms.gfmc.tow.screen.payment;

import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;

import org.apache.http.util.ByteArrayBuffer;

/**
 * 입차 영수증
 */
public class ReceiptReturnVo extends ReceiptVo {

    /**
     *
     */
    private static final long serialVersionUID = 8449913322993640160L;
    public int payType = 0;


    public String title;            // 타이틀
    public String company;            // 공단명
    public String[] carNum;
    public String[] bustDate;
    public String[] saveDate;
    public String returnDate;

    public String[] bustAmt;
    public String[] saveAmt;

    public String totalAmt;


    public String returnUser;
    public String returnRelation;

    public String footerStr = "";
    public String ceoNm;
    public String managerNum;
    public String manager;

    public String empPhone;

    public String issuerName;
    public String cardNo;
    public String approvalNo;

    @Override
    public ByteArrayBuffer print() {

        byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
        byte[] wSize = getPrintOption(PRINT_TEXT_SIZE_WIDE);
        byte[] bSize = getPrintOption(PRINT_TEXT_SIZE_BOLD);
        byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);


        ByteArrayBuffer bf = new ByteArrayBuffer(1024);
        bf.append(wSize, 0, wSize.length);
        replaceByteDate(bf, "  [" + title + "]\n");//제일큰글자
        bf.append(nSize, 0, nSize.length);
        replaceByteDate(bf, "--------------------------------\n");
        replaceByteDate(bf, company + "\n");
        replaceByteDate(bf, "--------------------------------\n");
        bf.append(lSize, 0, lSize.length);
        int pos = 0;
        for (String car : carNum) {
            replaceByteDate(bf, "차량번호 : " + car + "\n");
            bf.append(nSize, 0, nSize.length);
            if (bustDate != null && bustDate.length > pos) {
                replaceByteDate(bf, "견인일시 : " + DateUtil.convertDateType("yyyy-MM-dd HH:mm", bustDate[pos]) + "\n");
            }
            if (saveDate != null && saveDate.length > pos) {
                replaceByteDate(bf, "보관일시 : " + DateUtil.convertDateType("yyyy-MM-dd HH:mm", saveDate[pos]) + "\n");
            }
            replaceByteDate(bf, "반환일시 : " + returnDate + "\n");
            replaceByteDate(bf, "--------------------------------\n");
            bf.append(lSize, 0, lSize.length);
            if (bustAmt != null && bustAmt.length > pos) {
                replaceByteDate(bf, "견인료 : " + DateUtil.replaceMoney(bustAmt[pos]) + "원\n");
            }
            if (saveAmt != null && saveAmt.length > pos) {
                replaceByteDate(bf, "보관료 : " + DateUtil.replaceMoney(saveAmt[pos]) + "원\n");
            }
            bf.append(nSize, 0, nSize.length);
            replaceByteDate(bf, "--------------------------------\n");
            pos++;
        }

        bf.append(lSize, 0, lSize.length);
        if (payType == PAY_TYPE_CASH) {
            replaceByteDate(bf, "결제수단 : 현금\n");
        } else {
            replaceByteDate(bf, "결제수단 : 카드\n");
        }
        replaceByteDate(bf, "결제금액 : " + DateUtil.replaceMoney(totalAmt) + "원\n");

        bf.append(nSize, 0, nSize.length);
        if (payType == ReceiptVo.PAY_TYPE_CARD) {
            replaceByteDate(bf, "카 드 사 : " + issuerName + "\n");
            replaceByteDate(bf, "카드번호 : " + cardNo + "\n");
            replaceByteDate(bf, "승인번호 : " + approvalNo + "\n");
        }

        bf.append(nSize, 0, nSize.length);
        replaceByteDate(bf, "--------------------------------\n");

        replaceByteDate(bf, "반 환 자 : " + returnUser + "\n");
        replaceByteDate(bf, "반환관계 : " + returnRelation + "\n");
        replaceByteDate(bf, "################################\n");
        replaceByteDate(bf, "사업자명 : " + manager + "\n");
        replaceByteDate(bf, "사업자번호 : " + managerNum + "\n");
        replaceByteDate(bf, "대표자명 : " + ceoNm + "\n");
        replaceByteDate(bf, "문의전화 : " + empPhone + "\n");
        replaceByteDate(bf, "################################\n");
        if (!StringUtil.isEmpty(footerStr)) {
            replaceByteDate(bf, footerStr + "\n");
        } else {
            replaceByteDate(bf, "이용해주셔서 감사합니다.\n");
        }
        replaceByteDate(bf, "\n\n");
        return bf;
    }


}
