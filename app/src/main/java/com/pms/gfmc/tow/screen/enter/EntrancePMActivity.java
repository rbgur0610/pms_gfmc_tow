package com.pms.gfmc.tow.screen.enter;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.ViewPager;

import com.aquery.AQuery;
import com.google.android.gms.vision.barcode.Barcode;
import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.databinding.ActivityEntranceMenuBinding;
import com.pms.gfmc.tow.databinding.ActivityEntrancePmBinding;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.carnum.AutoCarNumberActivity;
import com.pms.gfmc.tow.screen.retore.RestoreListActivity;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.Logger;
import com.pms.gfmc.tow.utils.StringUtil;
import com.pms.gfmc.tow.view.SlidingTabStrip;
import com.pms.gfmc.tow.view.barcode.BarcodeReader;
import com.pms.gfmc.tow.view.tag.TagView;

import java.util.ArrayList;
import java.util.List;

public class EntrancePMActivity extends BaseActivity implements BarcodeReader.BarcodeReaderListener {
    private ActivityEntrancePmBinding binding;
    private String TAG = "PM";
    private BarcodeReader barcodeReader;
    private ArrayList<String> companyCdList = new ArrayList<>();
    private ArrayList<String> companyNameList = new ArrayList<>();
    private ArrayList<String> companyUrlList = new ArrayList<>();
    private String selectCompany;
    private String selectCompanyCd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityEntrancePmBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnBack.setOnClickListener(v -> finish());
        initTabMenu();

        binding.btnConfirm.setOnClickListener(v -> {
            if (binding.rvList.getTags().size() == 0) {
                Toast.makeText(mContext, "추가된 아이디가 없습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent i = new Intent(mContext, EntrancePMDetailActivity.class);
            i.putStringArrayListExtra("QuickList", (ArrayList<String>) binding.rvList.getTags());
            startActivity(i);
            finish();
        });


        binding.edInput.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
        for (String item : basicCodes) {
            if (item.contains("T018")) {
                String[] carType = item.split("-");
                companyCdList.add(carType[1]);
                companyNameList.add(carType[2]);
                companyUrlList.add(carType[3]);
            }
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, companyNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spCompany.setAdapter(adapter);
        binding.spCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectCompany = companyNameList.get(i);
                selectCompanyCd = companyCdList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.rvList.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                AlertDialog dialog = new AlertDialog.Builder(mContext)
                        .setTitle("삭제하기")
                        .setMessage("선택한 아이디를 삭제하시겠습니까?")
                        .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (position < binding.rvList.getChildCount()) {
                                    binding.rvList.removeTag(position);
                                }
                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onSelectedTagDrag(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {

            }
        });


        binding.btnAdd.setOnClickListener(v -> {
            AndroidUtil.hideKeyboard(mContext, binding.edInput);

            if (StringUtil.isEmpty(binding.edInput.getText().toString())) {
                Toast.makeText(mContext, "입력된 ID가 없습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (binding.rvList.getTags().size() < 11 && !binding.rvList.getTags().contains(selectCompany + " " + binding.edInput.getText().toString())) {
                binding.rvList.addTag(selectCompany + " " + binding.edInput.getText().toString());
                binding.edInput.setText("");
            } else if (binding.rvList.getTags().contains(selectCompany + " " + binding.edInput.getText().toString())) {
                Toast.makeText(mContext, "같은 아이디가 존재합니다.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "최대 10개까지만 등록 가능합니다.", Toast.LENGTH_SHORT).show();
            }
        });
        binding.edInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                AndroidUtil.hideKeyboard(mContext, binding.edInput);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (StringUtil.isEmpty(binding.edInput.getText().toString())) {
                        Toast.makeText(mContext, "입력된 ID가 없습니다.", Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    if (binding.rvList.getTags().size() < 11 && !binding.rvList.getTags().contains(selectCompany + " " + binding.edInput.getText().toString())) {
                        binding.rvList.addTag(selectCompany + " " + binding.edInput.getText().toString());
                        binding.edInput.setText("");
                    } else if (binding.rvList.getTags().contains(selectCompany + " " + binding.edInput.getText().toString())) {
                        Toast.makeText(mContext, "같은 아이디가 존재합니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "최대 10개까지만 등록 가능합니다.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initTabMenu() {
        ArrayList<String> tabsList = new ArrayList<>();
        tabsList.add("QR 인식");
        tabsList.add("직접 입력");
        binding.tabs.setTabItems(tabsList);
        binding.tabs.setTabPaddingLeftRight(18);
        binding.tabs.setOnTabItemClickListener(new SlidingTabStrip.OnTabItemClickListener() {
            @Override
            public void onSingleClickItem(int position) {
                switch (position) {
                    case 0:
                        if (barcodeReader != null) {
                            barcodeReader.resumeScanning();
                        }
                        binding.lyBarcode.setVisibility(View.VISIBLE);
                        binding.lySelfWrite.setVisibility(View.GONE);
                        break;
                    case 1:
                        if (barcodeReader != null) {
                            barcodeReader.pauseScanning();
                        }
                        binding.lyBarcode.setVisibility(View.GONE);
                        binding.lySelfWrite.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onDoubleClickItem(int position) {

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        if (barcodeReader != null) {
            barcodeReader.resumeScanning();
        }


    }

    private AQuery aq;
    private String selectCompany_qr;
    boolean isSelect = false;

    @Override
    public void onScanned(Barcode barcode) {
        isSelect = false;
        runOnUiThread(() -> {
            //todo action
            Logger.e(TAG, barcode.displayValue);
            //http://qrcode.theswing.co.kr/swingapp.page.link/BE2149

            ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
            for (String item : basicCodes) {
                if (item.contains("T018")) {
                    String[] carType = item.split("-");
                    if (!StringUtil.isEmpty(carType[3]) && barcode.displayValue.startsWith(carType[3])) {
                        int lastIdex = barcode.displayValue.lastIndexOf("/");
                        selectCompany_qr = barcode.displayValue.substring(lastIdex + 1);
                        selectCompany_qr = selectCompany_qr.replace("?qr=", ""); //킥고잉 예외처리
                        selectCompany_qr = selectCompany_qr.replace("?boardNo=", ""); //윈드 예외처리
                        selectCompany_qr = selectCompany_qr.replace("index.do?code=", ""); //알파카 예외처리
                        selectCompany_qr = selectCompany_qr.replace("xingxing?c=", ""); //씽씽 예외처리
                        selectCompany_qr = selectCompany_qr.replace("qr?name=", ""); //디어 예외처리
                        selectCompany_qr = selectCompany_qr.replace("?b=", ""); //지쿠터 예외처리
                        selectCompany_qr = selectCompany_qr.replace("?", ""); //다트 예외처리

                        if ("지쿠터".equals(carType[2]) && selectCompany_qr.length() > 6) {
                            selectCompany_qr = selectCompany_qr.substring(selectCompany_qr.length() - 6);
                        }

                        if ("버드".equals(carType[2]) || "빔".equals(carType[2]) || "라임".equals(carType[2]) || "휙고".equals(carType[2])) {
                            moveSelfInput();
                            Toast.makeText(mContext, "QR코드 아래 글자를 직접 입력해 주세요.", Toast.LENGTH_SHORT).show();
                            if (companyNameList != null && companyNameList.size() > 0) {
                                int selectPos = 0;
                                for (String company : companyNameList) {
                                    if (company.equals(carType[2])) {
                                        binding.spCompany.setSelection(selectPos);
                                        break;
                                    }
                                    selectPos++;
                                }
                            }
                        } else if (binding.rvList.getTags().size() < 11 && !binding.rvList.getTags().contains(carType[2] + " " + selectCompany_qr)) {
                            binding.rvList.addTag(carType[2] + " " + selectCompany_qr);
                            binding.edInput.setText("");
                        } else if (binding.rvList.getTags().contains(carType[2] + " " + selectCompany_qr)) {
                            Toast.makeText(mContext, "같은 아이디가 존재합니다.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "최대 10개까지만 등록 가능합니다.", Toast.LENGTH_SHORT).show();

                        }
                        selectCompany_qr = "";
                        isSelect = true;
                        break;
                    }
                }
            }

            if (!isSelect && StringUtil.isEmpty(selectCompany_qr)) {
                moveSelfInput();
                if (!StringUtil.isEmpty(barcode.displayValue)) {
                    Toast.makeText(mContext, "업체를 선택해주세요.", Toast.LENGTH_SHORT).show();
                    binding.edInput.setText(barcode.displayValue);
                } else {
                    Toast.makeText(mContext, "조회된 킥보드가 없습니다. 직접 입력해 주세요.", Toast.LENGTH_SHORT).show();
                }

            }


//            https://lime.bike/bc/v1/MTDOOZA=

//            if (barcode.displayValue.startsWith("http")) {
//                aq = new AQuery(mContext);
//                aq.ajax(barcode.displayValue)
//                        .get()
//                        .showLoading()
//                        .toObject(BaseDataModel.class, (data, error) -> {
//                            // Do stuff
//                            if (error == null) {
//                                Logger.e(TAG, data.id);
//
//                                if (binding.rvList.getTags().size() < 11 && !binding.rvList.getTags().contains(selectCompany_qr + " " + data.id)) {
//                                    binding.rvList.addTag(selectCompany_qr + " " + data.id);
//                                    binding.edInput.setText("");
//                                    selectCompany_qr = "";
//                                } else if (binding.rvList.getTags().contains(selectCompany_qr + " " + data.id)) {
//                                    Toast.makeText(mContext, "같은 아이디가 존재합니다.", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(mContext, "최대 10개까지만 등록 가능합니다.", Toast.LENGTH_SHORT).show();
//                                }
//
//
//                            } else {
//                                Toast.makeText(mContext, "조회에 실패하였습니다.", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//            }


        });
    }


    private void moveSelfInput() {
        binding.tabs.setSelection(1);
        if (barcodeReader != null) {
            barcodeReader.pauseScanning();
        }
        binding.lyBarcode.setVisibility(View.GONE);
        binding.lySelfWrite.setVisibility(View.VISIBLE);
        binding.edInput.setText("");
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }
}

