package com.pms.gfmc.tow.screen.payment;

import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;


public abstract class ReceiptVo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final byte ESC = 0x1b;
	private static final byte GS = 0x1d;

	protected static final byte PRINT_TEXT_SIZE_BOLD = 0x09;
	protected static final byte PRINT_TEXT_SIZE_LARGE = 0x10;
	protected static final byte PRINT_TEXT_SIZE_WIDE = 0x11;
	protected static final byte PRINT_TEXT_SIZE_DEFAULT = 0x00;


	public static final int PAY_TYPE_CASH = 0;
	public static final int PAY_TYPE_CARD = 1;

    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private byte mCharsize = 0x00;
	private byte mJustification = 0x00;
	
	/**
	 * @param isEmphasis
	 * @param isUnderline
	 * @param charSize
	 * @return
	 */
	protected byte[] getPrintOption(boolean isEmphasis, boolean isUnderline, byte charSize) {
//		ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
		
		byte[] cmd = { ESC, 0x45, (byte) (isEmphasis ? 1 : 0), // ESC E
				ESC, 0x2D, (byte) (isUnderline ? 1 : 0), // ESC -
				GS, 0x21, charSize, // GS !
				ESC, 0x61, mJustification }; // ESC a
//		buffer.append(cmd, 0, cmd.length);
		
		return cmd;
	}
	
	protected byte[] getPrintOption(boolean isEmphasis, byte charSize) {
		return getPrintOption(isEmphasis, false, charSize);
	}
	
	protected byte[] getPrintOption(byte charSize) {
		return getPrintOption(true, false, charSize);
	}
	
	public void replaceByteDate(ByteArrayBuffer bf,String str){
		byte[] text;
		try {
			Log.e("printMsg",str);
			text = str.getBytes("EUC-KR");
			
			if (text.length == 0)
				return;
		
			bf.append(text,0,text.length);
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static final int ALIGN_CENTER = 0;
	public static final int ALIGN_LEFT = 1;
	public static final int ALIGN_RIGHT = 2;
	
	public static String getEmpty(int max, int align, String value) {
		StringBuffer sb = new StringBuffer();
		
		if (value == null) {
			value = "0";
		}
		int len = value.length();
		
		
		switch (align) {
		case ALIGN_CENTER:
			int left = (max - len) / 2;
			for (int i = 0; i < left; i++) {
				sb.append(" ");
			}
			sb.append(value);
			for (int i = (left + len); i < max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_LEFT:
			sb.append(value);
			for (int i = len; i <= max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_RIGHT:
			for (int i = 0; i < (max - len); i++) {
				sb.append(" ");
			}
			sb.append(value);
			break;
		}
		
		return sb.toString();
	}

	
	public abstract ByteArrayBuffer print();
}
