package com.pms.gfmc.tow.screen.retore;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.databinding.ActivityRestoreDetailBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.RestoreListModel;
import com.pms.gfmc.tow.http.model.Tocim01;
import com.pms.gfmc.tow.http.model.WaitListModel;
import com.pms.gfmc.tow.http.request.CallRestoreListCar;
import com.pms.gfmc.tow.http.request.CallToInDetail;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.payment.PaymentActivity;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RestoreDetailActivity extends BaseActivity {

    private ActivityRestoreDetailBinding binding;
    private String inno = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityRestoreDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        inno = getIntent().getStringExtra("INNO");
        reqDetail();

        binding.btnBack.setOnClickListener(v -> finish());

        binding.btnConfirm.setOnClickListener(view -> {
            Intent i = new Intent(mContext, PaymentActivity.class);

            String[] toAmt = {binding.tvTowAmt.getText().toString().replace(",", "").replace("원", "")};
            String[] keepAmt = {binding.tvSaveAmt.getText().toString().replace(",", "").replace("원", "")};
            String[] totalAmt = {binding.tvTotalAmt.getText().toString().replace(",", "").replace("원", "")};

            String[] carNo = {binding.tvCarName.getText().toString()};
            String[] in_no = {inno};

            i.putExtra("CAR_NO", carNo);
            i.putExtra("INNO", in_no);

            i.putExtra("TO_AMT", toAmt);
            i.putExtra("KEEP_AMT", keepAmt);
            i.putExtra("TOTAL_AMT", totalAmt);
            i.putExtra("SELECT_TYPE", "SINGLE");

            String[] bust_date = {binding.tvTowDate.getText().toString()};
            String[] keep_date = {binding.tvSaveDate.getText().toString()};

            i.putExtra("BUST_DATE", bust_date);
            i.putExtra("KEEP_DATE", keep_date);

            startActivity(i);
            finish();
        });

    }


    private void reqDetail() {

        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("inno", inno);

        APICallRuller.getInstance().addCall(new CallToInDetail(mContext, map, new ActionResponseListener<WaitListModel>() {
            @Override
            public void onResponse(WaitListModel item) {
                try {
                    if (item.tocim01 == null || item.tocim01.size() == 0) {
                        Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    Tocim01 mData = item.tocim01.get(0);
                    binding.tvCarName.setText(mData.CARNO);
                    binding.tvCarType.setText(mData.CARTYPE);
                    ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
                    for (String data : basicCodes) {
                        if (data.contains("T015-" + mData.CARREF01)) {
                            String[] carType = data.split("-");
                            binding.tvCarGbn.setText(carType[2]);
                        }

                        if (data.contains("T016-" + mData.CARREF02)) {
                            String[] carType = data.split("-");
                            binding.tvCarSize.setText(carType[2]);
                        }

                        if (data.contains("T004-" + mData.PNUCODE)) {
                            String[] carType = data.split("-");
                            binding.tvTowAgency.setText(carType[2]);
                        }
                    }
                    ArrayList<String> dpCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_DP_CODE);
                    for (String data : dpCodes) {
                        if (data.contains("OP002-" + mData.SPOTCD)) {
                            String[] carType = data.split("-");
                            binding.tvAddressDong.setText(carType[2]);
                        }
                    }

                    binding.tvTowDate.setText(DateUtil.convertDateType("yyyy-MM-dd HH:mm", mData.TOWTIME));
                    binding.tvSaveDate.setText(DateUtil.convertDateType("yyyy-MM-dd HH:mm", mData.INDATE));
                    binding.tvSavePeriod.setText(mData.KEEPTIME);

                    binding.tvTowAmt.setText(StringUtil.addComma(mData.TOAMT) + "원");
                    binding.tvSaveAmt.setText(StringUtil.addComma(mData.KEEPAMT) + "원");
                    binding.tvTotalAmt.setText(StringUtil.addComma(mData.TOTALAMT) + "원");

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


}
