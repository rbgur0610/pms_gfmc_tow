package com.pms.gfmc.tow.screen.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.http.model.Stcim01Model;
import com.pms.gfmc.tow.http.model.Tocim01;
import com.pms.gfmc.tow.http.model.Tocim02;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class WaitListAdapter extends RecyclerView.Adapter<WaitListAdapter.eventBaseViewHolder> {
    private int selectedPosition = -1;
    Context mContext;
    private List<Tocim01> list;
    boolean isClear = false;
    ArrayList<String> basicCodes;

    public WaitListAdapter(Context context) {
        this.mContext = context;

        basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);

    }


    public void setListData(List<Tocim01> listData) {
        if (listData == null) {
            this.list = new ArrayList<>();
        }
        this.list = listData;
        try {
            new Handler().post(this::notifyDataSetChanged);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @NonNull
    @Override
    public eventBaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_wait, parent, false);
        return new eventBaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull eventBaseViewHolder holder, final int position) {
        Tocim01 item = list.get(position);

        holder.tv_carno.setText(item.CARNO);
        holder.tv_car_type.setText(item.CARTYPE);

        for (String data : basicCodes) {
            if (data.contains("T015-" + item.CARREF01)) {
                String[] carType = data.split("-");
                holder.tv_car_gbn.setText(carType[2]);
            }

            if (data.contains("T016-" + item.CARREF02)) {
                String[] carType = data.split("-");
                holder.tv_size.setText(carType[2]);
            }
        }


        holder.iv_check.setSelected(item.isCheck);
        holder.ly_contents.setTag(position);
        holder.ly_contents.setOnClickListener(v -> {
            int pos = (int) holder.ly_contents.getTag();
            list.get(pos).isCheck = !holder.iv_check.isSelected();
            notifyDataSetChanged();
        });


    }


    public List<Tocim01> getSelectItems() {
        List<Tocim01> rList = new ArrayList<>();
        for (Tocim01 item : list) {
            if (item.isCheck) rList.add(item);
        }
        return rList;
    }

    @Override
    public int getItemCount() {
        if (list == null) return 0;
        return list.size();
    }

    class eventBaseViewHolder extends RecyclerView.ViewHolder {

        eventBaseViewHolder(View itemView) {
            super(itemView);
            tv_carno = itemView.findViewById(R.id.tv_carno);
            tv_size = itemView.findViewById(R.id.tv_size);
            tv_car_gbn = itemView.findViewById(R.id.tv_car_gbn);
            ly_contents = itemView.findViewById(R.id.ly_contents);
            tv_car_type = itemView.findViewById(R.id.tv_car_type);
            iv_check = itemView.findViewById(R.id.iv_check);
        }

        public TextView tv_car_type;
        public View ly_contents;
        public TextView tv_size;
        public TextView tv_carno;
        public TextView tv_car_gbn;
        public ImageView iv_check;
    }

}
