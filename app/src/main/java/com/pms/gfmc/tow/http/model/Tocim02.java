package com.pms.gfmc.tow.http.model;

public class Tocim02 {

    public String TOAMT;
    public String PARTCODE;
    public String SPOTCD;
    public String SPOTNM;
    public String SPOTTIME;
    public String TOWTIME;
    public String TAKINGPERSON;
    public String TOWCAR;
    public String TAKINGVENDER;
    public String VENDERTEL;
    public String PNUCODE;
    public String SPOTGROUP;
    public String SPOTPERSON;
    public String INEMP;
    public String INCOMMENT;
    public String OUTDATE;
    public String OUTNO;
    public String CAROWNER;
    public String RELCD;
    public String CAROWNERID;
    public String TOWAMT;

    public String CARNO;
    public String INDATE;
    public String INSTATE;
    public String INNO;
    public String INTYPE;
    public String CARTYPE;
    public String CARREF01;
    public String CARREF02;


    public String KEEPAMT;
    public String TOTALAMT;
    public String KEEPTIME;
    public String SITEEMP;

    public String OUTDAY;
}
