package com.pms.gfmc.tow.data;

import java.util.HashMap;

public  class PaymentEvent {
    public String shopTransactionId;
    public String pgCno;
    public String mallId;
    public String resCd;
    public String transactionDate;
    public String approvalNo;
    public String approvalDate;
    public String cardNo;
    public String issuerName;
    public String acquirerName;
    public String shopName;
    public String ceoName;
}
