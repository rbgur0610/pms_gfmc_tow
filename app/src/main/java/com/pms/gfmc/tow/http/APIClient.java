package com.pms.gfmc.tow.http;

import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.http.interceptor.AddCookieInterceptor;
import com.pms.gfmc.tow.http.interceptor.ReceivedCookieInterceptor;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static APIInterface apiInterface;
    private static String TAG = "Network";
    public static boolean isGetCarInfo = false;


    public static APIInterface getInstance() {
        if (apiInterface == null || isGetCarInfo) {
            isGetCarInfo = false;
            apiInterface = getClient().create(APIInterface.class);
        }
        return apiInterface;
    }

    public static APIInterface getInstance(boolean isInfo) {
        if (apiInterface == null || isGetCarInfo != isInfo) {
            isGetCarInfo = isInfo;
            apiInterface = getClient().create(APIInterface.class);
        }
        return apiInterface;
    }

    static Retrofit getClient() {

        String deviceName = "";
        try {
            deviceName = AndroidUtil.getDeviceName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String _DeviceName = deviceName;
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(200, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(new AddCookieInterceptor())
                .addInterceptor(new ReceivedCookieInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder builder = chain.request().newBuilder()
                                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                                .addHeader("device", "android")
                                .addHeader("deviceName", _DeviceName)
                                .addHeader("Accept-Language", "ko");
                        return chain.proceed(builder.build());
                    }
                });

        setLoggingInterceptor(builder);
        OkHttpClient okHttpClient = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(isGetCarInfo ? Constants.BASE_CAR_INFO_IP : Constants.BASE_PUBLIC_IP)
                .client(okHttpClient)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


    private static void setLoggingInterceptor(OkHttpClient.Builder builder) {
        if (Constants.SHOW_LOG_MODE) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(message -> Logger.d(TAG, message));
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        APIClientExtension.setExtensionLoggingInterceptor(builder);
    }
}
