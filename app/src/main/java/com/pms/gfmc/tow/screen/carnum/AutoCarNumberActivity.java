package com.pms.gfmc.tow.screen.carnum;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;


import androidx.appcompat.app.AppCompatActivity;

import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.screen.enter.EntranceCarActivity;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.StringUtil;

import java.io.File;
import java.util.List;

import keona.keona_platerecoglib.keona.keona_platerecoglib.camera.KeonaRecogLayout;
import keona.keona_platerecoglib.keona.keona_platerecoglib.camera.KeonaRecogMODE;
import keona.recogmode.handle.KeonaRecogHandleMessage;

/**
 * 자동차 번호 자동인식 화면 Activity
 */
public class AutoCarNumberActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private String mImgPath;
    private String mImgName;
    private String mPioNumber;
    private String mCarNumber;

    private boolean bCarNum = false;
    private boolean bCarImg = false;
    private String CD_AREA = "";

    private KeonaRecogLayout mFrame;

    private TextView txt_result;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case KeonaRecogHandleMessage.RecogMessage_PlateNumber:

                    mCarNumber = msg.obj.toString();
                    txt_result.setText(mCarNumber);
                    if (mCarNumber.trim().length() > 4) {
                        Log.d(TAG, " mCarNumber >>> " + mCarNumber);
                        if (!bCarNum) {
                            Constants.AUTO_CAR_NUMBER = mCarNumber;
                            Log.d(TAG, " AUTO_CAR_NUMBER >>> " + Constants.AUTO_CAR_NUMBER);
                            bCarNum = true;
                        }
                        // 차량번호 추출 및 사진 저장 완료 시
                        if (bCarNum && bCarImg && !endFlag) {
                            Log.d(TAG, " RecogMessage_PlateNumber ");
                            moveEntrance();
                        }
                    }
                    break;

                case KeonaRecogHandleMessage.CameraControl_Zoom_In:
                    mFrame.CameraControl_ZoomIn();
                    break;

                case KeonaRecogHandleMessage.CameraControl_Zoom_Out:
                    mFrame.CameraControl_ZoomOut();
                    break;

                //카메라에, 사진 찍기를 요청
                case KeonaRecogHandleMessage.CameraControl_TakePicture:
                    mFrame.CameraControl_TakePicture();
                    break;

                //저장된 사진파일의 이름이 리턴됩니다
                case KeonaRecogHandleMessage.RecogMessage_TakePicture:
                    Toast.makeText(getApplicationContext(), "파일명 : " + msg.obj.toString() + " 저장되었습니다.", Toast.LENGTH_SHORT).show();
                    bCarImg = true;
                    // 차량번호 추출 및 사진 저장 완료 시
                    if (bCarNum && bCarImg && !endFlag) {
                        Log.d(TAG, " RecogMessage_TakePicture  ");
                        moveEntrance();
                    }
                    break;
                //카메라에서 지원되는 사진 사이즈를 전달
                case KeonaRecogHandleMessage.CameraControl_PictureSize_Get:
                    //*****************Example Code*********************
                    List<Size> list_CameraPictureSize = (List<Size>) msg.obj;
                    for (Size m : list_CameraPictureSize) {
                        Log.i("Keon-A", "Return Message PictureSize : " + m.width + "/" + m.height);
                    }
                    mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_PictureSize_Set);

                    break;

                //카메라에 사진 해상도를 적용

                case KeonaRecogHandleMessage.CameraControl_PictureSize_Set:
                    //	//*****************Example Code*********************
                    //mFrame.CameraControl_CameraPictureSize(800, 480);
                    mFrame.CameraControl_CameraPictureSize(1280, 960);
                    //mFrame.CameraControl_CameraPictureSize(640,480);
                    //mFrame.CameraControl_CameraPictureSize(800,600);
                    break;


                //라이브러리에서의 알림 메시지
				/*
			case KeonaRecogHandleMessage.Notice:
				String str_Message = (String) msg.obj;
				Toast.makeText(getApplicationContext(), str_Message, Toast.LENGTH_SHORT).show(); // 
				break;	
			}
			*/
            }
        }
    };
    private String NEXT_ACTIVITY;
    private String CAR_CINO;
    private boolean endFlag;
    private FrameLayout mFrame_Main;


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        endFlag = false;
        mFrame.Keona_ConfigurationChanged(newConfig);

        Log.d(TAG, "AutoCarNumberActivity : onConfigurationChanged");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.autocar_number_activity);

        showProgressDialog("카메라를 불러오는 중입니다. 잠시만 기다려주세요.");
        new Handler().postDelayed(this::closeProgressDialog, 1500);

        mImgPath = Constants.IMG_SAVE_PATH + "/" + DateUtil.getYmdhms("yyyyMMdd");
        Log.d(TAG, " mImgPath >>> " + mImgPath);

        File file = new File(mImgPath);
        if (!file.exists())  // 경로에 폴더가 있는지 확인
            file.mkdirs();

        txt_result = (TextView) findViewById(R.id.txt_result);
        String parkcode = PrefData.getInstance(this).getString(PrefData.SHARED_PREF_PARKCODE);
        mPioNumber = BaseActivity.makePIONumber(parkcode, Constants.PARK_NO);
        mImgName = mPioNumber + "_1.png";

        mFrame = new KeonaRecogLayout(this);
        LayoutParams mFrameParam = (LayoutParams) new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mFrame.setLayoutParams(mFrameParam);

        //인식 모드는 라이센스 키의 속성을 정합니다. 휴대전화로 인증을 하였다면 RECOG_CELLULAR를,
        //WIFI기기로 인증하시길 원한다면 RECOG_WIFI를 옵션에 넣어주세요.
        //아무것도 선택하지 않았다면 기본적으로 RECOG_CELLULAR가 됩니다.
        //mFrame.setKeonaReocog_Mode(KeonaRecogMODE.RECOG_WIFI);
        //mFrame.setKeonaOptions_PicturePath(mImgPath+"/auto_^yyyy^MM^dd^HH^mm^ss.jpg");	//사진 저장 경로 설정
        mFrame.setKeonaOptions_PicturePath(mImgPath + "/" + mImgName);    //사진 저장 경로 설정
        mFrame.setKeonaOptions_PictureMode(KeonaRecogMODE.OPTIONS_PictureSave_Immediately);    //인식후 자동 즉시 촬영
        mFrame.PrepareToRecog(mHandler);
        mFrame_Main = (FrameLayout) findViewById(R.id.frame_recog);
        mFrame_Main.addView(mFrame);

        ZoomControls mZoomCamera = (ZoomControls) findViewById(R.id.zoomControls_camerazoom);

        mZoomCamera.setOnZoomInClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_Zoom_In);
            }
        });

        mZoomCamera.setOnZoomOutClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_Zoom_Out);
            }
        });

        //사진찍기를 위한 버튼
		/*
		Button mTakePicture = (Button)findViewById(R.id.btn_takepictrue);
		mTakePicture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub튼
				//사진찍기를 요청합니다.
				mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_TakePicture);
			}
		});
		*/

        Intent intent = getIntent();
        if (intent != null) {
//            CD_AREA = intent.getStringExtra(Constants.CAR_AREA);
            NEXT_ACTIVITY = intent.getStringExtra(Constants.NEXT_ACTIVITY);
            CAR_CINO = intent.getStringExtra(Constants.CAR_CINO);
        }

        ImageButton btn_top_left = (ImageButton) findViewById(R.id.btn_top_left);
        btn_top_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button btn_top_inputcar = (Button) findViewById(R.id.btn_top_inputcar);
        btn_top_inputcar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startInputCarNumber(CD_AREA);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        bCarNum = false;
        bCarImg = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFrame.Keona_ResumeActivity();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mFrame != null) mFrame.Keona_PauseActivity();

    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Log.d(TAG, "AutoCarNumberActivity : onStart");
    }

    private void moveEntrance() {
        endFlag = true;
        Intent intent;

        intent = new Intent(this, EntranceCarActivity.class);

        if (!StringUtil.isEmpty(CAR_CINO)) {
            intent.putExtra(Constants.CAR_CINO, CAR_CINO);
        }

        intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_AUTO);
//        intent.putExtra(Constants.CAR_AREA, CD_AREA);
        intent.putExtra(Constants.CAR_INPUT_NO, mCarNumber);
        Constants.AUTO_PIO_NUMBER = mPioNumber;
        Constants.AUTO_CAR_IMG_PATH = mImgPath;
        Constants.AUTO_CAR_IMG_NAME = mImgName;
        startActivity(intent);
        finish();
    }


    private void startInputCarNumber(String carArea) {

        try {
            Intent i = new Intent(this, InputCarNumerActivity.class);
            i.putExtra(Constants.CAR_INPUT_MODE, "INCAR");
//            i.putExtra(Constants.CAR_AREA, carArea);
            i.putExtra(Constants.NEXT_ACTIVITY, NEXT_ACTIVITY);
            if (!StringUtil.isEmpty(CAR_CINO)) {
                i.putExtra(Constants.CAR_CINO, CAR_CINO);
            }

            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProgressDialog progressDialog;

    public void showProgressDialog(String text) {
        try {
            closeProgressDialog();

            String txt = "";
            progressDialog = new ProgressDialog(this);
            if (text != null && !"".equals(text)) {
                txt = text;
            } else {
                txt = "확인 중...";
            }

            progressDialog.setMessage(txt);
            progressDialog.setCancelable(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

}