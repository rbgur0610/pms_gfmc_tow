package com.pms.gfmc.tow.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.pms.gfmc.tow.R;


public class CommonDialog extends Dialog {
    private TextView mTxtTitle;
    private TextView mTxtComment;
    private TextView mBtLeft;
    private TextView mBtRight;
    private boolean isTitle = false;

    public CommonDialog(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.common_dialog2_no_title);
        init(context);
    }

    public CommonDialog(Context context, boolean isTitle) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.isTitle = isTitle;
        if (isTitle) {
            setContentView(R.layout.common_dialog_title);
        } else {
            setContentView(R.layout.common_dialog2_no_title);
        }
        init(context);
    }

    private void init(Context context) {
        mTxtComment = findViewById(R.id.txt_comment);
        mBtLeft = findViewById(R.id.bt_left);
        mBtRight = findViewById(R.id.bt_right);
        if (isTitle) {
            mTxtTitle = findViewById(R.id.txt_title);
        }
        setCancelable(true);
        setCanceledOnTouchOutside(false);
    }

    public CommonDialog setDialogTitle(int resId) {
        mTxtTitle.setText(resId);
        return this;
    }


    public CommonDialog setDialogComment(int resId) {
        mTxtComment.setText(resId);
        return this;
    }

    public CommonDialog setDialogComment(String string) {
        mTxtComment.setText(string);
        return this;
    }

    public CommonDialog setDialogSpannedComment(Spanned string) {
        mTxtComment.setText(string);
        return this;
    }


    public CommonDialog setBtnLeft(int res_id, View.OnClickListener listener) {
        mBtLeft.setText(res_id);
        mBtLeft.setVisibility(View.VISIBLE);
        mBtLeft.setOnClickListener(listener);
        return this;
    }

    public CommonDialog setBtnLeft(String str, View.OnClickListener listener) {
        mBtLeft.setText(str);
        mBtLeft.setVisibility(View.VISIBLE);
        mBtLeft.setOnClickListener(listener);
        return this;
    }

    public CommonDialog setBtnRight(int res_id, View.OnClickListener listener) {
        mBtRight.setText(res_id);
        mBtRight.setVisibility(View.VISIBLE);
        mBtRight.setOnClickListener(listener);
        return this;
    }

    public CommonDialog setBtnRight(String str, View.OnClickListener listener) {
        mBtRight.setText(str);
        mBtRight.setVisibility(View.VISIBLE);
        mBtRight.setOnClickListener(listener);
        return this;
    }
}
