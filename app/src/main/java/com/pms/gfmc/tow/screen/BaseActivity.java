package com.pms.gfmc.tow.screen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.print.BluetoothPrintService;
import com.pms.gfmc.tow.print.DeviceListActivity;
import com.pms.gfmc.tow.utils.DateUtil;
import com.woosim.printer.WoosimService;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    public Context mContext;
    public Activity mActivity;
    public ProgressDialog progressDialog;
    public static String TAG = "";


    // Name of the connected device
    public String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    public static BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the print services
    public static BluetoothPrintService mPrintService = null;
    private static ArrayList<Activity> ActList = new ArrayList<Activity>();
    public static WoosimService mWoosim;
    // Message types sent from the BluetoothPrintService Handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;
    // Intent request codes
    public static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    public static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    public static final int REQUEST_ENABLE_BT = 3;
    public static String mBluetoothAddress = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = this.getClass().getSimpleName();
        mContext = this;
        mActivity = this;
        getActivity(this);
    }

    public static String makePIONumber(String codePark, String empCode) {
        StringBuffer sb = new StringBuffer();
        sb.append("PA").append(codePark).append("-")
                .append(empCode).append("-")
                .append(DateUtil.getYmdhms("HHmmss"));
        return sb.toString();
    }

    private void getActivity(Activity act) {
        if (!ActList.contains(act)) {
            ActList.add(act);
        }
    }


    public void showProgressDialog() {
        showProgressDialog(ProgressDialog.STYLE_SPINNER, "");
    }

    public void showProgressDialog(boolean isClose) {
        showProgressDialog(ProgressDialog.STYLE_SPINNER, "", isClose);
    }

    public void showProgressDialog(int style, String text) {
        showProgressDialog(style, text, true);
    }


    public void showProgressDialog(int style, String text, boolean isClose) {
        try {
            if (isClose) {
                closeProgressDialog();
            } else {
                if (progressDialog != null && progressDialog.isShowing())
                    return;
            }

            String txt = "";
            progressDialog = new ProgressDialog(this);
            if (text != null && !"".equals(text)) {
                txt = text;
            } else {
                txt = "확인 중...";
            }

            progressDialog.setMessage(txt);
            progressDialog.setCancelable(true);
            progressDialog.setProgressStyle(style);

            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }


    public static RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    public static RequestBody toRequestBody(HashMap<String, Object> map) {
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(map).toString());
        return body;
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

    }

    /**
     * @return : onCreate이후 발생하는 이벤트 이며 initLayoutSetting(레이아웃설정)을 셋팅해준다.
     * @date : 2012.04.04
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initLayoutSetting();
    }

    /**
     * @return : 레이아웃 설정을 하는 뷰들을 셋팅하는곳 BaseActivity를 extends한 Activity에서 onCreate이후 발생한다.
     * @date : 2012.04.06
     */
    protected void initLayoutSetting() {
    }


    /**
     * @param : int tag 다이얼로그ID 테그
     * @param : String title 다이얼로그 타이틀 설정
     * @param : String message 다이얼로그 메세지 설정
     * @return : 아래 확인 취소 버튼 이벤트를 변수 지정없이 설정
     * @date : 2012.04.06
     */
    public Dialog twoButtonDialog(final int tag, String title, String message) {
        return twoButtonDialog(tag, title, message, true);
    }

    public Dialog twoButtonDialog(final int tag, String title, String message, boolean isTwoButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        try {
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(R.string.btn_ok,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                            if (tag == Constants.MAINACTIVITY_DESTROY_ACTIVITY) {
                                try {
                                    setDestroyList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                onBasicDlgItemSelected(tag, true);
                            }
                        }
                    });

            if (isTwoButton) {
                builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBasicDlgItemSelected(tag, false);
                    }
                });
            }

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    try {
                        dialog.cancel();
                        dialog.dismiss();
                    } catch (Exception e) {
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder.create();
    }

    /**
     * @return : 다이얼로그 ID를 반환 받아 콜백 이벤트를 사용할 수 있다.
     * @date : 2012.04.06
     */
    public void onBasicDlgItemSelected(int tag, boolean choice) {
    }


    /**
     * @return : 해당 어플의 켜져있는 모든 activity를 종료 시킨다./카테고리 초기화/잠금화면 종료 시간 저장
     * @date : 2012.04.10
     */
    protected void setDestroyList() {
        HashSet<Activity> hs = new HashSet<Activity>(ActList);
        Iterator<Activity> it = hs.iterator();
        while (it.hasNext()) {
            Activity mAct = it.next();
            Log.e(TAG, mAct.getLocalClassName());
            mAct.finish();
            mAct.overridePendingTransition(0, 0);
        }

        ActList.clear();
    }

    public void setToast(String msg) {
        try {
            Toast toastMsg = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
            toastMsg.show();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    public void closedActivity() {
    }

    public void Print() {
    }


    public void printStop() {
        if (mPrintService != null) {
            mPrintService.stop();
            Log.e("onDestroy", "mPrintService.stop()");
        }
    }

    public void setupPrint() {
        Log.e("!!!!!", "setupPrint()");
        // Initialize the BluetoothPrintService to perform bluetooth connections
        mPrintService = new BluetoothPrintService(this, mHandler);
        mWoosim = new WoosimService(mHandler);
    }

    // Key names received from the BluetoothPrintService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    // The Handler that gets information back from the BluetoothPrintService
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Log.d("!!!!!", " mConnectedDeviceName >>>> " + mConnectedDeviceName);
                    PrefData.getInstance(mContext).putString(PrefData.PRINT_NAME, mConnectedDeviceName);
                    Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    Print();
                    break;
                case MESSAGE_TOAST:
                    Log.d("!!!!!", " msg.getData().getString(TOAST) >>>> " + msg.getData().getString(TOAST));
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                    closedActivity();
                    break;
                case MESSAGE_READ:
                    mWoosim.processRcvData((byte[]) msg.obj, msg.arg1);
                    break;
                case WoosimService.MESSAGE_PRINTER:
                    switch (msg.arg1) {
                        case WoosimService.MSR:
                            Log.d("!!!!!", "MSR>>>> ");
                            Log.d("!!!!!", "WoosimService >>>> WoosimService.MSR ::: " + WoosimService.MSR);
                            Log.d("!!!!!", "WoosimService >>>> msg.arg2 ::: " + msg.arg2);
                            if (msg.arg2 == 0) {
                                Toast.makeText(getApplicationContext(), "MSR reading failure", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                    break;
            }
        }
    };


    public boolean bleSetting() {
        if (mBluetoothAdapter == null) mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.toast_bt_na, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    public void pServiceSetting() {
        Log.e("!!!!!", "pServiceSetting()");
        if (mPrintService != null && mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            mPrintService.start();
            Log.e("!!!!!", "pServiceSetting()  start()");
        }
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onStart() {
        super.onStart();

        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mPrintService == null) setupPrint();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a print
                    setupPrint();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d("", "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        mBluetoothAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BLuetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
        // Attempt to connect to the device
        mPrintService.connect(device, secure);
    }
    private boolean stopFlag = false;
    @SuppressLint("MissingPermission")
    public void bluetoothCheck() {
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mPrintService != null && mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            // Get a set of currently paired devices
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                Log.e("pairedDevices", pairedDevices.size() + "");
                for (BluetoothDevice device : pairedDevices) {
                    String saveDevice = PrefData.getInstance(mContext).getString(PrefData.PRINT_NAME, "");
                    Log.e(">>>>", " device.getName() >>> " + device.getName() + "      saveDevice  : " + saveDevice);
                    if (saveDevice.equals(device.getName())) {
                        BluetoothDevice printDevice = mBluetoothAdapter.getRemoteDevice(device.getAddress());
                        // Attempt to connect to the device
                        mPrintService.connect(printDevice, false);
                        stopFlag = true;
                    }
                }
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            } else {
                if (!stopFlag) {
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    stopFlag = true;
                }
            }
        }

    }


}
