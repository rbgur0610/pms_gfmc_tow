package com.pms.gfmc.tow.http.interceptor;

import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import androidx.annotation.NonNull;
import com.pms.gfmc.tow.GlobalApplication;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by mwkim
 * on 2018. 8. 1..
 */
public class ReceivedCookieInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(GlobalApplication.getContext());
        }

        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            for (String header : originalResponse.headers("Set-Cookie")) {
                CookieManager.getInstance().setCookie(chain.request().url().toString(), header);
            }
        }

        return originalResponse;
    }
}