package com.pms.gfmc.tow.http.request;

import android.content.Context;

import com.pms.gfmc.tow.http.APICall;
import com.pms.gfmc.tow.http.APIClient;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallWaitPm extends APICall {

    public ActionResponseListener<BaseDataModel> actionResponseListener;
    private RequestBody map;

    public CallWaitPm(Context context, RequestBody map, ActionResponseListener<BaseDataModel> actionResponseListener) {
        super(context);
        this.actionResponseListener = actionResponseListener;
        this.map = map;
    }

    @Override
    public void run() {

        call = APIClient.getInstance().getWaitPm(map);
        call.enqueue(new Callback<BaseDataModel>() {
            @Override
            public void onResponse(Call<BaseDataModel> call, Response<BaseDataModel> response) {
                callDone(response, CallWaitPm.this);
                switch (response.code()) {
                    case 200:
                        if (actionResponseListener != null) {
                            actionResponseListener.onResponse(response.body());
                        }
                        break;
                    default:
                        if (actionResponseListener != null) {
                            actionResponseListener.onFailure();
                        }
                }
            }

            @Override
            public void onFailure(Call<BaseDataModel> call, Throwable t) {
                callDone(t, CallWaitPm.this);
                if (actionResponseListener != null) {
                    actionResponseListener.onFailure();
                }
            }
        });
    }
}
