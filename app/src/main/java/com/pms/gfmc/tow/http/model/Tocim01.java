package com.pms.gfmc.tow.http.model;

public class Tocim01 {

    public String CARNO;
    public String INSTATE;
    public String INNO;
    public String INTYPE;
    public String CARTYPE;
    public String CARREF01;
    public String CARREF02;
    public String TOAMT;
    public String SPOTCD;
    public String SPOTNM;
    public String SPOTTIME;
    public String TOWTIME;
    public String TAKINGPERSON;
    public String TOWCAR;
    public String TAKINGVENDER;
    public String VENDERTEL;
    public String PNUCODE;
    public String SPOTGROUP;
    public String SPOTPERSON;
    public String INCOMMENT;
    public String INDATE;
    public String KEEPTIME;
    public String KEEPAMT;
    public String TOTALAMT;
    public boolean isCheck = false;
}
