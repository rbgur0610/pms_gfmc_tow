package com.pms.gfmc.tow.view.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.view.treeview.model.TreeNode;

public class TreeHolderHolder extends TreeNode.BaseNodeViewHolder<TreeHolderHolder.Item> {


    public TreeHolderHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, Item value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_tree_node, null, false);


        TextView tv_data = (TextView) view.findViewById(R.id.tv_data);
//        tv_data.setText(value.data);

        return view;
    }

    @Override
    public void toggle(boolean active) {
    }


    public static class Item {
        public BasicOPM01Model data;

        public Item(BasicOPM01Model data) {
            this.data = data;
        }
        // rest will be hardcoded
    }

}
