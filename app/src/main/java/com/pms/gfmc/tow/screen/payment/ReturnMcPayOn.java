package com.pms.gfmc.tow.screen.payment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcpay.call.api.McPaymentAPI;
import com.mcpay.call.bean.ReturnDataBean;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.common.Constants;
import com.pms.gfmc.tow.data.PaymentEvent;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.preference.Preference;
import com.pms.gfmc.tow.print.BluetoothPrintService;
import com.pms.gfmc.tow.print.DeviceListActivity;
import com.pms.gfmc.tow.screen.BaseActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.Set;

public class ReturnMcPayOn extends BaseActivity {

    /**
     * 리턴받는 Activity AndroidManifest.xml에서 아래와 같이 옵션 적용
     * <intent-filter>
     * <action android:name="android.intent.action.MAIN" />
     * </intent-filter>
     */
    private PaymentEvent data = new PaymentEvent();
    private Context mContext = this;
    private McPaymentAPI mPaymentAPI = McPaymentAPI.getInstance();
    private ReturnDataBean mReturnDataBean;
    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.returnmcpayon_layout);

        byte[] returnData = mPaymentAPI.getPaymentReturnData(mContext);

        mReturnDataBean = mPaymentAPI.parseReturnPayment(returnData);
        data = new PaymentEvent();
        data.resCd = mReturnDataBean.getTradeResultCode();
        String resultData = "";

        Log.d("ReturnMcPayOn", "####" + mReturnDataBean.getTradeResultCode() + "~" + mReturnDataBean.getTradePaymentCode());

        if (mReturnDataBean.getTradeResultCode().equals("0000")) {
            if (mReturnDataBean.getTradePaymentCode().equals("N01") || mReturnDataBean.getTradePaymentCode().equals("N02")) {
                if (mReturnDataBean.getTradePaymentCode().equals("N01"))
                    resultData = "[신용결제]";
                else
                    resultData = "[신용취소]";

                resultData += "\n요청인증번호 : " + mReturnDataBean.getVerificationCode();
                resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
                resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
                resultData += "\n승인번호 : " + mReturnDataBean.getTradeApprovalNo();
                resultData += "\n결제금액 : " + mReturnDataBean.getTradeTotalAmount();
                resultData += "\n세금 : " + mReturnDataBean.getTradeTax();
                resultData += "\n봉사료 : " + mReturnDataBean.getTradeSvc();
                resultData += "\n카드명 : " + mReturnDataBean.getTradeCardName();
                resultData += "\n카드번호 : " + mReturnDataBean.getTradeCardNo();
                resultData += "\n매입사명 : " + mReturnDataBean.getTradePurchaseName();
                resultData += "\n카드입력모드 : " + mReturnDataBean.getTradeReadMode();
                resultData += "\n가맹점번호 : " + mReturnDataBean.getTradeShopNo();
                resultData += "\n알림 : " + mReturnDataBean.getTradeNoti();
                resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
                resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
                resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
                resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
                resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();

                Log.d("ReturnMcPayOn", resultData);


                data.shopTransactionId = mReturnDataBean.getTradePaymentTid();
                data.pgCno = mReturnDataBean.getVerificationCode();
                data.mallId = mReturnDataBean.getTradeShopNo();
                data.transactionDate = mReturnDataBean.getTradeApprovalDate();
                data.approvalNo = mReturnDataBean.getTradeApprovalNo();
                data.approvalDate = mReturnDataBean.getTradeApprovalDate();
                data.cardNo = mReturnDataBean.getTradeCardNo();
                data.issuerName = mReturnDataBean.getTradeCardName();
                data.acquirerName = mReturnDataBean.getTradePurchaseName();
                data.ceoName = mReturnDataBean.getTradeCeoName();
                data.shopName = mReturnDataBean.getTradeShopName();


            } else if (mReturnDataBean.getTradePaymentCode().equals("N03") || mReturnDataBean.getTradePaymentCode().equals("N05")) {
                if (mReturnDataBean.getTradePaymentCode().equals("N03"))
                    resultData = "[현금결제]";
                else
                    resultData = "[현금취소]";

                resultData += "\n요청인증번호 : " + mReturnDataBean.getVerificationCode();
                resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
                resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
                resultData += "\n승인번호 : " + mReturnDataBean.getTradeApprovalNo();
                resultData += "\n결제금액 : " + mReturnDataBean.getTradeTotalAmount();
                resultData += "\n세금 : " + mReturnDataBean.getTradeTax();
                resultData += "\n봉사료 : " + mReturnDataBean.getTradeSvc();
                resultData += "\n개인정보 : " + mReturnDataBean.getTradeCashInfo();
                resultData += "\n카드입력모드 : " + mReturnDataBean.getTradeReadMode();
                resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
                resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
                resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
                resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
                resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();


            } else if (mReturnDataBean.getTradePaymentCode().equals("N05")) {
                resultData = "[간이영수증]";

                resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
                resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
                resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
                resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
                resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
                resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
                resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();
            }
        } else {

            resultData = "\n응답코드 : " + mReturnDataBean.getTradeResultCode();
            resultData += "\n응답메세지 : " + mReturnDataBean.getTradeResultMsg();


        }

        try {

            bleSetting();
            pServiceSetting();


            if (mPrintService != null && mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
                // Get a set of currently paired devices
                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        Log.d(">>>>", " device.getName() >>> " + device.getName());
                        if (PrefData.getInstance(mContext).getString(PrefData.PRINT_NAME, "").equals(device.getName())) {
                            BluetoothDevice printDevice = mBluetoothAdapter.getRemoteDevice(device.getAddress());
                            // Attempt to connect to the device
                            mPrintService.connect(printDevice, false);
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception˙
            e.printStackTrace();
        }


        new Handler().postDelayed(() ->{
            EventBus.getDefault().post(data);
            finish();
        }, 1500);

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // 바이트배열 -> 비트맵
    private Bitmap byteArrayToBitmap(byte[] byteArray) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        return bitmap;
    }
}
