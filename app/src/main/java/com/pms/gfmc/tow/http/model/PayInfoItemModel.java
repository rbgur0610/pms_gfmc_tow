package com.pms.gfmc.tow.http.model;

public class PayInfoItemModel {
    public String INNO;
    public int TOAMT;
    public int KEEPAMT;
    public int TOTALAMT;
    public String TOWTIME;
    public String INDATE;
}
