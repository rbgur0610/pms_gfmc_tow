package com.pms.gfmc.tow.screen.payment;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mcpay.call.api.McPaymentAPI;
import com.mcpay.call.bean.ReturnDataBean;
import com.pms.gfmc.tow.GlobalApplication;
import com.pms.gfmc.tow.R;
import com.pms.gfmc.tow.data.PaymentEvent;
import com.pms.gfmc.tow.databinding.ActivityPaymentBinding;
import com.pms.gfmc.tow.http.APICallRuller;
import com.pms.gfmc.tow.http.ActionResponseListener;
import com.pms.gfmc.tow.http.model.BaseDataModel;
import com.pms.gfmc.tow.http.model.BasicOPM01Model;
import com.pms.gfmc.tow.http.model.PayInfoItemModel;
import com.pms.gfmc.tow.http.model.PayInfoModel;
import com.pms.gfmc.tow.http.request.CallPayInfo;
import com.pms.gfmc.tow.http.request.CallToOutCar;
import com.pms.gfmc.tow.http.request.CallWaitPm;
import com.pms.gfmc.tow.preference.PrefData;
import com.pms.gfmc.tow.screen.BaseActivity;
import com.pms.gfmc.tow.utils.AndroidUtil;
import com.pms.gfmc.tow.utils.DateUtil;
import com.pms.gfmc.tow.utils.Logger;
import com.pms.gfmc.tow.utils.StringUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

public class PaymentActivity extends BaseActivity {
    private ActivityPaymentBinding binding;
    private ArrayList<String> payment_user;
    private String[] carno, inno, toAmt, keepAmt, totalAmt, towDate, keepDate;
    private String selectType;
    private String returnRelation;
    private String manager;
    private String managerNum;
    private String ceoNm;
    private String empPhone;
    private String footerStr;
    private String issuerName;
    private String cardNo;
    private String approvalNo;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);

        GlobalApplication.setStatusBarColor(this, Color.parseColor("#363636"));
        binding = ActivityPaymentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnBack.setOnClickListener(v -> finish());

        selectType = getIntent().getStringExtra("SELECT_TYPE");
        carno = getIntent().getStringArrayExtra("CAR_NO");
        inno = getIntent().getStringArrayExtra("INNO");

        if ("SINGLE".equals(selectType)) {
            toAmt = getIntent().getStringArrayExtra("TO_AMT");
            keepAmt = getIntent().getStringArrayExtra("KEEP_AMT");
            totalAmt = getIntent().getStringArrayExtra("TOTAL_AMT");

            towDate = getIntent().getStringArrayExtra("BUST_DATE");
            keepDate = getIntent().getStringArrayExtra("KEEP_DATE");


            int amt = 0;
            for (String item : totalAmt) {
                amt += Integer.parseInt(item);
            }
            binding.tvTotalAmt.setText(StringUtil.addComma(amt) + "원");
        } else {
            getPayInfo();
        }


        payment_user = new ArrayList<>();
        ArrayList<String> userSpList = new ArrayList<>();
        ArrayList<String> basicCodes = (ArrayList<String>) PrefData.getInstance(mContext).getListString(PrefData.SHARED_PREF_BASIC_CODE);
        for (String item : basicCodes) {
            if (item.contains("T005")) {
                String[] user = item.split("-");
                userSpList.add(user[2]);
                payment_user.add(user[1]);
            }
        }


        if (userSpList.size() > 1) {
            ArrayAdapter<String> adapter =
                    new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_textview_default, userSpList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            binding.spUser.setAdapter(adapter);
            binding.spUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    binding.spUser.setTag(payment_user.get(i));
                    returnRelation = userSpList.get(i);

                    AndroidUtil.hideKeyboard(mContext, binding.edOwnnum);
                    AndroidUtil.hideKeyboard(mContext, binding.edUsername);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                    AndroidUtil.hideKeyboard(mContext, binding.edOwnnum);
                    AndroidUtil.hideKeyboard(mContext, binding.edUsername);
                }
            });
        }

        makeDash();
        binding.lyCard.setOnClickListener(v -> {
            if (!checkData()) return;
// 결제 테스트 100원 셋팅
//            binding.tvTotalAmt.setText("100");
            goToPayActivity(binding.tvTotalAmt.getText().toString().replace("원", "").replace(",", ""));
        });

        binding.lyCash.setOnClickListener(v -> {
            if (!checkData()) return;
            reqToOutCar("CA");
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private boolean checkData() {
        if (binding.edUsername.getText().toString().length() == 0) {
            Toast.makeText(mContext, "이름을 입력해 주세요.", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.edOwnnum.getText().toString().length() < 7) {
            Toast.makeText(mContext, "주민번호를 입력해 주세요.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void reqToOutCar(String payType) {
        reqToOutCar(payType, null);
    }

    private void reqToOutCar(String payType, HashMap<String, Object> cardMap) {

        showProgressDialog();
        String userID = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_ID, "");

        HashMap<String, Object> map = new HashMap<>();
        map.put("carno", carno);
        map.put("inno", inno);
        map.put("toamt", toAmt);
        map.put("keepamt", keepAmt);
        map.put("totalamt", totalAmt);
        map.put("id", userID);
        map.put("carowner", binding.edUsername.getText().toString());
        map.put("relcd", binding.spUser.getTag().toString());
        map.put("carownerid", binding.edOwnnum.getText().toString().replace("-", ""));
        map.put("paytype", payType);
        if (cardMap != null) map.putAll(cardMap);
        //카드 결제


        APICallRuller.getInstance().addCall(new CallToOutCar(mContext, toRequestBody(map), new ActionResponseListener<BaseDataModel>() {
            @Override
            public void onResponse(BaseDataModel mData) {
                try {
                    Toast.makeText(mContext, "정상 처리 하였습니다.", Toast.LENGTH_SHORT).show();
                    payPrint("CA".equals(payType) ? ReceiptVo.PAY_TYPE_CASH : ReceiptVo.PAY_TYPE_CARD);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }

    private void getPayInfo() {
        showProgressDialog();
        HashMap<String, Object> map = new HashMap<>();
        map.put("inno", inno);

        APICallRuller.getInstance().addCall(new CallPayInfo(mContext, toRequestBody(map), new ActionResponseListener<PayInfoModel>() {
            @Override
            public void onResponse(PayInfoModel mData) {
                try {
                    toAmt = new String[mData.info.size()];
                    keepAmt = new String[mData.info.size()];
                    totalAmt = new String[mData.info.size()];
                    towDate = new String[mData.info.size()];
                    keepDate = new String[mData.info.size()];
                    int amt = 0;
                    int i = 0;
                    for (PayInfoItemModel item : mData.info) {
                        amt += item.TOTALAMT;
                        toAmt[i] = String.valueOf(item.TOAMT);
                        keepAmt[i] = String.valueOf(item.KEEPAMT);
                        totalAmt[i] = String.valueOf(item.TOTALAMT);
                        towDate[i] = String.valueOf(item.TOWTIME);
                        keepDate[i] = String.valueOf(item.INDATE);
                        i++;
                    }
                    binding.tvTotalAmt.setText(StringUtil.addComma(amt) + "원");


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                }

                closeProgressDialog();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "실패 하였습니다.", Toast.LENGTH_SHORT).show();
                closeProgressDialog();
            }
        }));
        APICallRuller.getInstance().runNext();
    }


    private void makeDash() {
        binding.edOwnnum.addTextChangedListener(new TextWatcher() {

            private int _beforeLenght = 0;
            private int _afterLenght = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                _beforeLenght = s.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 0) {
                    return;
                }

                char inputChar = s.charAt(s.length() - 1);
                if (inputChar != '-' && (inputChar < '0' || inputChar > '9')) {
                    binding.edOwnnum.getText().delete(s.length() - 1, s.length());
                    return;
                }

                _afterLenght = s.length();

                // 삭제 중
                if (_beforeLenght > _afterLenght) {
                    // 삭제 중에 마지막에 -는 자동으로 지우기
                    if (s.toString().endsWith("-")) {
                        binding.edOwnnum.setText(s.toString().substring(0, s.length() - 1));
                    }
                }
                // 입력 중
                else if (_beforeLenght < _afterLenght) {
                    if (_afterLenght == 7 && s.toString().indexOf("-") < 0) {
                        binding.edOwnnum.setText(s.toString().subSequence(0, 6) + "-" + s.toString().substring(6, s.length()));
                    }
                }
                binding.edOwnnum.setSelection(binding.edOwnnum.length());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // 생략
            }
        });
    }

    //******** 신용카드 결제 (McPay)   ***********************//
    private void goToPayActivity(String money) {
        mPrintService.stop();
        Intent i = new Intent(this, McPayActivity.class);
        i.putExtra("SHOW_DIALOG_TYPE", PayData.PAY_DIALOG_TYPE_EXIT_CARD);
        i.putExtra("PAYMENT_MONEY", money);
        startActivityForResult(i, PayData.PAY_DIALOG_TYPE_EXIT_CARD);
    }


    //******** 신용카드 결제  취소 (Mcpay)  ***********************//
    protected void goToPayCancelActivity(int dType, String money, String approvalNo, String approvalDate, ReceiptVo vo) {
        //Intent i = new Intent(this, PayActivity.class);
        mPrintService.stop();
        Intent i = new Intent(this, McPayActivity.class);
        i.putExtra("SHOW_DIALOG_TYPE", dType);
        i.putExtra("PAYMENT_MONEY", money);
        i.putExtra("APPROVAL_NO", approvalNo);
        i.putExtra("APPROVAL_DATE", approvalDate);
        i.putExtra("ReceiptVo", vo);
        Log.d(TAG, " approvalNo   ::: " + approvalNo);
        Log.d(TAG, " approvalDate ::: " + approvalDate);
        startActivityForResult(i, dType);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Subscribe
    public void onEvent(@NonNull PaymentEvent event) {
        if ("0000".equals(event.resCd)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("shopTransactionId", event.shopTransactionId);
            map.put("pgCno", event.pgCno);
            map.put("mallId", event.mallId);
            map.put("resCd", event.resCd);
            map.put("transactionDate", event.transactionDate);
            map.put("approvalNo", event.approvalNo);
            map.put("approvalDate", event.approvalDate);
            map.put("cardNo", event.cardNo);
            map.put("issuerName", event.issuerName);
            map.put("acquirerName", event.acquirerName);


            issuerName = event.issuerName;
            cardNo = event.cardNo;
            approvalNo = event.approvalNo;
            new Handler().postDelayed(() -> reqToOutCar("CD", map), 500);
        } else {
            Toast.makeText(mContext, "결제가 실패하였습니다. 다시시도해 주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    private void payPrint(int payType) {
        ReceiptReturnVo vo = new ReceiptReturnVo();
        vo.title = "반환영수증";
        vo.company = "금천구시설관리공단견인차량보관소";
        vo.carNum = carno;
        vo.bustDate = towDate;
        vo.saveDate = keepDate;
        vo.returnDate = DateUtil.toDayTime();
        vo.bustAmt = toAmt;
        vo.saveAmt = keepAmt;
        vo.totalAmt = binding.tvTotalAmt.getText().toString();
        vo.payType = payType;

        if (payType == ReceiptVo.PAY_TYPE_CARD) {
            vo.issuerName = issuerName;
            vo.cardNo = cardNo;
            vo.approvalNo = approvalNo;
        }


        vo.returnUser = binding.edUsername.getText().toString();
        vo.returnRelation = returnRelation;

        vo.manager = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_VENDERNM);
        vo.managerNum = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_VENDERNO);
        vo.ceoNm = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_CEO);
        vo.empPhone = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_TELNO);
        vo.footerStr = PrefData.getInstance(mContext).getString(PrefData.SHARED_PREF_COMMENT);

        PayData payData = new PayData(this);
        payData.printDefault(vo, mPrintService);
    }


}
